#!/bin/bash
#set -v
#$1:version $2:target

SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BUILDINFO=${BUILDINFO:-""}

# gather buildinfo if not already provided
# For a release build BUILDINFO should be produced
# at the beginning of the build and used throughout
if [[ -z ${BUILDINFO} ]];then
    BUILDINFO=$(mktemp)
    ${SCRIPTPATH}/report_build_info.sh $1 > "${BUILDINFO}"
fi

# BUILD LD_EXTRAFLAGS
LD_EXTRAFLAGS=""

while read -r line; do
    LD_EXTRAFLAGS="${LD_EXTRAFLAGS} -X ${line}"
#    echo ${LD_EXTRAFLAGS}
done < "${BUILDINFO}"

set -v
go build -trimpath -ldflags "-s -w -X main.version=$1 ${LD_EXTRAFLAGS} " -o /njet-ingress github.com/nginxinc/kubernetes-ingress/cmd/nginx-ingress

#docker image: target is local
if [ $2 = "local" ]
then
  cp -a /njet-ingress njet-ingress
fi

