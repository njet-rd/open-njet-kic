package version

import (
	"fmt"
)

var (
	// RELEASE returns the release version
	RELEASE = "UNKNOWN"
	// TAG returns the git tag
	TAG = "UNKNOWN"
	// COMMIT returns the short sha from git
	COMMIT = "UNKNOWN"
	//BRANCH returns the branch
	BRANCH = "UNKNOWN"
	// NJET_RELEASE returns the release version
	NJET_RELEASE = "UNKNOWN"
	// NJET_TAG returns the git tag
	NJET_TAG = "UNKNOWN"
	// NJET_COMMIT returns the short sha from git
	NJET_COMMIT = "UNKNOWN"
	//NJET_BRANCH returns the branch
	NJET_BRANCH = "UNKNOWN"
)

// String returns information about the release.
func String() string {
	return fmt.Sprintf(`-------------------------------------------------------------------------------
version.BuildInfo{
	kic Release:    %v
  	kic Commit:     %v
	kic Tag:    	%v
  	kic Branch:    	%v
  	njet Release:       	%v
  	njet Commit:         	%v
  	njet Tag:    			%v 
	njet Branch:    		%v
  	}
-------------------------------------------------------------------------------
`, RELEASE, COMMIT, TAG, BRANCH, NJET_RELEASE, NJET_COMMIT, NJET_TAG, NJET_BRANCH)
}
