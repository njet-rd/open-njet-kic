package controller

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/golang/glog"
	"github.com/nginxinc/kubernetes-ingress/internal/configs/mainconfig"
	njetapihttp "github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller/http"
)

type NjetLogApi struct {
	LogEntrys map[string]mainconfig.LogSettingEntry
}

type AccessLogFormat struct {
	Name   string `json:"name,omitempty"`
	Escape string `json:"escape,omitempty"`
	Format string `json:"format,omitempty"`
}

type AccessLogServerLocationLogs struct {
	Path       string `json:"path"`
	FormatName string `json:"formatName"`
}

type AccessLogServerLocation struct {
	Location    string                        `json:"location"`
	AccessLogOn bool                          `json:"accessLogOn"`
	AccessLogs  []AccessLogServerLocationLogs `json:"accessLogs,omitempty"`
}

type AccessLogServer struct {
	Listens     []string                  `json:"listens"`
	ServerNames []string                  `json:"serverNames"`
	Locations   []AccessLogServerLocation `json:"locations"`
}

type AccessLogData struct {
	Servers          []AccessLogServer `json:"servers"`
	AccessLogFormats []AccessLogFormat `json:"accessLogFormats"`
}

func newLogClient(c *NjetApi) *LogImp {
	return &LogImp{
		urlPrefix: c.baseURL + "/api/v1/config/http_log",
		njetApi:   c,
	}
}

type LogImp struct {
	urlPrefix string
	njetApi   *NjetApi
}

func (l *LogImp) UpdateAll(data *NjetLogApi) error {
	if data == nil {
		return nil
	}
	//get access log configuration
	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.ConfigResource,
		Method:       http.MethodGet,
		RequestURL:   l.urlPrefix,
		Body:         nil,
		Retries:      0,
	}
	accessLogData := AccessLogData{}
	code, body, err := l.njetApi.SendRequest(restRequest)
	if err == nil && code == 200 {
		err = json.Unmarshal(body, &accessLogData)

		var logSetting mainconfig.LogSettingEntry
		var ok bool
		//TODO: get global value, it should change when location level log settting is supported
		if logSetting, ok = data.LogEntrys["main"]; !ok {
			glog.V(2).Infof("can't find global log configuration, skip update")
			return nil
		}
		if strings.TrimSpace(strings.Join(logSetting.LogFormat, "")) == "" {
			glog.V(3).Infof("Log Format is empty, skip update")
			return nil
		}
		logOff := logSetting.AccessLogOff
		for i, server := range accessLogData.Servers {
			for j := range server.Locations {
				accessLogData.Servers[i].Locations[j].AccessLogOn = !logOff
				if logOff {
					accessLogData.Servers[i].Locations[j].AccessLogs = nil
				} else {
					accessLogData.Servers[i].Locations[j].AccessLogs = make([]AccessLogServerLocationLogs, 1)
					accessLogData.Servers[i].Locations[j].AccessLogs[0].FormatName = "main"
					//global level is using /dev/stdout as output, will support location level setting later
					accessLogData.Servers[i].Locations[j].AccessLogs[0].Path = "/dev/stdout"
				}
			}
		}
		// log format
		for i, logFormat := range accessLogData.AccessLogFormats {
			if logFormat.Name == "main" {
				if logSetting.LogFormatEscaping != "" {
					accessLogData.AccessLogFormats[i].Escape = logSetting.LogFormatEscaping
				}
				accessLogData.AccessLogFormats[i].Format = strings.Join(logSetting.LogFormat, " ")
			}
		}

		var buf bytes.Buffer
		enc := json.NewEncoder(&buf)
		enc.SetEscapeHTML(false)
		enc.Encode(accessLogData)
		restRequest = &njetapihttp.RestRequest{
			ResourceType: njetapihttp.ConfigResource,
			Method:       http.MethodPut,
			RequestURL:   l.urlPrefix,
			Body:         buf.Bytes(),
			Retries:      0,
		}

		code, body, err := l.njetApi.SendRequest(restRequest)
		if err == nil {
			glog.V(2).Infof("HTTP Response: code: %d, body: %s", code, string(body))
		} else {
			glog.Errorf("http request failed:%s", err)
		}
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return nil
}

func (r *LogImp) Update(data *AccessLogData) error {
	if data == nil {
		glog.Errorf("LogImp:Update:data is nil")
		return fmt.Errorf("LogImp:Update:data is nil")
	}

	var buf bytes.Buffer
	enc := json.NewEncoder(&buf)
	// &&
	enc.SetEscapeHTML(false)
	enc.Encode(data)

	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.AccessLogResource,
		Method:       http.MethodPut,
		RequestURL:   r.urlPrefix,
		Body:         buf.Bytes(),
		Retries:      0,
	}

	code, body, err := r.njetApi.SendRequest(restRequest)
	if err == nil {
		err = r.njetApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func AccessLogResponseHandle(apiResponse *njetapihttp.CommonApiResponse) error {
	if apiResponse == nil {
		glog.Errorf("AccessLogResponseHandle:Njet Api Response nil")
		return fmt.Errorf("AccessLogResponseHandle:Njet Api Response nil")
	}

	switch apiResponse.Code {
	case RouteSuccessCode:
		return nil
	default:
		glog.Errorf("AccessLogResponseHandle: Njet AccessLog Api failed: %+v", apiResponse)
		return fmt.Errorf("%s", apiResponse.Msg)
	}

	return nil
}
