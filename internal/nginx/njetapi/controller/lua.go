package controller

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/golang/glog"
	njetapihttp "github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller/http"
)

const (
	LuaControlBaseURL   = "http://127.0.0.1:8080"
	LuaStreamConfigIp   = "127.0.0.1"
	LuaStreamConfigPort = "12002"
)

type Endpoint struct {
	// Address IP address of the endpoint
	Address string `json:"address"`
	// Port number of the TCP port
	Port string `json:"port"`
}

type UpstreamHashByConfig struct {
	UpstreamHashBy string `json:"upstream-hash-by,omitempty"`
}

type HealthCheck struct {
	Type        string            `json:"type,omitempty"`
	Schema      string            `json:"schema,omitempty"`
	Path        string            `json:"path,omitempty"`
	Port        int               `json:"port,omitempty"`
	Interval    int32             `json:"interval,omitempty"`
	Fails       int32             `json:"fails,omitempty"`
	Passes      int32             `json:"passes,omitempty"`
	StatusMatch string            `json:"statusMatch,omitempty"`
	Timeout     int64             `json:"timeout,omitempty"`
	Headers     map[string]string `json:"headers,omitempty"`
}

// describes one or more remote server/s (endpoints) associated with a service
type LuaUpstreamApi struct {
	Name string `json:"name"`
	// Endpoints contains the list of endpoints currently running
	Endpoints []Endpoint `json:"endpoints,omitempty"`
	// LB algorithm configuration per ingress
	LoadBalancing        string                `json:"load-balance,omitempty"`
	UpstreamHashByConfig *UpstreamHashByConfig `json:"upstreamHashByConfig,omitempty"`
	HealthCheck          *HealthCheck          `json:"healthCheck,omitempty"`
}

func newLuaUpstreamClient(l *LuaApi) *LuaUpstreamImp {
	return &LuaUpstreamImp{
		urlPrefix: l.baseURL + "/configuration/backends",
		luaApi:    l,
	}
}

type LuaUpstreamImp struct {
	urlPrefix string
	luaApi    *LuaApi
}

type LuaApi struct {
	httpclient *njetapihttp.ClientImpl
	baseURL    string
	upstream   *LuaUpstreamImp
}

func NewLuaApi() *LuaApi {
	luaApi := &LuaApi{
		httpclient: &njetapihttp.ClientImpl{
			Httpclient: &http.Client{
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: true,
					},
				},
				Timeout: 10 * time.Second,
			},
			Headers: map[string]interface{}{
				"Content-Type": "application/json",
			},
		},
		baseURL: LuaControlBaseURL,
	}

	luaApi.upstream = newLuaUpstreamClient(luaApi)

	return luaApi
}

func (c *LuaApi) SendRequest(data *njetapihttp.RestRequest) (int, []byte, error) {
	var pd io.Reader = nil
	client := c.httpclient.Httpclient
	if data.Body != nil {
		if data.Headers != nil {
			contentType, ok := (*data.Headers)["content-type"]
			if ok && strings.Compare(contentType, "application/json") != 0 {
				pd = bytes.NewReader(*data.Body.(*[]byte))
			}
		}
		if pd == nil {
			payload, err := json.Marshal(data.Body)
			if err != nil {
				glog.Errorf("Http client send request err, json.Marshal failed:%s", err)
				return 0, nil, err
			}
			glog.V(2).Infof("http client request body:%s", string(payload))
			pd = strings.NewReader(string(payload))
		}

	}

	method := data.Method
	url := data.RequestURL
	req, err := http.NewRequest(method, url, pd)
	if err != nil {
		return 0, nil, err
	}
	headers := map[string]string{}
	for hk, hv := range c.httpclient.Headers {
		headers[hk] = fmt.Sprintf("%v", hv)
	}
	for k, v := range headers {
		req.Header.Add(k, v)
	}
	if data.Headers != nil {
		for k, v := range *data.Headers {
			req.Header.Set(k, v)
		}
	}

	glog.V(2).Infof("request headers:%v", req.Header)
	res, err := client.Do(req)
	if err != nil {
		return 0, nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return res.StatusCode, nil, err
	}
	return res.StatusCode, body, nil
}

func (c *LuaApi) HandleResponse(req *njetapihttp.RestRequest, statusCode int, body []byte) error {
	if statusCode != 200 && statusCode != http.StatusCreated {
		glog.Errorf("http status code is %d, and body is %q", statusCode, string(body))
		return fmt.Errorf("http status code is %d, and body is %q", statusCode, string(body))
	}

	return nil
}

// Upstream return upstream client
func (c *LuaApi) Upstream() *LuaUpstreamImp {
	return c.upstream
}

func (r *LuaUpstreamImp) Create(data []*LuaUpstreamApi) error {
	if len(data) == 0 {
		return nil
	}
	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.LuaUpstreamResource,
		Method:       http.MethodPost,
		RequestURL:   r.urlPrefix,
		Body:         data,
		Retries:      0,
	}

	glog.V(2).Infof("Njet resource type: %q, method: %s, url:%s,  http request payload:%+v", restRequest.ResourceType, restRequest.Method, restRequest.RequestURL, restRequest.Body)
	code, body, err := r.luaApi.SendRequest(restRequest)
	if err == nil {
		glog.V(2).Infof("Njet resource type: %q, method: %s, url:%s, http response code :%v , http response body:%s", restRequest.ResourceType, restRequest.Method, restRequest.RequestURL, code, string(body))
		err = r.luaApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func (r *LuaUpstreamImp) CreateStreamUpstream(data []*LuaUpstreamApi) error {
	if len(data) == 0 {
		return nil
	}

	buf, err := json.Marshal(data)
	if err != nil {
		return err
	}

	hostPort := net.JoinHostPort(LuaStreamConfigIp, LuaStreamConfigPort)
	conn, err := net.Dial("tcp", hostPort)
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = conn.Write(buf)
	if err != nil {
		return err
	}
	_, err = fmt.Fprintf(conn, "\r\n")
	if err != nil {
		return err
	}

	return err
}
