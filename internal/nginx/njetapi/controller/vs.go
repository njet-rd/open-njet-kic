package controller

import (
	"fmt"
	"github.com/golang/glog"
	njetapihttp "github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller/http"
	"net/http"
)

const (
	VsSuccessCode         = 0
	VsDeleteOperationType = "del"
	VsAddOperationType    = "add"
)

type NjetVsApi struct {
	OperationType  string `json:"type"`
	AddressAndPort string `json:"addr_port"`
	ServerName     string `json:"server_name"`
	ServerBody     string `json:"server_body,omitempty"`
}

func newVsClient(c *NjetApi) *VsImp {
	return &VsImp{
		urlPrefix: c.baseURL + "/api/v1/dyn_srv",
		njetApi:   c,
	}
}

type VsImp struct {
	urlPrefix string
	njetApi   *NjetApi
}

func (r *VsImp) Create(data *NjetVsApi) error {
	if data != nil {
		data.OperationType = VsAddOperationType
	} else {
		glog.Errorf("http request body nil")
		return fmt.Errorf("http request body nil")
	}

	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.VsResource,
		Method:       http.MethodPost,
		RequestURL:   r.urlPrefix,
		Body:         data,
		Retries:      0,
	}

	code, body, err := r.njetApi.SendRequest(restRequest)
	if err == nil {
		err = r.njetApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func (r *VsImp) Delete(data *NjetVsApi) error {
	if data != nil {
		data.OperationType = VsDeleteOperationType
	} else {
		glog.Errorf("VsImp:Delete:http request body nil")
		return fmt.Errorf("VsImp:Delete:http request body nil")
	}

	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.VsResource,
		Method:       http.MethodPut,
		RequestURL:   r.urlPrefix,
		Body:         data,
		Retries:      0,
	}

	code, body, err := r.njetApi.SendRequest(restRequest)
	if err == nil {
		err = r.njetApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func VsResponseHandle(apiResponse *njetapihttp.CommonApiResponse) error {
	if apiResponse == nil {
		glog.Errorf("VsResponseHandle:Njet Api Response nil")
		return fmt.Errorf("VsResponseHandle:Njet Api Response nil")
	}

	switch apiResponse.Code {
	case RouteSuccessCode:
		return nil
	default:
		glog.Errorf("VsResponseHandle: Njet Vs Api failed: %+v", apiResponse)
		return fmt.Errorf("%s", apiResponse.Msg)
	}

	return nil
}
