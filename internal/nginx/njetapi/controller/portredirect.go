/*
 * Copyright (C) 2021-2023  TMLake(Beijing) Technology Co., Ltd.
 * OpenNJet-KIC is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *        http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package controller

import (
	"fmt"
	"github.com/golang/glog"
	njetapihttp "github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller/http"
	"net/http"
)

const (
	PortRedirectSuccessCode         = 0
	PortRedirectDeleteOperationType = "del"
	PortRedirectAddOperationType    = "add"
	PortRedirectUDPProtocolType     = "udp"
	PortRedirectTCPProtocolType     = "tcp"
)

type NjetPortRedirectApi struct {
	Action       string `json:"action,omitempty"`
	SrcPort      string `json:"src_ports"`
	ProtocolType string `json:"type"`
	DstPort      int    `json:"dst_port"`
}

func newPortRedirectClient(c *NjetApi) *PortRedirectImp {
	return &PortRedirectImp{
		urlPrefix: c.baseURL + "/api/v1/range",
		njetApi:   c,
	}
}

type PortRedirectImp struct {
	urlPrefix string
	njetApi   *NjetApi
}

func (pr *PortRedirectImp) Create(data *NjetPortRedirectApi) error {
	if data != nil {
		data.Action = PortRedirectAddOperationType
	}

	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.PortRedirectResource,
		Method:       http.MethodPut,
		RequestURL:   pr.urlPrefix,
		Body:         data,
		Retries:      0,
	}

	code, body, err := pr.njetApi.SendRequest(restRequest)
	if err == nil {
		err = pr.njetApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func (pr *PortRedirectImp) Delete(data *NjetPortRedirectApi) error {
	if data != nil {
		data.Action = PortRedirectDeleteOperationType
	} else {
		glog.Errorf("http request body nil")
		return fmt.Errorf("http request body nil")
	}

	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.PortRedirectResource,
		Method:       http.MethodPut,
		RequestURL:   pr.urlPrefix,
		Body:         data,
		Retries:      0,
	}

	code, body, err := pr.njetApi.SendRequest(restRequest)
	if err == nil {
		err = pr.njetApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func PortRedirectResponseHandle(apiResponse *njetapihttp.CommonApiResponse) error {
	if apiResponse == nil {
		glog.Errorf("Njet Api Response nil")
		return fmt.Errorf("Njet Api Response nil")
	}

	switch apiResponse.Code {
	case PortRedirectSuccessCode:
		return nil
	default:
		glog.Errorf("PortRedirectResponseHandle: Njet PortRedirect Api failed: %+v", apiResponse)
		return fmt.Errorf("%s", apiResponse.Msg)
	}

	return nil
}
