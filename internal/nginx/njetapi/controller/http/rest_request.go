package http

import "net/http"

const (
	RouteResource        = "route"
	UpstreamResource     = "upstream"
	LuaUpstreamResource  = "luaupstream"
	ConfigResource       = "config"
	PortRedirectResource = "portredirect"
	StreamMapResource    = "streammap"
	VsResource           = "vs"
	AccessLogResource    = "accesslog"
)

type RestRequest struct {
	Method       string
	RequestURL   string
	ResourceType string
	Headers      *map[string]string
	Body         interface{}

	Retries int
}

type ClientImpl struct {
	Httpclient *http.Client
	Headers    map[string]interface{}
}
