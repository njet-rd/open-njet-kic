package http

type CommonApiResponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}
