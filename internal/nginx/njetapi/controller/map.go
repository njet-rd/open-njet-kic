package controller

import (
	"net/http"

	"github.com/golang/glog"
	njetapihttp "github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller/http"
)

type ValueMapping struct {
	ValueFrom string `json:"valueFrom,omitempty"`
	ValueTo   string `json:"valueTo,omitempty"`
}

type Mapping struct {
	KeyFrom    string         `json:"keyFrom,omitempty"`
	KeyTo      string         `json:"keyTo,omitempty"`
	Values     []ValueMapping `json:"values"`
	IsVolatile bool           `json:"isVolatile"`
	Hostnames  bool           `json:"hostnames"`
}

type MappingForRS uint32

const (
	DYN_MAP_INGRESS MappingForRS = 0
	DYN_MAP_VS      MappingForRS = 1
	DYN_MAP_MERGE   MappingForRS = 2
)

type NjetMapApi struct {
	Maps       []Mapping    `json:"maps,omitempty"`
	MappingFor MappingForRS `json:"-"`
}

func newDynMapClient(c *NjetApi) *DynMapImp {
	return &DynMapImp{
		urlPrefix: c.baseURL + "/api/v1/config/http_dyn_map",
		njetApi:   c,
	}
}

type DynMapImp struct {
	urlPrefix     string
	njetApi       *NjetApi
	dynMapIngress *NjetMapApi
	dynMapVS      *NjetMapApi
}

func (r *DynMapImp) Get() error {
	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.ConfigResource,
		Method:       http.MethodGet,
		RequestURL:   r.urlPrefix,
		Retries:      0,
	}

	code, body, err := r.njetApi.SendRequest(restRequest)
	if err == nil {
		err = r.njetApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func mergeNjetMapping(data1 *NjetMapApi, data2 *NjetMapApi) *NjetMapApi {
	mergeData := &NjetMapApi{
		MappingFor: DYN_MAP_MERGE,
		Maps:       []Mapping{},
	}

	if data1 == nil {
		return data2
	}
	if data2 == nil {
		return data1
	}

	for _, mapping := range data1.Maps {
		kf := mapping.KeyFrom
		kt := mapping.KeyTo
		for _, mapping2 := range data2.Maps {
			if mapping2.KeyFrom == kf && mapping2.KeyTo == kt {
				if len(mapping.Values) == 0 {
					mapping.Values = append(mapping.Values, mapping2.Values...)
				} else {
					for _, v := range mapping2.Values {
						if v.ValueFrom != "default" {
							mapping.Values = append(mapping.Values, v)
						}
					}
				}
			}
		}
		mergeData.Maps = append(mergeData.Maps, mapping)
	}

	return mergeData
}

func (r *DynMapImp) Update(data *NjetMapApi) error {
	if data == nil {
		return nil
	}
	var mergedData *NjetMapApi
	if data.MappingFor == DYN_MAP_INGRESS {
		r.dynMapIngress = data
		mergedData = mergeNjetMapping(data, r.dynMapVS)
	}
	if data.MappingFor == DYN_MAP_VS {
		r.dynMapVS = data
		mergedData = mergeNjetMapping(data, r.dynMapIngress)
	}

	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.ConfigResource,
		Method:       http.MethodPut,
		RequestURL:   r.urlPrefix,
		Body:         mergedData,
		Retries:      0,
	}

	code, body, err := r.njetApi.SendRequest(restRequest)
	if err == nil {
		err = r.njetApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}
