package controller

import (
	"fmt"
	"github.com/golang/glog"
	njetapihttp "github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller/http"
	"net/http"
)

const (
	RouteSuccessCode         = 0
	RouteDeleteOperationType = "del"
	RouteAddOperationType    = "add"
)

type Location struct {
	LocationRule string `json:"location_rule,omitempty"`
	LocationName string `json:"location_name,omitempty"`
	LocationBody string `json:"location_body,omitempty"`
	ProxyPass    string `json:"proxy_pass,omitempty"`
}

type NjetRouteApi struct {
	OperationType  string     `json:"type,omitempty"`
	AddressAndPort string     `json:"addr_port,omitempty"`
	ServerName     string     `json:"server_name"`
	Locations      []Location `json:"locations,omitempty"`
}

type NjetRouteDeleteApi struct {
	OperationType  string `json:"type,omitempty"`
	AddressAndPort string `json:"addr_port,omitempty"`
	ServerName     string `json:"server_name"`
	LocationName   string `json:"location_name"`
}

func newRouteClient(c *NjetApi) *RouteImp {
	return &RouteImp{
		urlPrefix: c.baseURL + "/api/v1/dyn_loc",
		njetApi:   c,
	}
}

type RouteImp struct {
	urlPrefix string
	njetApi   *NjetApi
}

func (r *RouteImp) Create(data *NjetRouteApi) error {
	if data != nil {
		data.OperationType = RouteAddOperationType
	}

	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.RouteResource,
		Method:       http.MethodPost,
		RequestURL:   r.urlPrefix,
		Body:         data,
		Retries:      0,
	}

	code, body, err := r.njetApi.SendRequest(restRequest)
	if err == nil {
		err = r.njetApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func (r *RouteImp) Delete(data *NjetRouteApi) error {
	if data != nil {
		data.OperationType = RouteDeleteOperationType
	} else {
		glog.Errorf("http request body nil")
		return fmt.Errorf("http request body nil")
	}
	routeDeleteApi := &NjetRouteDeleteApi{
		OperationType:  data.OperationType,
		AddressAndPort: data.AddressAndPort,
		ServerName:     data.ServerName,
	}
	for _, location := range data.Locations {
		routeDeleteApi.LocationName = location.LocationName
		break
	}
	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.RouteResource,
		Method:       http.MethodPut,
		RequestURL:   r.urlPrefix,
		Body:         routeDeleteApi,
		Retries:      0,
	}

	code, body, err := r.njetApi.SendRequest(restRequest)
	if err == nil {
		err = r.njetApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func RouteResponseHandle(apiResponse *njetapihttp.CommonApiResponse) error {
	if apiResponse == nil {
		glog.Errorf("Njet Api Response nil")
		return fmt.Errorf("Njet Api Response nil")
	}

	switch apiResponse.Code {
	case RouteSuccessCode:
		return nil
	default:
		glog.Errorf("RouteResponseHandle: Njet Route Api failed: %+v", apiResponse)
		return fmt.Errorf("%s", apiResponse.Msg)
	}

	return nil
}
