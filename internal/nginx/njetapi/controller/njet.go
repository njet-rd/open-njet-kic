package controller

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/golang/glog"
	njetapihttp "github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller/http"
)

const NjetControlBaseURL = "http://127.0.0.1:12001"

// Route is the specific client interface used for CURD Njet Route resource.
type Route interface {
	Get(string) error
	List() error
	Create() error
	Delete() error
	Update() error
}

type NjetApi struct {
	httpclient   *njetapihttp.ClientImpl
	baseURL      string
	route        *RouteImp
	upstream     *UpstreamImp
	config       *ConfigImp
	dynmap       *DynMapImp
	log          *LogImp
	worker       *WorkerImp
	portRedirect *PortRedirectImp
	streamMap    *StreamMapImp
	vs           *VsImp
}

// Route return route client
func (c *NjetApi) Route() *RouteImp {
	return c.route
}

// Upstream return upstream client
func (c *NjetApi) Upstream() *UpstreamImp {
	return c.upstream
}

// Upstream return upstream client
func (c *NjetApi) Config() *ConfigImp {
	return c.config
}

// Upstream return DynMap client
func (c *NjetApi) DynMap() *DynMapImp {
	return c.dynmap
}

// Log return LogImp client
func (c *NjetApi) Log() *LogImp {
	return c.log
}

// Worker return WorkerImp client
func (c *NjetApi) Worker() *WorkerImp {
	return c.worker
}

// StreamMap return StreamMapImp client
func (c *NjetApi) StreamMap() *StreamMapImp {
	return c.streamMap
}

// PortRedirect return portRedirectImp client
func (c *NjetApi) PortRedirect() *PortRedirectImp {
	return c.portRedirect
}

// VsImp return VsImp client
func (c *NjetApi) VsImp() *VsImp {
	return c.vs
}

func NewNjetApi() *NjetApi {
	njetApi := &NjetApi{
		httpclient: &njetapihttp.ClientImpl{
			Httpclient: &http.Client{
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: true,
					},
				},
				Timeout: 10 * time.Second,
			},
			Headers: map[string]interface{}{
				"Content-Type": "application/json",
			},
		},
		baseURL: NjetControlBaseURL,
	}

	njetApi.route = newRouteClient(njetApi)
	njetApi.config = newConfigClient(njetApi)
	njetApi.dynmap = newDynMapClient(njetApi)
	njetApi.log = newLogClient(njetApi)
	njetApi.worker = newWorkerClient(njetApi)
	njetApi.portRedirect = newPortRedirectClient(njetApi)
	njetApi.streamMap = newStreamMapClient(njetApi)
	njetApi.vs = newVsClient(njetApi)

	return njetApi
}

func (c *NjetApi) SendRequest(data *njetapihttp.RestRequest) (int, []byte, error) {
	if data == nil {
		return 0, nil, fmt.Errorf("RestfulRequest is nil")
	}
	var pd io.Reader = nil
	client := c.httpclient.Httpclient
	if data.Body != nil {
		if data.Headers != nil {
			contentType, ok := (*data.Headers)["content-type"]
			if ok && strings.Compare(contentType, "application/json") != 0 {
				if _, ok := data.Body.([]byte); !ok {
					(*data.Headers)["content-type"] = "application/json"
				}
			}
		}
		if v, ok := data.Body.([]byte); ok {
			pd = bytes.NewReader(data.Body.([]byte))
			glog.V(2).Infof("HTTP Request: Njet resource type: %q, method: %s, url:%s,  http request payload:%+v", data.ResourceType, data.Method, data.RequestURL, string(v))
		} else {
			glog.V(2).Infof("HTTP Request: Njet resource type: %q, method: %s, url:%s,  http request payload:%+v", data.ResourceType, data.Method, data.RequestURL, data.Body)
		}
		if pd == nil {
			payload, err := json.Marshal(data.Body)
			if err != nil {
				glog.Errorf("Http client send request err, json.Marshal failed:%s", err)
				return 0, nil, err
			}
			glog.V(2).Infof("http client request body:%s", string(payload))
			pd = strings.NewReader(string(payload))
		}

	}

	method := data.Method
	url := data.RequestURL
	req, err := http.NewRequest(method, url, pd)
	if err != nil {
		return 0, nil, err
	}
	headers := map[string]string{}
	for hk, hv := range c.httpclient.Headers {
		headers[hk] = fmt.Sprintf("%v", hv)
	}
	for k, v := range headers {
		req.Header.Add(k, v)
	}
	if data.Headers != nil {
		for k, v := range *data.Headers {
			req.Header.Set(k, v)
		}
	}

	glog.V(2).Infof("http client request headers:%v", req.Header)
	res, err := client.Do(req)
	if err != nil {
		return 0, nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return res.StatusCode, nil, err
	}
	return res.StatusCode, body, nil
}

func (c *NjetApi) HandleResponse(restRequest *njetapihttp.RestRequest, statusCode int, body []byte) error {
	if restRequest != nil {
		if v, ok := restRequest.Body.([]byte); ok {
			glog.V(2).Infof("HTTP Response: Njet resource type: %q, method: %s, url:%s, http request payload:%+v, http code :%v , http response:%s", restRequest.ResourceType, restRequest.Method, restRequest.RequestURL, string(v),
				statusCode, string(body))
		} else {
			glog.V(2).Infof("HTTP Response: Njet resource type: %q, method: %s, url:%s, http request payload:%+v, http code :%v , http response:%s", restRequest.ResourceType, restRequest.Method, restRequest.RequestURL, restRequest.Body,
				statusCode, string(body))
		}
	}

	var err error
	if statusCode == 200 {
		switch restRequest.ResourceType {
		case njetapihttp.ConfigResource:
			return nil
		}

		apiBody := new(njetapihttp.CommonApiResponse)

		err = json.Unmarshal(body, apiBody)
		if err != nil {
			glog.Errorf("Unmarshal NjetApiBody error:%s, http status code is %d, and body is %q", err, statusCode, string(body))
			return fmt.Errorf("Unmarshal NjetApiBody error, http status code is %d, and body is %q", statusCode, string(body))
		}

		err = c.HandleNjetResouceResponse(restRequest, apiBody)
		if err != nil {
			glog.Error(err)
			return err
		}
	} else {
		glog.Errorf("http status code is %d, and body is %q", statusCode, string(body))
		return fmt.Errorf("http status code is %d, and body is %q", statusCode, string(body))
	}

	return nil
}

func (c *NjetApi) HandleNjetResouceResponse(req *njetapihttp.RestRequest, apiResponse *njetapihttp.CommonApiResponse) error {
	if req == nil {
		return fmt.Errorf("RestRequest is nil")
	}

	var err error
	switch req.ResourceType {
	case njetapihttp.RouteResource:
		err = RouteResponseHandle(apiResponse)
	case njetapihttp.UpstreamResource:
		err = UpstreamResponseHandle(apiResponse)
	case njetapihttp.PortRedirectResource:
		err = PortRedirectResponseHandle(apiResponse)
	case njetapihttp.StreamMapResource:
		err = StreamMapResponseHandle(apiResponse)
	case njetapihttp.VsResource:
		err = VsResponseHandle(apiResponse)
	case njetapihttp.AccessLogResource:
		err = AccessLogResponseHandle(apiResponse)
	default:
		glog.Errorf("no support NjetApiResouceType:%q", req.ResourceType)
		err = fmt.Errorf("no support NjetApiResouceType:%q", req.ResourceType)
	}

	return err
}
