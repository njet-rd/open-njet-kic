package controller

import (
	"net/http"

	"github.com/golang/glog"
	njetapihttp "github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller/http"
)

var NjetWaitCh chan error

func newConfigClient(c *NjetApi) *ConfigImp {
	return &ConfigImp{
		urlPrefix: c.baseURL + "/api/v1/config",
		njetApi:   c,
	}
}

type ConfigImp struct {
	urlPrefix string
	njetApi   *NjetApi
}

func (r *ConfigImp) Get() error {
	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.ConfigResource,
		Method:       http.MethodGet,
		RequestURL:   r.urlPrefix,
		Retries:      0,
	}

	code, body, err := r.njetApi.SendRequest(restRequest)
	if err == nil {
		err = r.njetApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func (r *ConfigImp) Ready() error {
	//send PUT to bogus module, if njet is ready, it should return 200 with msg "no rpc handler registered"
	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.ConfigResource,
		Method:       http.MethodPut,
		RequestURL:   r.urlPrefix + "/notExistedModule",
		Body:         `{"msg":"ping"}`,
		Retries:      0,
	}

	code, body, err := r.njetApi.SendRequest(restRequest)
	if err == nil {
		err = r.njetApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}
