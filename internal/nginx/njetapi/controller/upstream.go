package controller

import (
	"fmt"
	"github.com/golang/glog"
	njetapihttp "github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller/http"
)

type NjetUpstreamApi struct {
}

func newUpstreamClient(c *NjetApi) *UpstreamImp {
	return &UpstreamImp{
		urlPrefix: c.baseURL + "/dyn_upstream",
		njetApi:   c,
	}
}

type UpstreamImp struct {
	urlPrefix string
	njetApi   *NjetApi
}

func UpstreamResponseHandle(apiResponse *njetapihttp.CommonApiResponse) error {
	if apiResponse == nil {
		glog.Errorf("Njet Api Response nil")
		return fmt.Errorf("Njet Api Response nil")
	}

	switch apiResponse.Code {
	case RouteSuccessCode:
		return nil
	default:
		glog.Errorf("UpstreamResponseHandle: Njet Upstream Api failed: %+v", apiResponse)
		return fmt.Errorf("%s", apiResponse.Msg)
	}

	return nil
}
