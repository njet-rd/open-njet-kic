package controller

import (
	"net/http"

	"github.com/golang/glog"
	njetapihttp "github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller/http"
)

type NjetWorkerApi struct {
	Key   string `json:"key,omitempty"`
	Value string `json:"value,omitempty"`
}

func newWorkerClient(c *NjetApi) *WorkerImp {
	return &WorkerImp{
		urlPrefix: c.baseURL + "/api/v1/kv",
		njetApi:   c,
	}
}

type WorkerImp struct {
	urlPrefix string
	njetApi   *NjetApi
}

func (w *WorkerImp) Update(data *NjetWorkerApi) error {
	if data == nil {
		return nil
	}

	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.ConfigResource,
		Method:       http.MethodPost,
		RequestURL:   w.urlPrefix,
		Body:         data,
		Retries:      0,
	}

	code, body, err := w.njetApi.SendRequest(restRequest)
	if err == nil {
		err = w.njetApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}
