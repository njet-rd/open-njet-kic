/*
 * Copyright (C) 2021-2023  TMLake(Beijing) Technology Co., Ltd.
 * OpenNJet-KIC is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *        http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package controller

import (
	"fmt"
	"github.com/golang/glog"
	njetapihttp "github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller/http"
	"net/http"
)

const (
	StreamMapSuccessCode = 0
)

type NjetStreamMapApi struct {
	Maps []Mapping `json:"maps,omitempty"`
}

func newStreamMapClient(c *NjetApi) *StreamMapImp {
	return &StreamMapImp{
		urlPrefix: c.baseURL + "/api/v1/config/stream_dyn_map",
		njetApi:   c,
	}
}

type StreamMapImp struct {
	urlPrefix string
	njetApi   *NjetApi
}

func (pr *StreamMapImp) Create(data *NjetStreamMapApi) error {
	if data == nil {
		glog.Errorf("StreamMapImp:Create:data is nil")
		return fmt.Errorf("StreamMapImp:Create:data is nil")
	}

	restRequest := &njetapihttp.RestRequest{
		ResourceType: njetapihttp.StreamMapResource,
		Method:       http.MethodPut,
		RequestURL:   pr.urlPrefix,
		Body:         data,
		Retries:      0,
	}

	code, body, err := pr.njetApi.SendRequest(restRequest)
	if err == nil {
		err = pr.njetApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func StreamMapResponseHandle(apiResponse *njetapihttp.CommonApiResponse) error {
	if apiResponse == nil {
		glog.Errorf("StreamMapResponseHandle:Njet Api Response nil")
		return fmt.Errorf("Njet Api Response nil")
	}

	switch apiResponse.Code {
	case StreamMapSuccessCode:
		return nil
	default:
		glog.Errorf("StreamMapResponseHandle: Njet StreamMap Api failed: %+v", apiResponse)
		return fmt.Errorf("%s", apiResponse.Msg)
	}

	return nil
}
