# configuration for {{.Ingress.Namespace}}/{{.Ingress.Name}}
#{{range $upstream := .Upstreams}}
#upstream {{$upstream.Name}} {
#	{{if ne $upstream.UpstreamZoneSize "0"}}zone {{$upstream.Name}} {{$upstream.UpstreamZoneSize}};{{end}}
#	{{if $upstream.LBMethod }}{{$upstream.LBMethod}};{{end}}
#	{{range $server := $upstream.UpstreamServers}}
#	server {{$server.Address}} max_fails={{$server.MaxFails}} fail_timeout={{$server.FailTimeout}} max_conns={{$server.MaxConns}};{{end}}
#	{{if $.Keepalive}}keepalive {{$.Keepalive}};{{end}}
#}{{end}}

{{range $server := .Servers}}
server {
    listen 80;
    listen [::]:80;
    listen 443 ssl;
    listen [::]:443 ssl;
    server_name {{$server.Name}};

	{{range $location := $server.Locations}}
	location ( $uri {{$location.Path}} ) {
	    log_by_lua_block {
	        balancer.log()
	    }

	    set $resource_type "ingress";
	    {{if $location.MinionIngress}}
	    # location for minion {{$location.MinionIngress.Namespace}}/{{$location.MinionIngress.Name}}
	    set $resource_name "{{$location.MinionIngress.Name}}";
	    set $resource_namespace "{{$location.MinionIngress.Namespace}}";
	    {{else}}
	    set $resource_name "{{$.Ingress.Name}}";
	    set $resource_namespace "{{$.Ingress.Namespace}}";
	    {{end}}
	    set $service "{{$location.ServiceName}}";

	    set $balancer_ewma_score -1;
	    set $proxy_upstream_name {{ $location.Upstream.Name | quote }};
	    set $proxy_alternative_upstream_name "";

	    proxy_http_version 1.1;
	    {{if $location.Websocket}}
	    proxy_set_header Upgrade $http_upgrade;
	    proxy_set_header Connection $connection_upgrade;
	    {{- end}}

		proxy_connect_timeout {{$location.ProxyConnectTimeout}};
		proxy_read_timeout {{$location.ProxyReadTimeout}};
		proxy_send_timeout {{$location.ProxySendTimeout}};
		client_max_body_size {{$location.ClientMaxBodySize}};
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Forwarded-Host $host;
		proxy_set_header X-Forwarded-Port $server_port;
		proxy_set_header X-Forwarded-Proto {{if $server.RedirectToHTTPS}}https{{else}}$scheme{{end}};
		{{- range $h := $server.ProxySetHeaders}}
        proxy_set_header {{$h.Name}} "{{$h.Value}}";
        {{- end}}
        {{- range $proxyHideHeader := $server.ProxyHideHeaders}}
        proxy_hide_header {{$proxyHideHeader}};
        {{- end}}
        {{- range $proxyPassHeader := $server.ProxyPassHeaders}}
        proxy_pass_header {{$proxyPassHeader}};
        {{- end}}
        {{- with $server.ProxyIgnoreHeaders }}
        proxy_ignore_headers  {{ $server.ProxyIgnoreHeaders }};
        {{- end }}
        {{- range $h := $server.AddHeaders }}
        add_header {{ $h.Name }} "{{ $h.Value }}" always;
        {{- end }}
		proxy_buffering {{if $location.ProxyBuffering}}on{{else}}off{{end}};

		{{if $location.SSL}}
		#proxy_pass https://{{$location.Upstream.Name}}{{$location.Rewrite}};
		proxy_pass http://upstream_balancer;
		{{else}}
		#proxy_pass http://{{$location.Upstream.Name}}{{$location.Rewrite}};
		proxy_pass http://upstream_balancer;
		{{end}}
	}
	{{end}}
}
{{end}}
