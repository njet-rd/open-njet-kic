package version1

import (
	"fmt"
	"k8s.io/klog/v2"
	"sort"
	"strings"
	"text/template"
)

func split(s string, delim string) []string {
	return strings.Split(s, delim)
}

func trim(s string) string {
	return strings.TrimSpace(s)
}

func quote(input interface{}) string {
	var inputStr string
	switch input := input.(type) {
	case string:
		inputStr = input
	case fmt.Stringer:
		inputStr = input.String()
	case *string:
		inputStr = *input
	default:
		inputStr = fmt.Sprintf("%v", input)
	}
	return fmt.Sprintf("%q", inputStr)
}

var helperFunctions = template.FuncMap{
	"split": split,
	"trim":  trim,
	"quote": quote,
}

var funcMap = template.FuncMap{
	"buildLuaSharedDictionaries":      buildLuaSharedDictionaries,
	"luaConfigurationRequestBodySize": luaConfigurationRequestBodySize,
}

func dictKbToStr(size int) string {
	if size%1024 == 0 {
		return fmt.Sprintf("%dM", size/1024)
	}
	return fmt.Sprintf("%dK", size)
}

func buildLuaSharedDictionaries(c interface{}) string {
	var out []string

	cfg, ok := c.(*MainConfig)
	if !ok {
		klog.Errorf("expected a '*MainConfig' type but %T was returned", c)
		return ""
	}

	for name, size := range cfg.LuaSharedDicts {
		sizeStr := dictKbToStr(size)
		out = append(out, fmt.Sprintf("lua_shared_dict %s %s", name, sizeStr))
	}

	sort.Strings(out)

	return strings.Join(out, ";\n") + ";\n"
}

func luaConfigurationRequestBodySize(c interface{}) string {
	cfg, ok := c.(*MainConfig)
	if !ok {
		klog.Errorf("expected a '*MainConfig' type but %T was returned", c)
		return "100M" // just a default number
	}

	size := cfg.LuaSharedDicts["configuration_data"]
	if size < cfg.LuaSharedDicts["certificate_data"] {
		size = cfg.LuaSharedDicts["certificate_data"]
	}
	size = size + 1024

	return dictKbToStr(size)
}
