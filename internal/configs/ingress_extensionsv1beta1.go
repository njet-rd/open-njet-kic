package configs

import (
	"fmt"
	"github.com/golang/glog"
	"github.com/nginxinc/kubernetes-ingress/internal/configs/version1"
	internalk8singress "github.com/nginxinc/kubernetes-ingress/internal/k8s/ingress"
	"github.com/nginxinc/kubernetes-ingress/internal/k8s/secrets"
	api_v1 "k8s.io/api/core/v1"
	extensionsv1beta1 "k8s.io/api/extensions/v1beta1"
	networking "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"strings"
)

func getNameForUpstreamForIngressVersionExtensionsV1beta1(ing *extensionsv1beta1.Ingress, host string, backend *extensionsv1beta1.IngressBackend) string {
	return fmt.Sprintf("%v-%v-%v-%v-%v", ing.Namespace, ing.Name, host, backend.ServiceName, backend.ServicePort.String())
}

func createUpstreamForIngressVersionExtensionsV1beta1(ingEx *IngressEx, name string, backend *extensionsv1beta1.IngressBackend, stickyCookie string, cfg *ConfigParams,
	isPlus bool, isResolverConfigured bool, isLatencyMetricsEnabled bool,
) version1.Upstream {
	var ups version1.Upstream
	labels := version1.UpstreamLabels{
		Service:           backend.ServiceName,
		ResourceType:      "ingress",
		ResourceName:      ingEx.Ingress.ObjectMeta().Name,
		ResourceNamespace: ingEx.Ingress.ObjectMeta().Namespace,
	}
	if isPlus {
		queue, timeout := upstreamRequiresQueue(backend.ServiceName+backend.ServicePort.String(), ingEx, cfg)
		ups = version1.Upstream{Name: name, StickyCookie: stickyCookie, Queue: queue, QueueTimeout: timeout, UpstreamLabels: labels}
	} else {
		ups = version1.NewUpstreamWithDefaultServer(name)
		if isLatencyMetricsEnabled {
			ups.UpstreamLabels = labels
		}
	}

	endps, exists := ingEx.Endpoints[backend.ServiceName+backend.ServicePort.String()]
	if exists {
		var upsServers []version1.UpstreamServer
		// Always false for NGINX OSS
		_, isExternalNameSvc := ingEx.ExternalNameSvcs[backend.ServiceName]
		if isExternalNameSvc && !isResolverConfigured {
			glog.Warningf("A resolver must be configured for Type ExternalName service %s, no upstream servers will be created", backend.ServiceName)
			endps = []string{}
		}

		for _, endp := range endps {
			upsServers = append(upsServers, version1.UpstreamServer{
				Address:     endp,
				MaxFails:    cfg.MaxFails,
				MaxConns:    cfg.MaxConns,
				FailTimeout: cfg.FailTimeout,
				SlowStart:   cfg.SlowStart,
				Resolve:     isExternalNameSvc,
			})
		}
		if len(upsServers) > 0 {
			ups.UpstreamServers = upsServers
		}
	}

	ups.LBMethod = cfg.LBMethod
	ups.UpstreamZoneSize = cfg.UpstreamZoneSize
	ups.UpstreamHashBy = cfg.UpstreamHashBy
	return ups
}

func addSSLConfigForIngressVersionExtensionsV1beta1(server *version1.Server, owner runtime.Object, host string, ingressTLS []extensionsv1beta1.IngressTLS,
	secretRefs map[string]*secrets.SecretReference, isWildcardEnabled bool,
) Warnings {
	warnings := newWarnings()

	var tlsEnabled bool
	var tlsSecret string

	for _, tls := range ingressTLS {
		for _, h := range tls.Hosts {
			if h == host {
				tlsEnabled = true
				tlsSecret = tls.SecretName
				break
			}
		}
	}

	if !tlsEnabled {
		return warnings
	}

	var pemFile string
	var rejectHandshake bool

	if tlsSecret != "" {
		secretRef := secretRefs[tlsSecret]
		var secretType api_v1.SecretType
		if secretRef.Secret != nil {
			secretType = secretRef.Secret.Type
		}
		if secretType != "" && secretType != api_v1.SecretTypeTLS {
			rejectHandshake = true
			warnings.AddWarningf(owner, "TLS secret %s is of a wrong type '%s', must be '%s'", tlsSecret, secretType, api_v1.SecretTypeTLS)
		} else if secretRef.Error != nil {
			rejectHandshake = true
			warnings.AddWarningf(owner, "TLS secret %s is invalid: %v", tlsSecret, secretRef.Error)
		} else {
			pemFile = secretRef.Path
		}
	} else if isWildcardEnabled {
		pemFile = pemFileNameForWildcardTLSSecret
	} else {
		rejectHandshake = true
		warnings.AddWarningf(owner, "TLS termination for host '%s' requires specifying a TLS secret or configuring a global wildcard TLS secret", host)
	}

	server.SSL = true
	server.SSLCertificate = pemFile
	server.SSLCertificateKey = pemFile
	server.SSLRejectHandshake = rejectHandshake

	return warnings
}

func getNameForRedirectLocationForIngressVersionExtensionsV1beta1(ing *extensionsv1beta1.Ingress) string {
	return fmt.Sprintf("@login_url_%v-%v", ing.Namespace, ing.Name)
}

func generateIngressPathForIngressVersionExtensionsV1beta1(path string, pathType *extensionsv1beta1.PathType) string {
	if pathType == nil {
		return "~ ^" + path
	}
	if *pathType == extensionsv1beta1.PathTypeExact {
		return "= " + path
	}

	return "~ ^" + path
}

func createLocationForIngressVersionExtensionsV1beta1(path string, upstream version1.Upstream, cfg *ConfigParams, websocket bool, rewrite string, ssl bool, grpc bool, proxySSLName string, pathType *extensionsv1beta1.PathType, serviceName string) version1.Location {
	loc := version1.Location{
		Path:                 generateIngressPathForIngressVersionExtensionsV1beta1(path, pathType),
		Upstream:             upstream,
		ProxyConnectTimeout:  cfg.ProxyConnectTimeout,
		ProxyReadTimeout:     cfg.ProxyReadTimeout,
		ProxySendTimeout:     cfg.ProxySendTimeout,
		ClientMaxBodySize:    cfg.ClientMaxBodySize,
		Websocket:            websocket,
		Rewrite:              rewrite,
		SSL:                  ssl,
		GRPC:                 grpc,
		ProxyBuffering:       cfg.ProxyBuffering,
		ProxyBuffers:         cfg.ProxyBuffers,
		ProxyBufferSize:      cfg.ProxyBufferSize,
		ProxyMaxTempFileSize: cfg.ProxyMaxTempFileSize,
		ProxySSLName:         proxySSLName,
		LocationSnippets:     cfg.LocationSnippets,
		ServiceName:          serviceName,
	}

	return loc
}

func generateNginxCfgForIngressVersionExtensionsV1beta1(ingEx *IngressEx, apResources *AppProtectResources, dosResource *appProtectDosResource, isMinion bool,
	baseCfgParams *ConfigParams, isPlus bool, isResolverConfigured bool, staticParams *StaticConfigParams, isWildcardEnabled bool,
) (version1.IngressNginxConfig, Warnings) {
	if ingEx.Ingress.GroupVersion() != internalk8singress.IngressVersionExtensionsV1beta1 {
		return version1.IngressNginxConfig{}, nil
	}

	hasAppProtect := staticParams.MainAppProtectLoadModule
	hasAppProtectDos := staticParams.MainAppProtectDosLoadModule

	cfgParams := parseAnnotations(ingEx, baseCfgParams, isPlus, hasAppProtect, hasAppProtectDos, staticParams.EnableInternalRoutes)

	wsServices := getWebsocketServices(ingEx)
	spServices := getSessionPersistenceServices(ingEx)
	rewrites := getRewrites(ingEx)
	sslServices := getSSLServices(ingEx)
	grpcServices := getGrpcServices(ingEx)

	upstreams := make(map[string]version1.Upstream)
	healthChecks := make(map[string]version1.HealthCheck)

	// HTTP2 is required for gRPC to function
	if len(grpcServices) > 0 && !cfgParams.HTTP2 {
		glog.Errorf("Ingress %s/%s: annotation nginx.org/grpc-services requires HTTP2, ignoring", ingEx.Ingress.ObjectMeta().Namespace, ingEx.Ingress.ObjectMeta().Name)
		grpcServices = make(map[string]bool)
	}

	ingress := ingEx.Ingress.ExtensionsV1beta1()
	if ingress.Spec.Backend != nil {
		name := getNameForUpstreamForIngressVersionExtensionsV1beta1(ingress, emptyHost, ingress.Spec.Backend)
		upstream := createUpstreamForIngressVersionExtensionsV1beta1(ingEx, name, ingress.Spec.Backend, spServices[ingress.Spec.Backend.ServiceName], &cfgParams,
			isPlus, isResolverConfigured, staticParams.EnableLatencyMetrics)
		upstreams[name] = upstream

		if cfgParams.HealthCheckEnabled {
			if hc, exists := ingEx.HealthChecks[ingress.Spec.Backend.ServiceName+ingress.Spec.Backend.ServicePort.String()]; exists {
				healthChecks[name] = createHealthCheck(hc, name, &cfgParams)
			}
		}
	}

	allWarnings := newWarnings()

	var servers []version1.Server

	for _, rule := range ingress.Spec.Rules {
		// skipping invalid hosts
		if !ingEx.ValidHosts[rule.Host] {
			continue
		}

		httpIngressRuleValue := rule.HTTP

		if httpIngressRuleValue == nil {
			// the code in this loop expects non-nil
			httpIngressRuleValue = &extensionsv1beta1.HTTPIngressRuleValue{}
		}

		serverName := rule.Host

		statusZone := rule.Host

		server := version1.Server{
			Name:                  serverName,
			ServerTokens:          cfgParams.ServerTokens,
			HTTP2:                 cfgParams.HTTP2,
			RedirectToHTTPS:       cfgParams.RedirectToHTTPS,
			SSLRedirect:           cfgParams.SSLRedirect,
			ProxyProtocol:         cfgParams.ProxyProtocol,
			HSTS:                  cfgParams.HSTS,
			HSTSMaxAge:            cfgParams.HSTSMaxAge,
			HSTSIncludeSubdomains: cfgParams.HSTSIncludeSubdomains,
			HSTSBehindProxy:       cfgParams.HSTSBehindProxy,
			StatusZone:            statusZone,
			RealIPHeader:          cfgParams.RealIPHeader,
			SetRealIPFrom:         cfgParams.SetRealIPFrom,
			RealIPRecursive:       cfgParams.RealIPRecursive,
			ProxyIgnoreHeaders:    strings.Join(cfgParams.ProxyIgnoreHeaders, " "),
			ProxyHideHeaders:      cfgParams.ProxyHideHeaders,
			ProxyPassHeaders:      cfgParams.ProxyPassHeaders,
			ProxySetHeaders:       make([]version1.Header, 0),
			AddHeaders:            make([]version1.Header, 0),
			ServerSnippets:        cfgParams.ServerSnippets,
			Ports:                 cfgParams.Ports,
			SSLPorts:              cfgParams.SSLPorts,
			TLSPassthrough:        staticParams.TLSPassthrough,
			AppProtectEnable:      cfgParams.AppProtectEnable,
			AppProtectLogEnable:   cfgParams.AppProtectLogEnable,
			SpiffeCerts:           cfgParams.SpiffeServerCerts,
			DisableIPV6:           staticParams.DisableIPV6,
		}

		initProxySetHeaders(&server.ProxySetHeaders, &server.AddHeaders, &cfgParams)
		warnings := addSSLConfigForIngressVersionExtensionsV1beta1(&server, ingEx.Ingress.Inress(), rule.Host, ingress.Spec.TLS, ingEx.SecretRefs, isWildcardEnabled)
		allWarnings.Add(warnings)

		if hasAppProtect {
			server.AppProtectPolicy = apResources.AppProtectPolicy
			server.AppProtectLogConfs = apResources.AppProtectLogconfs
		}

		if hasAppProtectDos && dosResource != nil {
			server.AppProtectDosEnable = dosResource.AppProtectDosEnable
			server.AppProtectDosLogEnable = dosResource.AppProtectDosLogEnable
			server.AppProtectDosMonitorURI = dosResource.AppProtectDosMonitorURI
			server.AppProtectDosMonitorProtocol = dosResource.AppProtectDosMonitorProtocol
			server.AppProtectDosMonitorTimeout = dosResource.AppProtectDosMonitorTimeout
			server.AppProtectDosName = dosResource.AppProtectDosName
			server.AppProtectDosAccessLogDst = dosResource.AppProtectDosAccessLogDst
			server.AppProtectDosPolicyFile = dosResource.AppProtectDosPolicyFile
			server.AppProtectDosLogConfFile = dosResource.AppProtectDosLogConfFile
		}

		if !isMinion && cfgParams.JWTKey != "" {
			jwtAuth, redirectLoc, warnings := generateJWTConfig(ingEx.Ingress.Inress(), ingEx.SecretRefs, &cfgParams, getNameForRedirectLocationForIngressVersionExtensionsV1beta1(ingress))
			server.JWTAuth = jwtAuth
			if redirectLoc != nil {
				server.JWTRedirectLocations = append(server.JWTRedirectLocations, *redirectLoc)
			}
			allWarnings.Add(warnings)
		}

		if !isMinion && cfgParams.BasicAuthSecret != "" {
			basicAuth, warnings := generateBasicAuthConfig(ingEx.Ingress.Inress(), ingEx.SecretRefs, &cfgParams)
			server.BasicAuth = basicAuth
			allWarnings.Add(warnings)
		}

		var locations []version1.Location
		healthChecks := make(map[string]version1.HealthCheck)

		rootLocation := false

		grpcOnly := true
		if len(grpcServices) > 0 {
			for _, path := range httpIngressRuleValue.Paths {
				if _, exists := grpcServices[path.Backend.ServiceName]; !exists {
					grpcOnly = false
					break
				}
			}
		} else {
			grpcOnly = false
		}

		for _, path := range httpIngressRuleValue.Paths {
			// skip invalid paths for minions
			if isMinion && !ingEx.ValidMinionPaths[path.Path] {
				continue
			}

			upsName := getNameForUpstreamForIngressVersionExtensionsV1beta1(ingress, rule.Host, &path.Backend)

			if cfgParams.HealthCheckEnabled {
				if hc, exists := ingEx.HealthChecks[path.Backend.ServiceName+path.Backend.ServicePort.String()]; exists {
					healthChecks[upsName] = createHealthCheck(hc, upsName, &cfgParams)
				}
			}

			if _, exists := upstreams[upsName]; !exists {
				upstream := createUpstreamForIngressVersionExtensionsV1beta1(ingEx, upsName, &path.Backend, spServices[path.Backend.ServiceName], &cfgParams, isPlus, isResolverConfigured, staticParams.EnableLatencyMetrics)
				upstreams[upsName] = upstream
			}

			ssl := isSSLEnabled(sslServices[path.Backend.ServiceName], cfgParams, staticParams)
			proxySSLName := generateProxySSLName(path.Backend.ServiceName, ingEx.Ingress.ObjectMeta().Namespace)
			loc := createLocationForIngressVersionExtensionsV1beta1(pathOrDefault(path.Path), upstreams[upsName], &cfgParams, wsServices[path.Backend.ServiceName], rewrites[path.Backend.ServiceName],
				ssl, grpcServices[path.Backend.ServiceName], proxySSLName, path.PathType, path.Backend.ServiceName)

			if isMinion && cfgParams.JWTKey != "" {
				jwtAuth, redirectLoc, warnings := generateJWTConfig(ingEx.Ingress.Inress(), ingEx.SecretRefs, &cfgParams, getNameForRedirectLocationForIngressVersionExtensionsV1beta1(ingress))
				loc.JWTAuth = jwtAuth
				if redirectLoc != nil {
					server.JWTRedirectLocations = append(server.JWTRedirectLocations, *redirectLoc)
				}
				allWarnings.Add(warnings)
			}

			if isMinion && cfgParams.BasicAuthSecret != "" {
				basicAuth, warnings := generateBasicAuthConfig(ingEx.Ingress.Inress(), ingEx.SecretRefs, &cfgParams)
				loc.BasicAuth = basicAuth
				allWarnings.Add(warnings)
			}

			locations = append(locations, loc)

			if loc.Path == "/" {
				rootLocation = true
			}
		}

		if !rootLocation && ingress.Spec.Backend != nil {
			upsName := getNameForUpstreamForIngressVersionExtensionsV1beta1(ingress, emptyHost, ingress.Spec.Backend)
			ssl := isSSLEnabled(sslServices[ingress.Spec.Backend.ServiceName], cfgParams, staticParams)
			proxySSLName := generateProxySSLName(ingress.Spec.Backend.ServiceName, ingEx.Ingress.ObjectMeta().Namespace)
			pathtype := networking.PathTypePrefix

			loc := createLocation(pathOrDefault("/"), upstreams[upsName], &cfgParams, wsServices[ingress.Spec.Backend.ServiceName], rewrites[ingress.Spec.Backend.ServiceName],
				ssl, grpcServices[ingress.Spec.Backend.ServiceName], proxySSLName, &pathtype, ingress.Spec.Backend.ServiceName)
			locations = append(locations, loc)

			if cfgParams.HealthCheckEnabled {
				if hc, exists := ingEx.HealthChecks[ingress.Spec.Backend.ServiceName+ingress.Spec.Backend.ServicePort.String()]; exists {
					healthChecks[upsName] = createHealthCheck(hc, upsName, &cfgParams)
				}
			}

			if _, exists := grpcServices[ingress.Spec.Backend.ServiceName]; !exists {
				grpcOnly = false
			}
		}

		server.Locations = locations
		server.HealthChecks = healthChecks
		server.GRPCOnly = grpcOnly

		servers = append(servers, server)
	}

	var keepalive string
	if cfgParams.Keepalive > 0 {
		keepalive = fmt.Sprint(cfgParams.Keepalive)
	}

	return version1.IngressNginxConfig{
		Upstreams: upstreamMapToSlice(upstreams),
		Servers:   servers,
		Keepalive: keepalive,
		Ingress: version1.Ingress{
			Name:        ingEx.Ingress.ObjectMeta().Name,
			Namespace:   ingEx.Ingress.ObjectMeta().Namespace,
			Annotations: ingEx.Ingress.ObjectMeta().Annotations,
		},
		SpiffeClientCerts: staticParams.NginxServiceMesh && !cfgParams.SpiffeServerCerts,
	}, allWarnings
}

func generateNginxCfgForMergeableIngressesForIngressVersionExtensionsV1beta1(mergeableIngs *MergeableIngresses, apResources *AppProtectResources,
	dosResource *appProtectDosResource, baseCfgParams *ConfigParams, isPlus bool, isResolverConfigured bool,
	staticParams *StaticConfigParams, isWildcardEnabled bool,
) (version1.IngressNginxConfig, Warnings) {
	if mergeableIngs == nil || mergeableIngs.Master.Ingress.GroupVersion() != internalk8singress.IngressVersionExtensionsV1beta1 {
		return version1.IngressNginxConfig{}, nil
	}
	var masterServer version1.Server
	var locations []version1.Location
	var upstreams []version1.Upstream
	healthChecks := make(map[string]version1.HealthCheck)
	var keepalive string

	// replace master with a deepcopy because we will modify it
	originalMaster := mergeableIngs.Master.Ingress
	mergeableIngs.Master.Ingress, _ = internalk8singress.NewIngressWrap(mergeableIngs.Master.Ingress.V1beta1().DeepCopy())

	removedAnnotations := filterMasterAnnotations(mergeableIngs.Master.Ingress.ObjectMeta().Annotations)
	if len(removedAnnotations) != 0 {
		glog.Errorf("Ingress Resource %v/%v with the annotation 'njet.org.cn/mergeable-ingress-type' set to 'master' cannot contain the '%v' annotation(s). They will be ignored",
			mergeableIngs.Master.Ingress.ObjectMeta().Namespace, mergeableIngs.Master.Ingress.ObjectMeta().Name, strings.Join(removedAnnotations, ","))
	}
	isMinion := false

	masterNginxCfg, warnings := generateNginxCfgForIngressVersionNetworkingV1beta1(mergeableIngs.Master, apResources, dosResource, isMinion, baseCfgParams, isPlus, isResolverConfigured, staticParams, isWildcardEnabled)

	// because mergeableIngs.Master.Ingress is a deepcopy of the original master
	// we need to change the key in the warnings to the original master
	if _, exists := warnings[mergeableIngs.Master.Ingress.V1beta1()]; exists {
		warnings[originalMaster.V1beta1()] = warnings[mergeableIngs.Master.Ingress.V1beta1()]
		delete(warnings, mergeableIngs.Master.Ingress.V1beta1())
	}

	masterServer = masterNginxCfg.Servers[0]
	masterServer.Locations = []version1.Location{}

	upstreams = append(upstreams, masterNginxCfg.Upstreams...)

	if masterNginxCfg.Keepalive != "" {
		keepalive = masterNginxCfg.Keepalive
	}

	minions := mergeableIngs.Minions
	for _, minion := range minions {
		// replace minion with a deepcopy because we will modify it
		originalMinion := minion.Ingress
		minion.Ingress, _ = internalk8singress.NewIngressWrap(minion.Ingress.V1beta1().DeepCopy())

		// Remove the default backend so that "/" will not be generated
		minion.Ingress.V1beta1().Spec.Backend = nil

		// Add acceptable master annotations to minion
		mergeMasterAnnotationsIntoMinion(minion.Ingress.ObjectMeta().Annotations, mergeableIngs.Master.Ingress.ObjectMeta().Annotations)

		removedAnnotations = filterMinionAnnotations(minion.Ingress.ObjectMeta().Annotations)
		if len(removedAnnotations) != 0 {
			glog.Errorf("Ingress Resource %v/%v with the annotation 'njet.org.cn/mergeable-ingress-type' set to 'minion' cannot contain the %v annotation(s). They will be ignored",
				minion.Ingress.ObjectMeta().Namespace, minion.Ingress.ObjectMeta().Name, strings.Join(removedAnnotations, ","))
		}

		isMinion := true
		// App Protect Resources not allowed in minions - pass empty struct
		dummyApResources := &AppProtectResources{}
		dummyDosResource := &appProtectDosResource{}
		nginxCfg, minionWarnings := generateNginxCfgForIngressVersionNetworkingV1beta1(minion, dummyApResources, dummyDosResource, isMinion, baseCfgParams, isPlus, isResolverConfigured, staticParams, isWildcardEnabled)
		warnings.Add(minionWarnings)

		// because minion.Ingress is a deepcopy of the original minion
		// we need to change the key in the warnings to the original minion
		if _, exists := warnings[minion.Ingress.V1beta1()]; exists {
			warnings[originalMinion.V1beta1()] = warnings[minion.Ingress.V1beta1()]
			delete(warnings, minion.Ingress.V1beta1())
		}

		for _, server := range nginxCfg.Servers {
			for _, loc := range server.Locations {
				loc.MinionIngress = &nginxCfg.Ingress
				locations = append(locations, loc)
			}
			for hcName, healthCheck := range server.HealthChecks {
				healthChecks[hcName] = healthCheck
			}
			masterServer.JWTRedirectLocations = append(masterServer.JWTRedirectLocations, server.JWTRedirectLocations...)
		}

		upstreams = append(upstreams, nginxCfg.Upstreams...)
	}

	masterServer.HealthChecks = healthChecks
	masterServer.Locations = locations

	return version1.IngressNginxConfig{
		Servers:           []version1.Server{masterServer},
		Upstreams:         upstreams,
		Keepalive:         keepalive,
		Ingress:           masterNginxCfg.Ingress,
		SpiffeClientCerts: staticParams.NginxServiceMesh && !baseCfgParams.SpiffeServerCerts,
	}, warnings
}
