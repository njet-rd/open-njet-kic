package njetdynapi

import (
	"fmt"
	"strings"

	"github.com/golang/glog"
	"github.com/nginxinc/kubernetes-ingress/internal/configs/version2"
	"github.com/nginxinc/kubernetes-ingress/internal/model"
	"github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller"
)

type locationBodyTemplateForVirtualServer struct {
	Server   *version2.Server
	Location *version2.Location
}

type locationBodyTemplateForVirtualServerActionReturn struct {
	Server   *version2.Server
	Location *version2.ReturnLocation
}

type VirtualServerGenerator struct {
	Type                string
	TemplateExecutor    *TemplateExecutor
	VirtualServerConfig map[string]*version2.VirtualServerConfig
}

func NewVirtualServerGenerator(resourceType string) *VirtualServerGenerator {
	virtualServerGeneratorGenerator := &VirtualServerGenerator{
		Type:                resourceType,
		VirtualServerConfig: map[string]*version2.VirtualServerConfig{},
	}
	t, err := NewTemplateExecutor()
	if err != nil {
		glog.Fatalf("Error creating TemplateExecutor: %v", err)
	}
	virtualServerGeneratorGenerator.TemplateExecutor = t
	return virtualServerGeneratorGenerator
}

func generateVSKey(server version2.Server) string {
	return fmt.Sprintf("vs_%s_%s", server.VSNamespace, server.VSName)
}

func (vs *VirtualServerGenerator) FindVirtualServerConfig(vsKey string) (*version2.VirtualServerConfig, error) {
	if vsConfig, ok := vs.VirtualServerConfig[vsKey]; ok {
		return vsConfig, nil
	}
	glog.Errorf("VirtualServerGenerator not found VirtualServerConfig")
	return nil, fmt.Errorf("VirtualServerGenerator not found VirtualServerConfig")
}

func (vs *VirtualServerGenerator) Generate(push *model.PushNjetApiResourceContext) (*model.NjetApiContextWrap, error) {
	if push == nil || push.K8sResourceInfo.RType.String() != vs.Type || push.VirtualServerConfig == nil {
		return nil, fmt.Errorf("PushContext is nil")
	}

	njetApiContext := &model.NjetApiContext{}
	oldNjetApiContext := &model.NjetApiContext{}

	// handle version2.VirtualServerConfig
	err := vs.buildRoutes(njetApiContext, push.VirtualServerConfig)
	if err != nil {
		glog.Errorf("IngressGenerator failed to build Njet Route: %s, Skip this event", err)
		return nil, err
	}
	// create NJet VS
	njetApiContext.Vss, err = vs.buildVs(push.VirtualServerConfig)
	if err != nil {
		glog.Errorf("IngressGenerator failed to build Njet Vs: %s, Skip this event", err)
		return nil, err
	}
	var (
		added   = &model.NjetApiContext{}
		updated = &model.NjetApiContext{}
		deleted = &model.NjetApiContext{}
	)

	vsKey := generateVSKey(push.VirtualServerConfig.Server)

	if push.K8sResourceInfo.Operation == model.Delete {
		if len(njetApiContext.Vss) != 0 {
			// NJet VS need delete, and locations are also deleted
			deleted.Vss = njetApiContext.Vss
		}
	} else {
		//handle old VirtualServerConfig
		// full update, because dynamic locatioins are matched in order.
		// (Make advanced routes available.The advanced route is always first, because the loc array in VirtualServerConfig is like that)
		if push.VirtualServerConfig != nil {
			if _, ok := vs.VirtualServerConfig[vsKey]; ok {
				err = vs.buildRoutes(oldNjetApiContext, vs.VirtualServerConfig[vsKey])
				if err != nil {
					glog.Errorf("VirtualServerGenerator failed to build Njet Route: %s, Skip this event", err)
					return nil, err
				}
				oldNjetApiContext.Vss, err = vs.buildVs(vs.VirtualServerConfig[vsKey])
				if err != nil {
					glog.Errorf("VirtualServerGenerator failed to build Njet Vs: %s, Skip this event", err)
					return nil, err
				}
				if njetApiContext.HasDifferentRoute(oldNjetApiContext) || njetApiContext.HasDifferentVs(oldNjetApiContext) {
					if len(njetApiContext.Routes) != 0 {
						// full update
						added.Routes = njetApiContext.Routes
						deleted.Routes = oldNjetApiContext.Routes
					}
					// host may be modified in VS CR
					addedvs, _, deletedvs := njetApiContext.DifferentVs(oldNjetApiContext)
					if len(addedvs.Vss) != 0 {
						added.Vss = addedvs.Vss
					}
					if len(deletedvs.Vss) != 0 {
						// There is one and only one host in VS CR, so the deletion of vs means the deletion of route
						deleted.Vss = deletedvs.Vss
						deleted.Routes = nil
					}
				}
			} else {
				// Before operating location, create NJet VS
				if len(njetApiContext.Vss) != 0 {
					// Before operating location, make sure vs exists
					added.Vss = njetApiContext.Vss
				}
				if len(njetApiContext.Routes) != 0 {
					added.Routes = njetApiContext.Routes
				}
			}
		}

		//// full update, because dynamic locatioins are matched in order.
		//// (Make advanced routes available.The advanced route is always first, because the loc array in VirtualServerConfig is like that)
		//added = njetApiContext
		//deleted = oldNjetApiContext
	}

	wrap := &model.NjetApiContextWrap{
		Added:   added,
		Updated: updated,
		Deleted: deleted,
	}

	model.CloneVsAndRouteForHttps(wrap)
	// create NJet AceessLog
	njetApiContext.AccessLog, err = vs.buildAccessLog(push.VirtualServerConfig)
	if err != nil {
		glog.Errorf("VirtualServerGenerator failed to build Njet AceessLog: %s, Skip this event", err)
	}
	oldNjetApiContext.AccessLog, err = vs.buildAccessLog(vs.VirtualServerConfig[vsKey])
	if err != nil {
		glog.Errorf("VirtualServerGenerator failed to build Njet buildAccessLog: %s, Skip this event", err)
	}
	isDifferent := njetApiContext.DifferentAccessLog(oldNjetApiContext)
	if isDifferent {
		wrap.Updated.AccessLog = njetApiContext.AccessLog
	}

	model.CloneAccessLogForHttps(wrap)
	// update cache
	if push.VirtualServerConfig != nil {
		if push.K8sResourceInfo.Operation == model.Delete {
			delete(vs.VirtualServerConfig, vsKey)
		} else {
			vs.VirtualServerConfig[vsKey] = push.VirtualServerConfig
		}

	}

	certMapping := vs.buildCertMapping()
	if wrap.Updated != nil {
		wrap.Updated.CertMapping = certMapping
	} else {
		wrap.Updated = &model.NjetApiContext{
			CertMapping: certMapping,
		}
	}

	return wrap, nil
}

func (vs *VirtualServerGenerator) buildCertMapping() *controller.NjetMapApi {
	certMapping := &controller.NjetMapApi{
		Maps: []controller.Mapping{{
			KeyFrom:    "$ssl_server_name",
			KeyTo:      "$cert_name",
			Values:     make([]controller.ValueMapping, 0),
			IsVolatile: false,
			Hostnames:  true,
		}},
		MappingFor: controller.DYN_MAP_VS,
	}
	for _, vsConfig := range vs.VirtualServerConfig {
		server_ssl := vsConfig.Server.SSL
		if server_ssl == nil {
			continue
		}
		if vsConfig.Server.ServerName == "" {
			continue
		}
		//certificate file is pem format, crt and key are the same files
		certMapping.Maps[0].Values = append(certMapping.Maps[0].Values, controller.ValueMapping{
			ValueFrom: vsConfig.Server.ServerName,
			ValueTo:   server_ssl.Certificate,
		})
	}
	//add default mapping
	certMapping.Maps[0].Values = append(certMapping.Maps[0].Values, controller.ValueMapping{
		ValueFrom: "default",
		ValueTo:   "/etc/njet/secrets/default",
	})

	return certMapping
}

func (vs *VirtualServerGenerator) buildRoutes(njetApiContext *model.NjetApiContext, virtualServerConfig *version2.VirtualServerConfig) error {
	if njetApiContext == nil || virtualServerConfig == nil {
		return fmt.Errorf("parameter is nil")
	}

	njetRouteApi := vs.generateNjetRouteApi(&virtualServerConfig.Server)
	njetApiContext.Routes = append(njetApiContext.Routes, njetRouteApi...)

	return nil
}

func (vs *VirtualServerGenerator) generateNjetRouteApi(server *version2.Server) []*controller.NjetRouteApi {
	var njetRouteApis []*controller.NjetRouteApi
	for _, location := range server.Locations {
		njetRouteApi := &controller.NjetRouteApi{
			OperationType:  "",
			AddressAndPort: model.DynLocationAddressAndPort80,
			ServerName:     server.ServerName,
		}
		njetRouteApi.Locations = vs.generateGeneralLocations(server, &location)
		njetRouteApis = append(njetRouteApis, njetRouteApi)
	}

	for _, returnLoc := range server.ReturnLocations {
		njetRouteApiForNamedLocation := &controller.NjetRouteApi{
			OperationType:  "",
			AddressAndPort: model.DynLocationAddressAndPort80,
			ServerName:     server.ServerName,
		}
		njetRouteApiForNamedLocation.Locations = vs.generateNamedLocationsForActioinReturn(server, &returnLoc)
		njetRouteApis = append(njetRouteApis, njetRouteApiForNamedLocation)
	}

	return njetRouteApis
}

func (vs *VirtualServerGenerator) generateGeneralLocations(server *version2.Server, location *version2.Location) []controller.Location {
	var locations []controller.Location

	njetRouteApiLocation := controller.Location{
		LocationRule: "",
		LocationName: vs.generateNjetRouteApiLocationName(server, location),
		ProxyPass:    vs.generateNjetRouteApiProxyPass(location),
		LocationBody: vs.generateNjetRouteApiLocationBody(server, location),
	}
	locations = append(locations, njetRouteApiLocation)
	return locations
}

// vs: action.return
func (vs *VirtualServerGenerator) generateNamedLocationsForActioinReturn(server *version2.Server, returnLoc *version2.ReturnLocation) []controller.Location {
	var locations []controller.Location
	if returnLoc.Name == "" {
		return locations
	}

	njetRouteApiLocation := controller.Location{
		LocationRule: "",
		LocationName: returnLoc.Name,
		LocationBody: vs.generateNjetRouteApiLocationBodyForActioinReturn(server, returnLoc),
	}
	locations = append(locations, njetRouteApiLocation)
	return locations
}

func (vs *VirtualServerGenerator) generateNjetRouteApiLocationName(server *version2.Server, location *version2.Location) string {
	var matchs []string
	var multiConditions string
	//location ( $uri {{$l.OriginalPath}} {{ range $match := $l.MatcheConditions}} {{ range $con, $v := $match}} && {{$con}} = {{$v}} {{end}} {{end}})
	for _, matcheCondition := range location.MatcheConditions {
		for con, v := range matcheCondition {
			if con != "" && v != "" {
				matchs = append(matchs, fmt.Sprintf(" && %s = %s ", con, v))
			}
		}
	}

	if len(matchs) != 0 {
		multiConditions = strings.Join(matchs, "")
	}

	return fmt.Sprintf("( %s %s %s )", UriVariable, location.OriginalPath, multiConditions)
}

// 废弃，在更新accesslog时，会出错。引号括起来的正则，前引号前需要加空格，后引号后需要有空格。
// 直接使用动态location添加时的locationName
func (vs *VirtualServerGenerator) generateNjetRouteApiLocationNameForAccessLog(server *version2.Server, location *version2.Location) string {
	var accesslogPath string
	// generateVSPath(path string) string
	if location.OriginalPath == "" {
		accesslogPath = "/"
	}
	// Wrap the regular expression (if present) inside double quotes (") to avoid NGINX parsing errors
	if strings.HasPrefix(location.OriginalPath, "~* \"") {
		accesslogPath = fmt.Sprintf(`~*%v`, strings.TrimPrefix(strings.TrimPrefix(location.OriginalPath, "~*"), " "))
	}
	if strings.HasPrefix(location.OriginalPath, "~ \"") {
		accesslogPath = fmt.Sprintf(`~%v`, strings.TrimPrefix(strings.TrimPrefix(location.OriginalPath, "~ "), " "))
	}

	if strings.HasPrefix(location.OriginalPath, "~ ^") {
		accesslogPath = fmt.Sprintf(`~^%v`, strings.TrimPrefix(strings.TrimPrefix(location.OriginalPath, "~ ^"), " "))
	}

	if strings.HasPrefix(location.OriginalPath, "=") {
		accesslogPath = strings.Replace(location.OriginalPath, "= ", "=", 1)
	}

	var matchs []string
	var multiConditions string
	//location ( $uri {{$l.OriginalPath}} {{ range $match := $l.MatcheConditions}} {{ range $con, $v := $match}} && {{$con}} = {{$v}} {{end}} {{end}})
	for _, matcheCondition := range location.MatcheConditions {
		for con, v := range matcheCondition {
			if con != "" && v != "" {
				matchs = append(matchs, fmt.Sprintf("&&%s=%s", con, v))
			}
		}
	}

	if len(matchs) != 0 {
		multiConditions = strings.Join(matchs, "")
	}

	return fmt.Sprintf("(%s%s%s)", UriVariable, accesslogPath, multiConditions)
}

func (vs *VirtualServerGenerator) generateNjetRouteApiProxyPass(location *version2.Location) string {
	//proxy_pass {{ $l.ProxyPass }}{{ $l.ProxyPassRewrite }};
	//if location.ProxyPass != "" {
	//	return fmt.Sprintf("%s%s", location.ProxyPass, location.ProxyPassRewrite)
	//} else {
	//	return fmt.Sprintf("%s", location.GRPCPass)
	//}

	//{{ if $l.InternalProxyPass }}
	//proxy_pass {{ $l.InternalProxyPass }};
	//{{ end }}

	if location.InternalProxyPass != "" {
		// vs: action.redirect and action.return
		return fmt.Sprintf("%s", location.InternalProxyPass)
	} else {
		// lua dynamic upstream
		return fmt.Sprintf("%s%s", Protocol, UpstreamName)
		//return fmt.Sprintf("http://test-upstream")
	}
}

func (vs *VirtualServerGenerator) generateNjetRouteApiLocationBody(server *version2.Server, location *version2.Location) string {
	lBody := &locationBodyTemplateForVirtualServer{
		Location: location,
		Server:   server,
	}
	content, err := vs.TemplateExecutor.ExecuteVirtualServerToNJetApiRouteTemplate(lBody)
	if err != nil {
		glog.Fatalf("Error generating NjetRouteApi LocationBody: %v", err)
	}

	body := strings.Trim(string(content), "\n")
	body = strings.TrimSuffix(body, ";")

	glog.V(3).Infof("NjetRouteApi LocationBody:%s", body)
	return body
}

func (vs *VirtualServerGenerator) generateNjetRouteApiLocationBodyForActioinReturn(server *version2.Server, returnLoc *version2.ReturnLocation) string {
	lBody := &locationBodyTemplateForVirtualServerActionReturn{
		Location: returnLoc,
		Server:   server,
	}
	content, err := vs.TemplateExecutor.ExecuteVirtualServerActionReturnToNJetApiRouteTemplate(lBody)
	if err != nil {
		glog.Fatalf("Error generating NjetRouteApi LocationBody: %v", err)
	}

	body := strings.Trim(string(content), "\n")
	body = strings.TrimSuffix(body, ";")

	glog.V(3).Infof("NjetRouteApi LocationBody:%s", body)
	return body
}

func (vs *VirtualServerGenerator) buildVs(virtualServerConfig *version2.VirtualServerConfig) ([]*controller.NjetVsApi, error) {
	if virtualServerConfig == nil {
		return nil, fmt.Errorf("VirtualServerGenerator:buildVs:parameter is nil")
	}

	var njetVsApis []*controller.NjetVsApi
	njetVsApi := vs.generateNjetVsApi(&virtualServerConfig.Server)
	njetVsApis = append(njetVsApis, njetVsApi)
	return njetVsApis, nil
}

func (vs *VirtualServerGenerator) generateNjetVsApi(server *version2.Server) *controller.NjetVsApi {
	if server == nil {
		return nil
	}

	njetVsApi := &controller.NjetVsApi{
		OperationType:  "",
		AddressAndPort: model.DynLocationAddressAndPort80,
		ServerName:     server.ServerName,
		ServerBody:     "ssl_certificate $cert_name; ssl_certificate_key $cert_name;",
	}
	return njetVsApi
}

func (vs *VirtualServerGenerator) buildAccessLog(virtualServerConfig *version2.VirtualServerConfig) (*controller.AccessLogData, error) {
	if virtualServerConfig == nil {
		return nil, fmt.Errorf("VirtualServerGenerator:buildAccessLog:parameter is nil")
	}
	njetAccessLogApi := controller.AccessLogData{
		AccessLogFormats: []controller.AccessLogFormat{},
	}
	accessLogServer := vs.generateNjetAccessLogServer(&virtualServerConfig.Server)
	njetAccessLogApi.Servers = append(njetAccessLogApi.Servers, accessLogServer)

	return &njetAccessLogApi, nil
}

func (vs *VirtualServerGenerator) generateNjetAccessLogServer(server *version2.Server) controller.AccessLogServer {
	if server == nil {
		return controller.AccessLogServer{}
	}
	accessLogServer := controller.AccessLogServer{
		Listens:     []string{model.DynLocationAddressAndPort80},
		ServerNames: []string{server.ServerName},
		Locations:   vs.generateNjetAccessLogApiLocations(server),
	}
	return accessLogServer
}

func (vs *VirtualServerGenerator) generateNjetAccessLogApiLocations(server *version2.Server) []controller.AccessLogServerLocation {
	if server == nil {
		return []controller.AccessLogServerLocation{}
	}
	var accessLogServerLocations []controller.AccessLogServerLocation
	for _, location := range server.Locations {
		accessLogServerLocation := controller.AccessLogServerLocation{
			Location:    vs.generateNjetRouteApiLocationName(server, &location),
			AccessLogOn: !server.AccessLogOff,
		}
		if server.AccessLogOff {
			accessLogServerLocation.AccessLogs = nil
		} else {
			accessLogServerLocation.AccessLogs = make([]controller.AccessLogServerLocationLogs, 1)
			accessLogServerLocation.AccessLogs[0].FormatName = server.LogFormatName
			accessLogServerLocation.AccessLogs[0].Path = server.AccessLogPath
		}
		accessLogServerLocations = append(accessLogServerLocations, accessLogServerLocation)
	}

	return accessLogServerLocations
}
