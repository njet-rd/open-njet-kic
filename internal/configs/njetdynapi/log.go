package njetdynapi

import (
	"fmt"
	"strings"

	"github.com/nginxinc/kubernetes-ingress/internal/model"
	"github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller"
)

type LogGenerator struct {
	Type string
}

func NewLogGenerator(resourceType string) *LogGenerator {
	logGenerator := &LogGenerator{
		Type: resourceType,
	}
	return logGenerator
}

func (lg *LogGenerator) Generate(push *model.PushNjetApiResourceContext) (*model.NjetApiContextWrap, error) {
	if push == nil || push.K8sResourceInfo.RType.String() != lg.Type || push.LogConfig == nil {
		return nil, fmt.Errorf("PushContext is nil")
	}

	njetAccessLogApi := controller.AccessLogData{
		Servers: []controller.AccessLogServer{},
	}
	accessLogFormat := controller.AccessLogFormat{
		Name:   "main",
		Escape: push.LogConfig.Entrys["main"].LogFormatEscaping,
		Format: strings.Join(push.LogConfig.Entrys["main"].LogFormat, " "),
	}
	njetAccessLogApi.AccessLogFormats = append(njetAccessLogApi.AccessLogFormats, accessLogFormat)

	njetApiContext := &model.NjetApiContext{
		AccessLog: &njetAccessLogApi,
	}

	wrap := &model.NjetApiContextWrap{
		Added:   nil,
		Updated: njetApiContext,
		Deleted: nil,
	}

	return wrap, nil
}
