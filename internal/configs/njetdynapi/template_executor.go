package njetdynapi

import (
	"bytes"
	"fmt"
	njetdynapigenv2 "github.com/nginxinc/kubernetes-ingress/internal/configs/njetdynapi/version2"
	"path"
	"text/template"
)

const (
	IngressToNJetApiRouteTemplatePath                   = "ingressToNjetApiRoute.tmpl"
	VirtualServerToNJetApiRouteTemplatePath             = "virtualServerToNjetApiRoute.tmpl"
	virtualServerActionReturnToNJetApiRouteTemplatePath = "virtualServerActionReturnToNjetApiRoute.tmpl"
)

func quote(input interface{}) string {
	var inputStr string
	switch input := input.(type) {
	case string:
		inputStr = input
	case fmt.Stringer:
		inputStr = input.String()
	case *string:
		inputStr = *input
	default:
		inputStr = fmt.Sprintf("%v", input)
	}
	return fmt.Sprintf("%q", inputStr)
}

var (
	funcMap = template.FuncMap{
		"quote": quote,
	}
)

// TemplateExecutor executes NGINX configuration templates.
type TemplateExecutor struct {
	ingressToNJetApiRoute                   *template.Template
	virtualServerToNJetApiRoute             *template.Template
	virtualServerActionReturnToNJetApiRoute *template.Template
}

// NewTemplateExecutor creates a TemplateExecutor.
func NewTemplateExecutor() (*TemplateExecutor, error) {
	// template name must be the base name of the template file https://golang.org/pkg/text/template/#Template.ParseFiles
	ingressToNJetApiRoute, err := template.New(path.Base(IngressToNJetApiRouteTemplatePath)).Funcs(funcMap).ParseFiles(IngressToNJetApiRouteTemplatePath)
	if err != nil {
		return nil, err
	}

	vsToNJetApiRoute, err := template.New(path.Base(VirtualServerToNJetApiRouteTemplatePath)).Funcs(njetdynapigenv2.HelperFunctions).ParseFiles(VirtualServerToNJetApiRouteTemplatePath)
	if err != nil {
		return nil, err
	}

	vsActionReturnToNJetApiRoute, err := template.New(path.Base(virtualServerActionReturnToNJetApiRouteTemplatePath)).Funcs(njetdynapigenv2.HelperFunctions).ParseFiles(virtualServerActionReturnToNJetApiRouteTemplatePath)
	if err != nil {
		return nil, err
	}
	return &TemplateExecutor{
		ingressToNJetApiRoute:                   ingressToNJetApiRoute,
		virtualServerToNJetApiRoute:             vsToNJetApiRoute,
		virtualServerActionReturnToNJetApiRoute: vsActionReturnToNJetApiRoute,
	}, nil
}

// ExecuteingressToNJetApiRouteTemplate generates the content of the NJetApiRoute.Locations
func (te *TemplateExecutor) ExecuteingressToNJetApiRouteTemplate(cfg *locationBody) ([]byte, error) {
	var configBuffer bytes.Buffer
	err := te.ingressToNJetApiRoute.Execute(&configBuffer, cfg)

	return configBuffer.Bytes(), err
}

// ExecuteingressToNJetApiRouteTemplate generates the content of the NJetApiRoute.Locations
func (te *TemplateExecutor) ExecuteVirtualServerToNJetApiRouteTemplate(cfg *locationBodyTemplateForVirtualServer) ([]byte, error) {
	var configBuffer bytes.Buffer
	err := te.virtualServerToNJetApiRoute.Execute(&configBuffer, cfg)

	return configBuffer.Bytes(), err
}

// ExecuteingressToNJetApiRouteTemplate generates the content of the NJetApiRoute.Locations
func (te *TemplateExecutor) ExecuteVirtualServerActionReturnToNJetApiRouteTemplate(cfg *locationBodyTemplateForVirtualServerActionReturn) ([]byte, error) {
	var configBuffer bytes.Buffer
	err := te.virtualServerActionReturnToNJetApiRoute.Execute(&configBuffer, cfg)

	return configBuffer.Bytes(), err
}
