package njetdynapi

import (
	"fmt"

	"github.com/nginxinc/kubernetes-ingress/internal/model"
	"github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller"
)

type WorkerGenerator struct {
	Type string
}

func NewWorkerGenerator(resourceType string) *WorkerGenerator {
	workerGenerator := &WorkerGenerator{
		Type: resourceType,
	}
	return workerGenerator
}

func (wg *WorkerGenerator) Generate(push *model.PushNjetApiResourceContext) (*model.NjetApiContextWrap, error) {
	if push == nil || push.K8sResourceInfo.RType.String() != wg.Type || push.WorkerConfig == nil {
		return nil, fmt.Errorf("PushContext is nil")
	}

	njetApiContext := &model.NjetApiContext{
		WorkerSetting: &controller.NjetWorkerApi{
			Key:   "__master_worker_count",
			Value: push.WorkerConfig.WorkerProcesses,
		},
	}
	wrap := &model.NjetApiContextWrap{
		Added:   nil,
		Updated: njetApiContext,
		Deleted: nil,
	}

	return wrap, nil
}
