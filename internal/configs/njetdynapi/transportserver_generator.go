/*
 * Copyright (C) 2021-2023  TMLake(Beijing) Technology Co., Ltd.
 * OpenNJet-KIC is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *        http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package njetdynapi

import (
	"fmt"
	"github.com/golang/glog"
	"github.com/nginxinc/kubernetes-ingress/internal/configs/version2"
	"github.com/nginxinc/kubernetes-ingress/internal/model"
	"github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller"
	"reflect"
	"strconv"
)

const (
	PortRedirectDST       = 12003
	PortRedirectDSTForUDP = 12004
	IptablesPath          = "/sbin/iptables"
)

type TransportServerGenerator struct {
	Type                           string
	TransportServerConfig          map[string]*version2.TransportServerConfig
	streamMapForTCPPortAndUpstream *controller.NjetStreamMapApi
	streamMapForUDPPortAndUpstream *controller.NjetStreamMapApi
}

func NewTransportServerGenerator(resourceType string) *TransportServerGenerator {
	tsGenerator := &TransportServerGenerator{
		Type:                  resourceType,
		TransportServerConfig: map[string]*version2.TransportServerConfig{},
	}

	return tsGenerator
}

func (ts *TransportServerGenerator) FindTransportServerConfig(tsKey string) (*version2.TransportServerConfig, error) {
	if tsConfig, ok := ts.TransportServerConfig[tsKey]; ok {
		return tsConfig, nil
	}
	glog.Errorf("TransportServerGenerator not found TransportServerConfig")
	return nil, fmt.Errorf("TransportServerGenerator not found TransportServerConfig")
}

func generateTransportServerKey(stream version2.StreamServer) string {
	return fmt.Sprintf("ts_%s_%s", stream.Namespace, stream.Name)
}

func (ts *TransportServerGenerator) Generate(push *model.PushNjetApiResourceContext) (*model.NjetApiContextWrap, error) {
	if push == nil || push.K8sResourceInfo.RType.String() != ts.Type || push.TransportServerConfig == nil {
		return nil, fmt.Errorf("PushContext is invalid")
	}

	// handle version2.TransportServerConfig
	// generate PortRedirect
	err, portRedirectApi := ts.buildPortRedirect(push.TransportServerConfig)
	if err != nil {
		glog.Errorf("TransportServerGenerator failed to build Njet PortRedirect: %s, Skip this event", err)
		return nil, err
	}
	njetApiContext := &model.NjetApiContext{
		PortRedirect: portRedirectApi,
	}
	var (
		added   *model.NjetApiContext
		deleted *model.NjetApiContext
	)
	tsKey := generateTransportServerKey(push.TransportServerConfig.Server)

	if push.K8sResourceInfo.Operation == model.Delete {
		deleted = njetApiContext
	} else {
		//handle old TransportServerConfig
		oldNjetApiContext := &model.NjetApiContext{}
		if _, ok := ts.TransportServerConfig[tsKey]; ok {
			err, portRedirectApi = ts.buildPortRedirect(ts.TransportServerConfig[tsKey])
			if err != nil {
				glog.Errorf("TransportServerGenerator failed to build Njet PortRedirect: %s, Skip this event", err)
				return nil, err
			}
			oldNjetApiContext.PortRedirect = portRedirectApi
		}

		added, _, deleted = njetApiContext.DifferentPortRedirect(oldNjetApiContext)
	}

	wrap := &model.NjetApiContextWrap{
		Added:   added,
		Deleted: deleted,
	}

	// update cache
	if push.TransportServerConfig != nil {
		if push.K8sResourceInfo.Operation == model.Delete {
			delete(ts.TransportServerConfig, tsKey)
		} else {
			ts.TransportServerConfig[tsKey] = push.TransportServerConfig
		}

	}

	// generate streamMap
	// TCP
	streamMap := ts.buildPortRedirectMapping()
	if !reflect.DeepEqual(streamMap, ts.streamMapForTCPPortAndUpstream) {
		if wrap.Updated != nil {
			wrap.Updated.StreamMap = streamMap
		} else {
			wrap.Updated = &model.NjetApiContext{
				StreamMap: streamMap,
			}
		}
		// update cache
		ts.streamMapForTCPPortAndUpstream = streamMap

	}

	// generate streamMap
	// UDP
	streamMap = ts.buildPortRedirectMappingForUDP()
	if !reflect.DeepEqual(streamMap, ts.streamMapForUDPPortAndUpstream) {
		if wrap.Updated != nil {
			wrap.Updated.StreamMapForUDPPortAndUpstream = streamMap
		} else {
			wrap.Updated = &model.NjetApiContext{
				StreamMapForUDPPortAndUpstream: streamMap,
			}
		}
		// update cache
		ts.streamMapForUDPPortAndUpstream = streamMap

	}

	return wrap, nil
}

func (ts *TransportServerGenerator) buildPortRedirect(tsc *version2.TransportServerConfig) (error, *controller.NjetPortRedirectApi) {
	if tsc == nil {
		glog.Errorf("buildPortRedirect parameter is nil")
		return fmt.Errorf("buildPortRedirect parameter is nil"), nil
	}

	//if tsc.Server.UDP {
	//	glog.Errorf("TransportServerGenerator:buildPortRedirect:Server is UDP, nonsupport")
	//	return fmt.Errorf("TransportServerGenerator:buildPortRedirect:Server is UDP, nonsupport"), nil
	//}

	portRedirectApi := &controller.NjetPortRedirectApi{
		SrcPort:      strconv.Itoa(tsc.Server.Port),
		ProtocolType: controller.PortRedirectTCPProtocolType,
		DstPort:      PortRedirectDST,
	}

	if tsc.Server.UDP {
		portRedirectApi.ProtocolType = controller.PortRedirectUDPProtocolType
		portRedirectApi.DstPort = PortRedirectDSTForUDP
	}
	return nil, portRedirectApi
}

func (ts *TransportServerGenerator) buildPortRedirectMapping() *controller.NjetStreamMapApi {
	streamMapApi := &controller.NjetStreamMapApi{
		Maps: []controller.Mapping{{
			KeyFrom:    "$njtmesh_port",
			KeyTo:      "$stream_upstream",
			Values:     make([]controller.ValueMapping, 0),
			IsVolatile: false,
			Hostnames:  false,
		}},
	}
	var valueMapping []controller.ValueMapping
	for _, tsc := range ts.TransportServerConfig {
		if !tsc.Server.UDP {
			if tsc.Server.Port != 0 && tsc.Server.ProxyPass != "" {
				valueMapping = append(valueMapping, controller.ValueMapping{
					ValueFrom: strconv.Itoa(tsc.Server.Port),
					ValueTo:   tsc.Server.ProxyPass})
			}
		}
	}

	if len(valueMapping) != 0 {
		streamMapApi.Maps[0].Values = valueMapping
	}
	return streamMapApi
}

func (ts *TransportServerGenerator) buildPortRedirectMappingForUDP() *controller.NjetStreamMapApi {
	streamMapApi := &controller.NjetStreamMapApi{
		Maps: []controller.Mapping{{
			KeyFrom:    "$njtmesh_port",
			KeyTo:      "$stream_upstream_udp",
			Values:     make([]controller.ValueMapping, 0),
			IsVolatile: false,
			Hostnames:  false,
		}},
	}
	var valueMapping []controller.ValueMapping
	for _, tsc := range ts.TransportServerConfig {
		if tsc.Server.UDP {
			if tsc.Server.Port != 0 && tsc.Server.ProxyPass != "" {
				valueMapping = append(valueMapping, controller.ValueMapping{
					ValueFrom: strconv.Itoa(tsc.Server.Port),
					ValueTo:   tsc.Server.ProxyPass})
			}
		}
	}

	if len(valueMapping) != 0 {
		streamMapApi.Maps[0].Values = valueMapping
	}
	return streamMapApi
}
