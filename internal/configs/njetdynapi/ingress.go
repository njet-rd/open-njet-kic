package njetdynapi

import (
	"fmt"
	networking "k8s.io/api/networking/v1"
	"strings"

	"github.com/golang/glog"
	"github.com/nginxinc/kubernetes-ingress/internal/configs/version1"
	"github.com/nginxinc/kubernetes-ingress/internal/model"
	"github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller"
)

const (
	MethodVariable    = "$request_method"
	AuthorityVariable = "$host"
	SchemeVariable    = "$scheme"
	UriVariable       = "$uri"

	Protocol     = "http://"
	UpstreamName = "upstream_balancer"
)

type locationBody struct {
	Server   *version1.Server
	Location *version1.Location
	Ingress  *version1.Ingress
}

type IngressGenerator struct {
	Type               string
	TemplateExecutor   *TemplateExecutor
	IngressNginxConfig map[string]*version1.IngressNginxConfig
}

func NewIngressGenerator(resourceType string) *IngressGenerator {
	ingressGenerator := &IngressGenerator{
		Type:               resourceType,
		IngressNginxConfig: map[string]*version1.IngressNginxConfig{},
	}
	t, err := NewTemplateExecutor()
	if err != nil {
		glog.Fatalf("Error creating TemplateExecutor: %v", err)
	}
	ingressGenerator.TemplateExecutor = t
	return ingressGenerator
}

func generateIngressKey(ingress version1.Ingress) string {
	return fmt.Sprintf("%s/%s", ingress.Namespace, ingress.Name)
}

func (ingress *IngressGenerator) FindIngressNginxConfig(ingressKey string) (*version1.IngressNginxConfig, error) {
	if nginxConfig, ok := ingress.IngressNginxConfig[ingressKey]; ok {
		return nginxConfig, nil
	}
	glog.Errorf("IngressGenerator not found IngressNginxConfig")
	return nil, fmt.Errorf("IngressGenerator not found IngressNginxConfig")
}

func (ingress *IngressGenerator) Generate(push *model.PushNjetApiResourceContext) (*model.NjetApiContextWrap, error) {
	if push == nil || push.K8sResourceInfo.RType.String() != ingress.Type || push.IngressNginxConfig == nil {
		return nil, fmt.Errorf("PushContext is nil")
	}

	njetApiContext := &model.NjetApiContext{}
	oldNjetApiContext := &model.NjetApiContext{}

	// handle version1.IngressNginxConfig
	err := ingress.buildRoutes(njetApiContext, push.IngressNginxConfig)
	if err != nil {
		glog.Errorf("IngressGenerator failed to build Njet Route: %s, Skip this event", err)
		return nil, err
	}
	// create NJet VS
	njetApiContext.Vss, err = ingress.buildVs(push.IngressNginxConfig)
	if err != nil {
		glog.Errorf("IngressGenerator failed to build Njet Vs: %s, Skip this event", err)
		return nil, err
	}

	var (
		added   = &model.NjetApiContext{}
		updated = &model.NjetApiContext{}
		deleted = &model.NjetApiContext{}
	)

	ingressKey := generateIngressKey(push.IngressNginxConfig.Ingress)

	if push.K8sResourceInfo.Operation == model.Delete {
		if len(njetApiContext.Vss) != 0 {
			// NJet VS need delete, and locations are also deleted
			deleted.Vss = njetApiContext.Vss
		}
	} else {
		//handle old IngressNginxConfig
		if push.IngressNginxConfig != nil {
			if _, ok := ingress.IngressNginxConfig[ingressKey]; ok {
				err = ingress.buildRoutes(oldNjetApiContext, ingress.IngressNginxConfig[ingressKey])
				if err != nil {
					glog.Errorf("IngressGenerator failed to build Njet Route: %s, Skip this event", err)
					return nil, err
				}
				added, updated, deleted = njetApiContext.DifferentRoute(oldNjetApiContext)

				oldNjetApiContext.Vss, err = ingress.buildVs(ingress.IngressNginxConfig[ingressKey])
				if err != nil {
					glog.Errorf("IngressGenerator failed to build Njet Vs: %s, Skip this event", err)
					return nil, err
				}
				addedvs, updatedvs, deletedvs := njetApiContext.DifferentVs(oldNjetApiContext)
				added, updated, deleted = njetApiContext.MergeVsAndRoute(addedvs.Vss, updatedvs.Vss, deletedvs.Vss, added.Routes, updated.Routes, deleted.Routes)
			} else {
				added, updated, deleted = njetApiContext.DifferentRoute(oldNjetApiContext)
				// Before operating location, create NJet VS
				if len(njetApiContext.Vss) != 0 {
					// Before operating location, make sure vs exists
					added.Vss = njetApiContext.Vss
				}
			}
		}
	}

	wrap := &model.NjetApiContextWrap{
		Added:   added,
		Updated: updated,
		Deleted: deleted,
	}

	model.CloneVsAndRouteForHttps(wrap)
	// create NJet AceessLog
	njetApiContext.AccessLog, err = ingress.buildAccessLog(push.IngressNginxConfig)
	if err != nil {
		glog.Errorf("IngressGenerator failed to build Njet AceessLog: %s, Skip this event", err)
	}
	oldNjetApiContext.AccessLog, err = ingress.buildAccessLog(ingress.IngressNginxConfig[ingressKey])
	if err != nil {
		glog.Errorf("IngressGenerator failed to build Njet buildAccessLog: %s, Skip this event", err)
	}
	isDifferent := njetApiContext.DifferentAccessLog(oldNjetApiContext)
	if isDifferent {
		wrap.Updated.AccessLog = njetApiContext.AccessLog
	}

	model.CloneAccessLogForHttps(wrap)
	// update cache
	if push.IngressNginxConfig != nil {
		if push.K8sResourceInfo.Operation == model.Delete {
			delete(ingress.IngressNginxConfig, ingressKey)
		} else {
			ingress.IngressNginxConfig[ingressKey] = push.IngressNginxConfig
		}
	}

	certMapping := ingress.buildCertMapping()
	if wrap.Updated != nil {
		wrap.Updated.CertMapping = certMapping
	} else {
		wrap.Updated = &model.NjetApiContext{
			CertMapping: certMapping,
		}
	}

	return wrap, nil
}

func (ingress *IngressGenerator) buildCertMapping() *controller.NjetMapApi {
	certMapping := &controller.NjetMapApi{
		Maps: []controller.Mapping{{
			KeyFrom:    "$ssl_server_name",
			KeyTo:      "$cert_name",
			Values:     make([]controller.ValueMapping, 0),
			IsVolatile: false,
			Hostnames:  true,
		}},
		MappingFor: controller.DYN_MAP_INGRESS,
	}
	for _, nginxConfig := range ingress.IngressNginxConfig {
		for _, server := range nginxConfig.Servers {
			if !server.SSL {
				continue
			}
			//certificate file is pem format, crt and key are the same files
			certMapping.Maps[0].Values = append(certMapping.Maps[0].Values, controller.ValueMapping{
				ValueFrom: server.Name,
				ValueTo:   server.SSLCertificate,
			})
		}
	}
	//add default mapping
	certMapping.Maps[0].Values = append(certMapping.Maps[0].Values, controller.ValueMapping{
		ValueFrom: "default",
		ValueTo:   "/etc/njet/secrets/default",
	})

	return certMapping
}

func (ingress *IngressGenerator) buildRoutes(njetApiContext *model.NjetApiContext, ingressNginxConfig *version1.IngressNginxConfig) error {
	if njetApiContext == nil || ingressNginxConfig == nil {
		return fmt.Errorf("parameter is nil")
	}

	for _, server := range ingressNginxConfig.Servers {
		njetRouteApi := ingress.generateNjetRouteApi(&server, &ingressNginxConfig.Ingress)
		njetApiContext.Routes = append(njetApiContext.Routes, njetRouteApi...)
	}
	return nil
}

func (ingress *IngressGenerator) generateNjetRouteApi(server *version1.Server, ing *version1.Ingress) []*controller.NjetRouteApi {
	var njetRouteApis []*controller.NjetRouteApi
	for _, location := range server.Locations {
		njetRouteApi := &controller.NjetRouteApi{
			OperationType:  "",
			AddressAndPort: generateAddressAndPort(server),
			ServerName:     server.Name,
		}
		njetRouteApi.Locations = ingress.generateLocations(server, &location, ing)
		njetRouteApis = append(njetRouteApis, njetRouteApi)
	}

	return njetRouteApis
}

func generateAddressAndPort(server *version1.Server) string {
	//for _, port := range server.Ports {
	//	return fmt.Sprintf("0.0.0.0:%s", strconv.Itoa(port))
	//}

	return "0.0.0.0:80"
}

func (ingress *IngressGenerator) generateLocations(server *version1.Server, location *version1.Location, ing *version1.Ingress) []controller.Location {
	var locations []controller.Location

	njetRouteApiLocation := controller.Location{
		LocationRule: "",
		LocationName: generateNjetRouteApiLocationName(server, location),
		//ProxyPass:    generateNjetRouteApiProxyPass(location),
		// lua dynamic upstream
		ProxyPass:    fmt.Sprintf("%s%s", Protocol, UpstreamName),
		LocationBody: ingress.generateNjetRouteApiLocationBody(server, location, ing),
	}
	locations = append(locations, njetRouteApiLocation)
	return locations
}

func generateNjetRouteApiLocationName(server *version1.Server, location *version1.Location) string {
	//return fmt.Sprintf("( %s = %s && %s %s )", AuthorityVariable, server.Name, UriVariable, location.Path)
	return fmt.Sprintf("( %s %s )", UriVariable, location.Path)
}

// 废弃
// 直接使用动态location添加时的locationName
func (ingress *IngressGenerator) generateNjetRouteApiLocationNameForAccessLog(server *version1.Server, location *version1.Location) string {
	var accesslogPath string
	// generateIngressPathForDynLoction(path string, pathType *networking.PathType) string
	if location.IngressPathType == nil {
		accesslogPath = "~^" + location.IngressPath
	} else if *location.IngressPathType == networking.PathTypeExact {
		accesslogPath = "=" + location.IngressPath
	} else {
		accesslogPath = "~^" + location.IngressPath
	}
	//return fmt.Sprintf("( %s = %s && %s %s )", AuthorityVariable, server.Name, UriVariable, location.Path)
	return fmt.Sprintf("(%s%s)", UriVariable, accesslogPath)
}

func generateNjetRouteApiProxyPass(location *version1.Location) string {
	if location.SSL {
		return fmt.Sprintf("https://%s%s", location.Upstream.Name, location.Rewrite)
	} else {
		return fmt.Sprintf("http://%s%s", location.Upstream.Name, location.Rewrite)
	}
}

func (ingress *IngressGenerator) generateNjetRouteApiLocationBody(server *version1.Server, location *version1.Location, ing *version1.Ingress) string {
	lBody := &locationBody{
		Location: location,
		Server:   server,
		Ingress:  ing,
	}
	content, err := ingress.TemplateExecutor.ExecuteingressToNJetApiRouteTemplate(lBody)
	if err != nil {
		glog.Fatalf("Error generating NjetRouteApi LocationBody: %v", err)
	}

	body := strings.Trim(string(content), "\n")
	body = strings.TrimSuffix(body, ";")

	glog.V(3).Infof("NjetRouteApi LocationBody:%s", body)
	return body
}

func (ingress *IngressGenerator) buildVs(ingressNginxConfig *version1.IngressNginxConfig) ([]*controller.NjetVsApi, error) {
	if ingressNginxConfig == nil {
		return nil, fmt.Errorf("IngressGenerator:buildVs:parameter is nil")
	}
	var njetVsApis []*controller.NjetVsApi
	for _, server := range ingressNginxConfig.Servers {
		njetVsApi := ingress.generateNjetVsApi(&server)
		njetVsApis = append(njetVsApis, njetVsApi)
	}

	return njetVsApis, nil
}

func (ingress *IngressGenerator) generateNjetVsApi(server *version1.Server) *controller.NjetVsApi {
	if server == nil {
		return nil
	}

	njetVsApi := &controller.NjetVsApi{
		OperationType:  "",
		AddressAndPort: generateAddressAndPort(server),
		ServerName:     server.Name,
		ServerBody:     "ssl_certificate $cert_name; ssl_certificate_key $cert_name;",
	}

	return njetVsApi
}

func (ingress *IngressGenerator) buildAccessLog(ingressNginxConfig *version1.IngressNginxConfig) (*controller.AccessLogData, error) {
	if ingressNginxConfig == nil {
		return nil, fmt.Errorf("IngressGenerator:buildAccessLog:parameter is nil")
	}
	njetAccessLogApi := controller.AccessLogData{
		AccessLogFormats: []controller.AccessLogFormat{},
	}
	for _, server := range ingressNginxConfig.Servers {
		accessLogServer := ingress.generateNjetAccessLogServer(&server)
		njetAccessLogApi.Servers = append(njetAccessLogApi.Servers, accessLogServer)
	}

	return &njetAccessLogApi, nil
}

func (ingress *IngressGenerator) generateNjetAccessLogServer(server *version1.Server) controller.AccessLogServer {
	if server == nil {
		return controller.AccessLogServer{}
	}
	accessLogServer := controller.AccessLogServer{
		Listens:     []string{generateAddressAndPort(server)},
		ServerNames: []string{server.Name},
		Locations:   ingress.generateNjetAccessLogApiLocations(server),
	}
	return accessLogServer
}

func (ingress *IngressGenerator) generateNjetAccessLogApiLocations(server *version1.Server) []controller.AccessLogServerLocation {
	if server == nil {
		return []controller.AccessLogServerLocation{}
	}
	var accessLogServerLocations []controller.AccessLogServerLocation
	for _, location := range server.Locations {
		accessLogServerLocation := controller.AccessLogServerLocation{
			Location:    generateNjetRouteApiLocationName(server, &location),
			AccessLogOn: !location.AccessLogOff,
		}
		if location.AccessLogOff {
			accessLogServerLocation.AccessLogs = nil
		} else {
			accessLogServerLocation.AccessLogs = make([]controller.AccessLogServerLocationLogs, 1)
			accessLogServerLocation.AccessLogs[0].FormatName = location.LogFormatName
			accessLogServerLocation.AccessLogs[0].Path = location.AccessLogPath
		}
		accessLogServerLocations = append(accessLogServerLocations, accessLogServerLocation)
	}

	return accessLogServerLocations
}
