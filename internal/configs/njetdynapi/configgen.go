package njetdynapi

import (
	"github.com/nginxinc/kubernetes-ingress/internal/model"
)

// NjetApiResourceGenerator represents the interfaces to be implemented by code that generates Njet API requests
type NjetApiResourceGenerator interface {
	// Generate generates the Sotw resources for Xds.
	Generate(push *model.PushNjetApiResourceContext) (*model.NjetApiContextWrap, error)
}

// NjetLuaApiResourceGenerator represents the interfaces to be implemented by code that generates Njet lua API requests
type NjetLuaApiResourceGenerator interface {
	// Generate generates the Sotw resources for Xds.
	Generate(push *model.PushNjetLuaApiResourceContext) (*model.NjetLuaApiContext, error)
}
