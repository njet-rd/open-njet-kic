/*
 * Copyright (C) 2021-2023  TMLake(Beijing) Technology Co., Ltd.
 * OpenNJet-KIC is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *        http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */

package njetdynapi

import (
	"fmt"
	"reflect"
	"sort"
	"strings"
	"time"

	"github.com/nginxinc/kubernetes-ingress/internal/configs/version2"

	"github.com/golang/glog"
	"github.com/nginxinc/kubernetes-ingress/internal/configs/version1"
	"github.com/nginxinc/kubernetes-ingress/internal/model"
	njetapicontroller "github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller"
)

const (
	defaultUpstreamIP   = "127.0.0.1"
	defaultUpstreamPort = "8181"
)

type LuaUpstreamGenerator struct {
	Type               string
	Upstreams          []*njetapicontroller.LuaUpstreamApi
	UpstreamsForStream []*njetapicontroller.LuaUpstreamApi
}

func NewLuaUpstreamGenerator(resourceType string) *LuaUpstreamGenerator {
	luaUpstreamGenerator := &LuaUpstreamGenerator{
		Type: resourceType,
	}
	return luaUpstreamGenerator
}

func (u *LuaUpstreamGenerator) Generate(push *model.PushNjetLuaApiResourceContext) (*model.NjetLuaApiContext, error) {
	if push == nil {
		return nil, fmt.Errorf("PushNjetLuaApiResourceContext is nil")
	}

	njetLuaApiContext := &model.NjetLuaApiContext{}

	for _, ingressNginxConfig := range push.IngressNginxConfig {

		// handle version1.IngressNginxConfig
		upstreams, err := u.buildUpstreams(ingressNginxConfig)
		if err != nil {
			glog.Errorf("LuaUpstreamGenerator failed to build Njet Lua HTTP Upstream: %s, Skip this event", err)
			continue
		}

		njetLuaApiContext.Upstreams = append(njetLuaApiContext.Upstreams, upstreams...)
	}

	for _, virtualServerConfig := range push.VirtualServerConfig {

		// handle version2.VirtualServerConfig
		upstreams, err := u.buildUpstreamsForVS(virtualServerConfig)
		if err != nil {
			glog.Errorf("LuaUpstreamGenerator failed to build Njet Lua HTTP Upstream: %s, Skip this event", err)
			continue
		}

		njetLuaApiContext.Upstreams = append(njetLuaApiContext.Upstreams, upstreams...)
	}

	sort.SliceStable(u.Upstreams, func(a, b int) bool {
		return u.Upstreams[a].Name < u.Upstreams[b].Name
	})
	sort.SliceStable(njetLuaApiContext.Upstreams, func(a, b int) bool {
		return njetLuaApiContext.Upstreams[a].Name < njetLuaApiContext.Upstreams[b].Name
	})

	backendsChangedForHttp := !reflect.DeepEqual(njetLuaApiContext.Upstreams, u.Upstreams)
	if backendsChangedForHttp {
		// update cache
		u.Upstreams = njetLuaApiContext.Upstreams
	} else {
		njetLuaApiContext.Upstreams = nil
	}

	// stream upstream
	for _, transportServerConfig := range push.TransportServerConfig {

		// handle version2.TransportServerConfig
		upstreams, err := u.buildUpstreamsForTS(transportServerConfig)
		if err != nil {
			glog.Errorf("LuaUpstreamGenerator failed to build Njet Stream Lua Upstream: %s, Skip this event", err)
			continue
		}

		njetLuaApiContext.UpstreamsForStream = append(njetLuaApiContext.UpstreamsForStream, upstreams...)
	}

	sort.SliceStable(u.UpstreamsForStream, func(a, b int) bool {
		return u.UpstreamsForStream[a].Name < u.UpstreamsForStream[b].Name
	})
	sort.SliceStable(njetLuaApiContext.UpstreamsForStream, func(a, b int) bool {
		return njetLuaApiContext.UpstreamsForStream[a].Name < njetLuaApiContext.UpstreamsForStream[b].Name
	})

	backendsChangedForStream := !reflect.DeepEqual(njetLuaApiContext.UpstreamsForStream, u.UpstreamsForStream)
	if backendsChangedForStream {
		// update cache
		u.UpstreamsForStream = njetLuaApiContext.UpstreamsForStream
	} else {
		njetLuaApiContext.UpstreamsForStream = nil
	}

	if !backendsChangedForHttp && !backendsChangedForStream {
		glog.Infof("****************************************************njetLuaApiContext.Upstreams no change**********************************************************************")
		return nil, nil
	}

	return njetLuaApiContext, nil
}

func (u *LuaUpstreamGenerator) buildUpstreams(ingressNginxConfig *version1.IngressNginxConfig) ([]*njetapicontroller.LuaUpstreamApi, error) {
	if ingressNginxConfig == nil {
		return nil, fmt.Errorf("version1.IngressNginxConfig is nil")
	}

	var luaUpstreams []*njetapicontroller.LuaUpstreamApi
	for _, upstream := range ingressNginxConfig.Upstreams {
		luaUpstream := &njetapicontroller.LuaUpstreamApi{
			Name:          upstream.Name,
			LoadBalancing: upstream.LBMethod,
		}
		if luaUpstream.LoadBalancing == "chash" {
			luaUpstream.UpstreamHashByConfig = &njetapicontroller.UpstreamHashByConfig{
				UpstreamHashBy: upstream.UpstreamHashBy,
			}
		}
		var endpoints []njetapicontroller.Endpoint
		for _, endpoint := range upstream.UpstreamServers {
			endpoints = append(endpoints, njetapicontroller.Endpoint{
				Address: generateAddress(endpoint.Address),
				Port:    generatePort(endpoint.Address),
			})
		}
		//get healthcheck from IngressEx
		for _, server := range ingressNginxConfig.Servers {
			if hc, found := server.HealthChecks[upstream.Name]; found {
				luaUpstream.HealthCheck = &njetapicontroller.HealthCheck{
					Schema:      hc.Scheme,
					Path:        hc.URI,
					StatusMatch: "200-399", //readiness probe can't set statusMatch value
					Interval:    hc.Interval,
					Fails:       hc.Fails,
					Passes:      hc.Passes,
					Timeout:     hc.TimeoutSeconds,
					Headers:     hc.Headers,
				}
				break
			}
		}
		luaUpstream.Endpoints = endpoints
		luaUpstreams = append(luaUpstreams, luaUpstream)
	}
	return luaUpstreams, nil
}

func (u *LuaUpstreamGenerator) buildUpstreamsForVS(virtualServerConfig *version2.VirtualServerConfig) ([]*njetapicontroller.LuaUpstreamApi, error) {
	if virtualServerConfig == nil {
		return nil, fmt.Errorf("version2.VirtualServerConfig is nil")
	}
	var luaUpstreams []*njetapicontroller.LuaUpstreamApi
	for _, upstream := range virtualServerConfig.Upstreams {
		luaUpstream := &njetapicontroller.LuaUpstreamApi{
			Name:          upstream.Name,
			LoadBalancing: upstream.LBMethod,
		}

		if strings.HasPrefix(luaUpstream.LoadBalancing, "chash") {
			chashFields := strings.Fields(luaUpstream.LoadBalancing)
			if len(chashFields) == 2 {
				luaUpstream.LoadBalancing = "chash"
				luaUpstream.UpstreamHashByConfig = &njetapicontroller.UpstreamHashByConfig{
					UpstreamHashBy: chashFields[1],
				}
			} else {
				luaUpstream.LoadBalancing = "round_robin"
			}
		}
		if len(strings.TrimSpace(luaUpstream.LoadBalancing)) == 0 {
			luaUpstream.LoadBalancing = "round_robin"
		}
		//get healthcheck from virtualServerConfig
		if virtualServerConfig.Server.HealthChecks != nil {
			for _, hc := range virtualServerConfig.Server.HealthChecks {
				if hc.Name == upstream.Name {
					schema := "http"
					if upstream.TLSEnable {
						schema = "https"
					}
					interval, err := time.ParseDuration(hc.Interval)
					if err != nil {
						interval = 5
					}
					timeout, err := time.ParseDuration(hc.ProxyConnectTimeout)
					if err != nil {
						timeout = 1
					}
					statusMatch := "200-399"
					for _, match := range virtualServerConfig.StatusMatches {
						if match.Name == hc.Match {
							statusMatch = match.Code
						}
					}

					path := hc.URI

					if hc.Type != "http" && hc.Type != "" {
						statusMatch = ""
						schema = ""
						path = ""
					}

					luaUpstream.HealthCheck = &njetapicontroller.HealthCheck{
						Type:        hc.Type,
						Schema:      schema,
						Path:        path,
						Port:        hc.Port,
						StatusMatch: statusMatch,
						Interval:    int32(interval.Seconds()),
						Fails:       int32(hc.Fails),
						Passes:      int32(hc.Passes),
						Timeout:     int64(timeout.Seconds()),
						Headers:     hc.Headers,
					}
				}
			}
		}
		var endpoints []njetapicontroller.Endpoint
		for _, endpoint := range upstream.Servers {
			endpoints = append(endpoints, njetapicontroller.Endpoint{
				Address: generateAddress(endpoint.Address),
				Port:    generatePort(endpoint.Address),
			})
		}

		luaUpstream.Endpoints = endpoints
		luaUpstreams = append(luaUpstreams, luaUpstream)
	}
	return luaUpstreams, nil
}

func (u *LuaUpstreamGenerator) buildUpstreamsForTS(transportServerConfig *version2.TransportServerConfig) ([]*njetapicontroller.LuaUpstreamApi, error) {
	if transportServerConfig == nil {
		return nil, fmt.Errorf("version2.TransportServerConfig is nil")
	}
	var luaUpstreams []*njetapicontroller.LuaUpstreamApi
	for _, upstream := range transportServerConfig.Upstreams {
		if upstream.Name != transportServerConfig.Server.ProxyPass {
			continue
		}

		luaUpstream := &njetapicontroller.LuaUpstreamApi{
			Name:          upstream.Name,
			LoadBalancing: upstream.LoadBalancingMethod,
		}

		var endpoints []njetapicontroller.Endpoint
		for _, endpoint := range upstream.Servers {
			endpoints = append(endpoints, njetapicontroller.Endpoint{
				Address: generateAddress(endpoint.Address),
				Port:    generatePort(endpoint.Address),
			})
		}

		luaUpstream.Endpoints = endpoints

		var healthCheck njetapicontroller.HealthCheck
		if transportServerConfig.Server.HealthCheck != nil && transportServerConfig.Server.HealthCheck.Enabled {
			hc := transportServerConfig.Server.HealthCheck
			healthCheck.Type = "tcp"
			interval, err := time.ParseDuration(hc.Interval)
			if err != nil {
				interval = 5
			}
			timeout, err := time.ParseDuration(hc.Timeout)
			if err != nil {
				timeout = 1
			}

			healthCheck.Fails = int32(hc.Fails)
			healthCheck.Passes = int32(hc.Passes)
			healthCheck.Interval = int32(interval.Seconds())
			healthCheck.Timeout = int64(timeout.Seconds())
			healthCheck.Port = hc.Port
		}
		luaUpstream.HealthCheck = &healthCheck
		luaUpstreams = append(luaUpstreams, luaUpstream)
	}
	return luaUpstreams, nil
}

func generateAddress(server string) string {
	// ingress default server -> 127.0.0.1:8181
	// VS CR default server -> unix domain
	if strings.HasPrefix(server, "unix:") {
		return defaultUpstreamIP
	}
	if len(strings.Split(server, ":")) == 2 {
		return strings.Split(server, ":")[0]
	} else {
		return defaultUpstreamIP
	}
}

func generatePort(server string) string {
	// ingress default server -> 127.0.0.1:8181
	// VS CR default server -> unix domain
	if strings.HasPrefix(server, "unix:") {
		return defaultUpstreamPort
	}
	if len(strings.Split(server, ":")) == 2 {
		return strings.Split(server, ":")[1]
	} else {
		return defaultUpstreamPort
	}
}
