package mainconfig

import "github.com/nginxinc/kubernetes-ingress/internal/configs/version1"

type LogSettingEntry struct {
	AccessLogOff      bool
	LogFormat         []string
	LogFormatEscaping string
}

type LogConfig struct {
	Entrys map[string]LogSettingEntry
}

func NewLogConfig(njetMainConfig *version1.MainConfig) *LogConfig {
	if njetMainConfig == nil {
		return nil
	}

	lc := &LogConfig{
		Entrys: make(map[string]LogSettingEntry),
	}
	lc.Entrys["main"] = LogSettingEntry{
		AccessLogOff:      njetMainConfig.AccessLogOff,
		LogFormat:         njetMainConfig.LogFormat,
		LogFormatEscaping: njetMainConfig.LogFormatEscaping,
	}
	return lc
}

type ReloadConfig struct {
	// Add the configuration that requires reload here
	luaSharedDicts map[string]int
}

func NewReloadConfig(njetMainConfig *version1.MainConfig) *ReloadConfig {
	if njetMainConfig == nil {
		return nil
	}
	rc := &ReloadConfig{
		luaSharedDicts: make(map[string]int),
	}

	for key, value := range njetMainConfig.LuaSharedDicts {
		rc.luaSharedDicts[key] = value
	}
	return rc
}

type WorkerConfig struct {
	WorkerProcesses string
}

func NewWorkerConfig(njetMainConfig *version1.MainConfig) *WorkerConfig {
	if njetMainConfig == nil {
		return nil
	}
	wc := &WorkerConfig{
		WorkerProcesses: njetMainConfig.WorkerProcesses,
	}
	return wc
}
