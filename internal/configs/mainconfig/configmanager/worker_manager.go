package configmanager

import (
	"github.com/nginxinc/kubernetes-ingress/internal/configs/mainconfig"
	"github.com/nginxinc/kubernetes-ingress/internal/configs/version1"
	"github.com/nginxinc/kubernetes-ingress/internal/model"
	"github.com/nginxinc/kubernetes-ingress/internal/nginx"
	"reflect"
)

type workerManager interface {
	ConfigChange(wc *mainconfig.WorkerConfig) bool
	UpdateWorker(wc *mainconfig.WorkerConfig, nginxManager nginx.Manager)
}

type workerBasicManager struct {
	workerConfig *mainconfig.WorkerConfig
	configChange bool
}

func newNjetWorkerManager(njetMainConfig *version1.MainConfig) workerManager {
	wm := &workerBasicManager{}
	wm.workerConfig = mainconfig.NewWorkerConfig(njetMainConfig)
	return wm
}

func (wm *workerBasicManager) ConfigChange(wc *mainconfig.WorkerConfig) bool {
	if wc == nil {
		return false
	}
	if !reflect.DeepEqual(wm.workerConfig, wc) {
		wm.configChange = true
		return true
	}

	return false
}

func (wm *workerBasicManager) UpdateWorker(wc *mainconfig.WorkerConfig, nginxManager nginx.Manager) {
	if wc == nil {
		return
	}

	if wm.configChange {
		push := model.NewPushNjetApiResourceContext()
		push.K8sResourceInfo = &model.K8sResourceInfo{
			RType:     model.WorkerResource,
			Operation: model.AddOrUpdate,
		}
		push.WorkerConfig = wc
		nginxManager.PushNjetApiResource(push)
		wm.workerConfig = wc
	}
}
