package configmanager

import (
	"github.com/nginxinc/kubernetes-ingress/internal/configs/mainconfig"
	"github.com/nginxinc/kubernetes-ingress/internal/configs/version1"
	"reflect"
)

type ReloadManager interface {
	ConfigChange(rc *mainconfig.ReloadConfig) bool
	Update(rc *mainconfig.ReloadConfig)
}

type reloadBasicManager struct {
	reloadConfig *mainconfig.ReloadConfig
	configChange bool
}

func newNjetReloadManager(njetMainConfig *version1.MainConfig) ReloadManager {
	rm := &reloadBasicManager{}
	rm.reloadConfig = mainconfig.NewReloadConfig(njetMainConfig)
	return rm
}

func (rm *reloadBasicManager) ConfigChange(rc *mainconfig.ReloadConfig) bool {
	if rc == nil {
		return false
	}
	if !reflect.DeepEqual(rm.reloadConfig, rc) {
		rm.configChange = true
		return true
	}

	return false
}

func (rm *reloadBasicManager) Update(rc *mainconfig.ReloadConfig) {
	if rc == nil {
		return
	}

	if rm.configChange {
		rm.reloadConfig = rc
	}
}
