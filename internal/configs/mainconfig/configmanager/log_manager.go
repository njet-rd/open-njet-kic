package configmanager

import (
	"github.com/nginxinc/kubernetes-ingress/internal/configs/mainconfig"
	"github.com/nginxinc/kubernetes-ingress/internal/configs/version1"
	"github.com/nginxinc/kubernetes-ingress/internal/model"
	"github.com/nginxinc/kubernetes-ingress/internal/nginx"
	"reflect"
)

type LogManager interface {
	ConfigChange(wc *mainconfig.LogConfig) bool
	UpdateLog(wc *mainconfig.LogConfig, nginxManager nginx.Manager)
}

type logBasicManager struct {
	logConfig    *mainconfig.LogConfig
	configChange bool
}

func newNjetLogManager(njetMainConfig *version1.MainConfig) LogManager {
	lm := &logBasicManager{}
	lm.logConfig = mainconfig.NewLogConfig(njetMainConfig)
	return lm
}

func (lm *logBasicManager) ConfigChange(lc *mainconfig.LogConfig) bool {
	if lc == nil {
		return false
	}
	if !reflect.DeepEqual(lm.logConfig, lc) {
		lm.configChange = true
		return true
	}

	return false
}

func (lm *logBasicManager) UpdateLog(lc *mainconfig.LogConfig, nginxManager nginx.Manager) {
	if lc == nil {
		return
	}

	if lm.configChange {
		push := model.NewPushNjetApiResourceContext()
		push.K8sResourceInfo = &model.K8sResourceInfo{
			RType:     model.LogResource,
			Operation: model.AddOrUpdate,
		}
		push.LogConfig = lc
		nginxManager.PushNjetApiResource(push)
		lm.logConfig = lc
	}
}
