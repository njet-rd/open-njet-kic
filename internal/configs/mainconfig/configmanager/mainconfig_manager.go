package configmanager

import (
	"github.com/nginxinc/kubernetes-ingress/internal/configs/mainconfig"
	"github.com/nginxinc/kubernetes-ingress/internal/configs/version1"
	"github.com/nginxinc/kubernetes-ingress/internal/nginx"
)

type NjetMainConfigManager struct {
	nginxManager  nginx.Manager
	workerManager workerManager
	logManager    LogManager
	reloadManager ReloadManager
}

// NewNjetMainConfigManager returns a functional Manager.
func NewNjetMainConfigManager(njetMainConfig *version1.MainConfig, nginxManager nginx.Manager) *NjetMainConfigManager {
	cm := &NjetMainConfigManager{}
	cm.nginxManager = nginxManager
	cm.workerManager = newNjetWorkerManager(njetMainConfig)
	cm.logManager = newNjetLogManager(njetMainConfig)
	cm.reloadManager = newNjetReloadManager(njetMainConfig)
	return cm
}

func (cm *NjetMainConfigManager) HandleConfigurationChanges(njetMainConfig *version1.MainConfig) (bool, bool) {
	var needOverWriteeMainConfig, needReloadMainConfig bool
	if njetMainConfig == nil {
		return false, false
	}

	// 1.handle worker config
	if cm.workerManager.ConfigChange(mainconfig.NewWorkerConfig(njetMainConfig)) {
		cm.workerManager.UpdateWorker(mainconfig.NewWorkerConfig(njetMainConfig), cm.nginxManager)
		// Although the configuration is dynamically updated, the file content must also be updated to maintain consistency
		needOverWriteeMainConfig = true
	}

	// 2.handle log config
	if cm.logManager.ConfigChange(mainconfig.NewLogConfig(njetMainConfig)) {
		cm.logManager.UpdateLog(mainconfig.NewLogConfig(njetMainConfig), cm.nginxManager)
		// Although the configuration is dynamically updated, the file content must also be updated to maintain consistency
		needOverWriteeMainConfig = true
	}

	// 3.handle other config that need to reload
	if cm.reloadManager.ConfigChange(mainconfig.NewReloadConfig(njetMainConfig)) {
		cm.reloadManager.Update(mainconfig.NewReloadConfig(njetMainConfig))
		needOverWriteeMainConfig = true
		needReloadMainConfig = true
	}
	return needOverWriteeMainConfig, needReloadMainConfig
}
