package adc

import (
	"crypto/rand"
	"github.com/golang/glog"
	"github.com/nginxinc/kubernetes-ingress/internal/adc"
	adccontroller "github.com/nginxinc/kubernetes-ingress/internal/adc/controller"
	"github.com/nginxinc/kubernetes-ingress/internal/model"
	"math/big"
	"reflect"
	"sort"
	"strconv"
	"strings"
)

const (
	DELETE_EVENT = iota
	ADD_UPDATE_EVENT
)

type Configurator struct {
	adcManager *adc.AdcManager
	//host:gslb_pool_uuid
	hosts map[string]string
	// map key:slbpool name(Less than 32), map value: slbpool members
	adcSlbPools map[string][]string
	// map key:vs name
	adcVss map[string]adccontroller.SlbVirtualServerCreateApi
}

func NewConfigurator(adcManager *adc.AdcManager) *Configurator {

	cnf := Configurator{
		adcManager:  adcManager,
		hosts:       make(map[string]string),
		adcSlbPools: make(map[string][]string),
		adcVss:      make(map[string]adccontroller.SlbVirtualServerCreateApi),
	}
	return &cnf
}

func (c *Configurator) GenerateSlbVirtualServer(adcVsAddress string, tcpudpProfileUuid string, eventType int) error {
	// vs delete
	if eventType == DELETE_EVENT {
		deletes := make(map[string]adccontroller.SlbVirtualServerCreateApi)
		for key, _ := range c.adcVss {
			deletes[key] = c.adcVss[key]
			delete(c.adcVss, key)
		}
		task := adc.Task{Kind: adc.VS,
			Obj: &model.AdcVsContextWrap{
				Deleted: deletes,
			},
		}
		c.SendChangeToAdcManager(task)
		return nil
	}

	slbpoolTcpName, _ := generateNjetKicSlbPoolKey(c.adcManager.GetAdcConfig().NjetSvcname)

	// create new vs based on the k8s configuration
	// When the adcVsAddress is empty, it is not processed, because the adc vs port changes to 0
	if adcVsAddress == "" {
		return nil
	}
	adcVsNew := make(map[string]adccontroller.SlbVirtualServerCreateApi)
	vs := adccontroller.NewSlbVirtualServer()
	vs.Name = slbpoolTcpName
	vs.SlbPoolName = slbpoolTcpName
	vs.Vip = adcVsAddress
	vs.TcpUdpProfileUuid = tcpudpProfileUuid
	adcVsNew[slbpoolTcpName] = *vs

	// adc needs update
	updates := make(map[string]adccontroller.SlbVirtualServerCreateApi)
	for key, _ := range c.adcVss {
		if _, ok := adcVsNew[key]; ok {
			backendsChanged := !reflect.DeepEqual(c.adcVss[key], adcVsNew[key])
			if backendsChanged {
				// update cache
				c.adcVss[key] = adcVsNew[key]
				updates[key] = adcVsNew[key]
			}
		}
	}

	// adc needs delete
	deletes := make(map[string]adccontroller.SlbVirtualServerCreateApi)
	for key, _ := range c.adcVss {
		if _, ok := adcVsNew[key]; !ok {
			deletes[key] = c.adcVss[key]
			delete(c.adcVss, key)
		}
	}

	// adc needs add
	adds := make(map[string]adccontroller.SlbVirtualServerCreateApi)
	for key, _ := range adcVsNew {
		if _, ok := c.adcVss[key]; !ok {
			adds[key] = adcVsNew[key]
			c.adcVss[key] = adcVsNew[key]
		}
	}

	if len(deletes) != 0 || len(adds) != 0 || len(updates) != 0 {
		task := adc.Task{Kind: adc.VS,
			Obj: &model.AdcVsContextWrap{
				Deleted: deletes,
				Added:   adds,
				Updated: updates},
		}

		c.SendChangeToAdcManager(task)
	}
	return nil
}

func (c *Configurator) GenerateSlbPool(svcKey string, targetPortToNodePort map[int32]int32, nodeIps []string) error {
	tcpKey, tlsKey := generateNjetKicSlbPoolKey(svcKey)

	// svc delete
	if len(targetPortToNodePort) == 0 && len(nodeIps) == 0 {
		deletes := make(map[string][]string)
		for key, _ := range c.adcSlbPools {
			deletes[key] = c.adcSlbPools[key]
			delete(c.adcSlbPools, key)
		}
		task := adc.Task{Kind: adc.SlbPool,
			Obj: &model.AdcSlbPoolContextWrap{
				Deleted: deletes,
			},
		}
		c.SendChangeToAdcManager(task)
		return nil
	}

	// svc add or update
	mTcp, mTls := c.getSlbPoolMemberAddress(targetPortToNodePort, nodeIps)
	adcSlbPoolsNew := make(map[string][]string)
	adcSlbPoolsNew[tcpKey] = mTcp
	adcSlbPoolsNew[tlsKey] = mTls

	// adc needs update
	updates := make(map[string][]string)
	for key, _ := range c.adcSlbPools {
		if _, ok := adcSlbPoolsNew[key]; ok {
			sort.SliceStable(c.adcSlbPools[key], func(a, b int) bool {
				return c.adcSlbPools[key][a] < c.adcSlbPools[key][b]
			})
			sort.SliceStable(adcSlbPoolsNew[key], func(a, b int) bool {
				return adcSlbPoolsNew[key][a] < adcSlbPoolsNew[key][b]
			})

			backendsChanged := !reflect.DeepEqual(adcSlbPoolsNew[key], c.adcSlbPools[key])
			if backendsChanged {
				// update cache
				c.adcSlbPools[key] = adcSlbPoolsNew[key]
				updates[key] = adcSlbPoolsNew[key]
			}
		}
	}

	// adc needs delete
	deletes := make(map[string][]string)
	for key, _ := range c.adcSlbPools {
		if _, ok := adcSlbPoolsNew[key]; !ok {
			deletes[key] = c.adcSlbPools[key]
			delete(c.adcSlbPools, key)
		}
	}

	// adc needs add
	adds := make(map[string][]string)
	for key, _ := range adcSlbPoolsNew {
		if _, ok := c.adcSlbPools[key]; !ok {
			adds[key] = adcSlbPoolsNew[key]
			c.adcSlbPools[key] = adcSlbPoolsNew[key]
		}
	}

	if len(deletes) != 0 || len(adds) != 0 || len(updates) != 0 {
		task := adc.Task{Kind: adc.SlbPool,
			Obj: &model.AdcSlbPoolContextWrap{
				Deleted: deletes,
				Added:   adds,
				Updated: updates},
		}

		c.SendChangeToAdcManager(task)
	}

	return nil
}

func (c *Configurator) getSlbPoolMemberAddress(targetPortToNodePort map[int32]int32, nodeIps []string) ([]string, []string) {
	if len(targetPortToNodePort) == 0 || len(nodeIps) == 0 {
		glog.Errorf("targetPortToNodePort or nodeIps empty.")
		return nil, nil
	}
	var mTcp []string
	var mTls []string
	for port, nodePort := range targetPortToNodePort {
		for _, nodeIp := range nodeIps {
			switch port {
			case 80:
				if strconv.Itoa(int(nodePort)) != "" && nodeIp != "" {
					mTcp = append(mTcp, strings.Join([]string{nodeIp, strconv.Itoa(int(nodePort))}, ":"))
				}
			case 443:
				if strconv.Itoa(int(nodePort)) != "" && nodeIp != "" {
					mTls = append(mTls, strings.Join([]string{nodeIp, strconv.Itoa(int(nodePort))}, ":"))
				}
			}

		}
	}

	return mTcp, mTls
}

func (c *Configurator) Generate(removedHosts []string, _, addedHosts []string) error {
	for _, rh := range removedHosts {
		if _, ok := c.hosts[rh]; ok {
			delete(c.hosts, rh)
		}
	}

	for _, ah := range addedHosts {
		if _, ok := c.hosts[ah]; !ok {
			c.hosts[ah] = ""
		}
	}

	// update uuid
	for host, _ := range c.hosts {
		c.hosts[host] = c.adcManager.GetAdcApi().GetGslbPoolUuid()
	}

	// get adc host
	err, adcHostApis := c.adcManager.GetAdcApi().DomainName().List()
	if err != nil {
		return err
	}

	adcHosts := make(map[string]adccontroller.AdcHostApi)
	for _, adcHostApi := range adcHostApis {
		adcHosts[adcHostApi.Name] = adccontroller.AdcHostApi{
			Enable:   adcHostApi.Enable,
			Gslbpool: adcHostApi.Gslbpool,
			Uuid:     adcHostApi.Uuid,
		}
	}

	// adc need delete
	deletes := make(map[string]*adccontroller.AdcHostApi)
	for _, host := range removedHosts {
		if adc, ok := adcHosts[host]; ok {
			deletes[host] = &adccontroller.AdcHostApi{
				Enable:   adc.Enable,
				Gslbpool: adc.Gslbpool,
				Uuid:     adc.Uuid,
			}
		}
	}

	// adc needs update,Because the uuid changes, configMap is changed
	updates := make(map[string]*adccontroller.AdcHostApi)
	for host, uuid := range c.hosts {
		if adc, ok := adcHosts[host]; ok {
			if uuid != adc.Gslbpool {
				updates[host] = &adccontroller.AdcHostApi{
					Enable:   adc.Enable,
					Gslbpool: uuid,
					Uuid:     adc.Uuid,
				}
			}
		}
	}

	// adc needs add
	adds := make(map[string]*adccontroller.AdcHostApi)
	for _, host := range addedHosts {
		if uuid, ok := c.hosts[host]; ok {
			adds[host] = &adccontroller.AdcHostApi{
				Gslbpool: uuid,
			}
		}
	}

	if len(deletes) != 0 || len(adds) != 0 || len(updates) != 0 {
		task := adc.Task{Kind: adc.Host,
			Obj: &model.AdcHostApiContextWrap{
				Deleted: deletes,
				Added:   adds,
				Updated: updates},
		}

		c.SendChangeToAdcManager(task)
	}

	return nil

}
func (c *Configurator) SendChangeToAdcManager(t adc.Task) {

	if c.adcManager != nil {
		c.adcManager.SendRequestToQueue(t)
	}

}

func generateRandomString(n int) (string, error) {
	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-"
	ret := make([]byte, n)
	for i := 0; i < n; i++ {
		num, err := rand.Int(rand.Reader, big.NewInt(int64(len(letters))))
		if err != nil {
			return "", err
		}
		ret[i] = letters[num.Int64()]
	}

	return string(ret), nil
}

func generateNjetKicSlbPoolKey(svcKey string) (string, string) {
	if len(strings.Split(svcKey, "/")) != 2 {
		random, _ := generateRandomString(6)
		svcKey = "tmlake-njet-kic-" + random
		tcpKey := svcKey + "-tcp"
		tlsKey := svcKey + "-tls"

		return tcpKey, tlsKey
	}
	if len(svcKey) >= 28 {
		strs := strings.Split(svcKey, "/")
		if len(strs[1]) >= 28 {
			svcKey = strs[1][:27]
		} else {
			svcKey = strs[1]
		}
	} else {
		svcKey = strings.ReplaceAll(svcKey, "/", "-")
	}
	tcpKey := svcKey + "-tcp"
	tlsKey := svcKey + "-tls"

	return tcpKey, tlsKey
}
