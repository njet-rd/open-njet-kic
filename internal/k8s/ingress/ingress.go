package ingress

import (
	"errors"
	extensionsv1beta1 "k8s.io/api/extensions/v1beta1"
	networkingv1 "k8s.io/api/networking/v1"
	networkingv1beta1 "k8s.io/api/networking/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

const (
	// networking.k8s.io/v1
	IngressVersionNetworkingV1 = "networking/v1"
	// networking.k8s.io/v1beta1
	IngressVersionNetworkingV1beta1 = "networking/v1beta1"
	// extensions/v1beta1
	IngressVersionExtensionsV1beta1 = "extensions/v1beta1"
)

type IngressListWrap struct {
	groupVersion      string
	v1                *networkingv1.IngressList
	v1beta1           *networkingv1beta1.IngressList
	extensionsV1beta1 *extensionsv1beta1.IngressList
}

func NewIngressListWrap(obj interface{}) (*IngressListWrap, error) {
	switch obj.(type) {
	case *networkingv1.Ingress:
		return &IngressListWrap{
			groupVersion: IngressVersionNetworkingV1,
			v1:           &networkingv1.IngressList{},
		}, nil
	case *networkingv1beta1.Ingress:
		return &IngressListWrap{
			groupVersion: IngressVersionNetworkingV1beta1,
			v1beta1:      &networkingv1beta1.IngressList{},
		}, nil
	case *extensionsv1beta1.Ingress:
		return &IngressListWrap{
			groupVersion:      IngressVersionExtensionsV1beta1,
			extensionsV1beta1: &extensionsv1beta1.IngressList{},
		}, nil
	default:
		return nil, errors.New("invalid ingress type")
	}
}

func (ingl *IngressListWrap) V1() *networkingv1.IngressList {
	return ingl.v1
}

func (ingl *IngressListWrap) V1beta1() *networkingv1beta1.IngressList {
	return ingl.v1beta1
}

func (ingl *IngressListWrap) ExtensionsV1beta1() *extensionsv1beta1.IngressList {
	return ingl.extensionsV1beta1
}

func (ingl *IngressListWrap) GroupVersion() string {
	return ingl.groupVersion
}

// There seems to be no composite interface in the kubernetes api package,
// so we have to declare our own.
type apiObject interface {
	metav1.Object
	runtime.Object
}

type Ingress interface {
	GroupVersion() string
	V1() *networkingv1.Ingress
	V1beta1() *networkingv1beta1.Ingress
	ExtensionsV1beta1() *extensionsv1beta1.Ingress
	ResourceVersion() string
	ObjectMeta() metav1.ObjectMeta
	Inress() runtime.Object
	ApiObjet() apiObject
}

type IngressWrap struct {
	groupVersion      string
	v1                *networkingv1.Ingress
	v1beta1           *networkingv1beta1.Ingress
	extensionsV1beta1 *extensionsv1beta1.Ingress
}

func NewIngressWrap(obj interface{}) (*IngressWrap, error) {
	switch ing := obj.(type) {
	case *networkingv1.Ingress:
		ing = obj.(*networkingv1.Ingress).DeepCopy()
		return &IngressWrap{
			groupVersion: IngressVersionNetworkingV1,
			v1:           ing,
		}, nil
	case *networkingv1beta1.Ingress:
		ing = obj.(*networkingv1beta1.Ingress).DeepCopy()
		return &IngressWrap{
			groupVersion: IngressVersionNetworkingV1beta1,
			v1beta1:      ing,
		}, nil
	case *extensionsv1beta1.Ingress:
		ing = obj.(*extensionsv1beta1.Ingress).DeepCopy()
		return &IngressWrap{
			groupVersion:      IngressVersionExtensionsV1beta1,
			extensionsV1beta1: ing,
		}, nil
	default:
		return nil, errors.New("invalid ingress type")
	}
}

func (ing *IngressWrap) V1() *networkingv1.Ingress {
	if ing.groupVersion != IngressVersionNetworkingV1 {
		panic("not a networking/v1 ingress")
	}
	return ing.v1
}

func (ing *IngressWrap) V1beta1() *networkingv1beta1.Ingress {
	if ing.groupVersion != IngressVersionNetworkingV1beta1 {
		panic("not a networking/v1beta1 ingress")
	}
	return ing.v1beta1
}

func (ing *IngressWrap) ExtensionsV1beta1() *extensionsv1beta1.Ingress {
	if ing.groupVersion != IngressVersionExtensionsV1beta1 {
		panic("not a extensions/v1beta1 ingress")
	}
	return ing.extensionsV1beta1
}

func (ing *IngressWrap) GroupVersion() string {
	return ing.groupVersion
}

func (ing *IngressWrap) ResourceVersion() string {
	if ing.GroupVersion() == IngressVersionNetworkingV1 {
		return ing.V1().ResourceVersion
	}
	if ing.GroupVersion() == IngressVersionNetworkingV1beta1 {
		return ing.V1beta1().ResourceVersion
	}
	return ing.ExtensionsV1beta1().ResourceVersion
}

func (ing *IngressWrap) ObjectMeta() metav1.ObjectMeta {
	if ing.GroupVersion() == IngressVersionNetworkingV1 {
		return ing.V1().ObjectMeta
	}
	if ing.GroupVersion() == IngressVersionNetworkingV1beta1 {
		return ing.V1beta1().ObjectMeta
	}
	return ing.ExtensionsV1beta1().ObjectMeta
}

func (ing *IngressWrap) Inress() runtime.Object {
	if ing.GroupVersion() == IngressVersionNetworkingV1 {
		return ing.V1()
	}
	if ing.GroupVersion() == IngressVersionNetworkingV1beta1 {
		return ing.V1beta1()
	}
	return ing.ExtensionsV1beta1()
}

func (ing *IngressWrap) ApiObjet() apiObject {
	if ing.GroupVersion() == IngressVersionNetworkingV1 {
		return ing.V1()
	}
	if ing.GroupVersion() == IngressVersionNetworkingV1beta1 {
		return ing.V1beta1()
	}
	return ing.ExtensionsV1beta1()
}
