/*
Copyright 2015 The Kubernetes Authors All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package k8s

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	discovery_v1 "k8s.io/api/discovery/v1"

	v1 "k8s.io/api/core/v1"
	extensionsv1beta1 "k8s.io/api/extensions/v1beta1"
	networkingv1 "k8s.io/api/networking/v1"
	networkingv1beta1 "k8s.io/api/networking/v1beta1"

	internalk8singress "github.com/nginxinc/kubernetes-ingress/internal/k8s/ingress"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/apimachinery/pkg/util/version"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
)

// storeToIngressLister makes a Store that lists Ingress.
// TODO: Move this to cache/listers post 1.1.
type storeToIngressLister struct {
	cache.Store
}

// GetByKeySafe calls Store.GetByKeySafe and returns a copy of the ingress, so it is
// safe to modify.
func (s *storeToIngressLister) GetByKeySafe(key string) (ing *internalk8singress.IngressWrap, exists bool, err error) {
	item, exists, err := s.Store.GetByKey(key)
	if !exists || err != nil {
		return nil, exists, err
	}

	ingWrap, err := internalk8singress.NewIngressWrap(item)
	if err != nil {
		return nil, exists, err
	}
	return ingWrap, exists, nil
}

// List lists all Ingress' in the store.
func (s *storeToIngressLister) List() (ingl *internalk8singress.IngressListWrap, err error) {
	for _, obj := range s.Store.List() {
		switch obj.(type) {
		case *networkingv1.Ingress:
			if ingl.V1() == nil {
				ingl, err = internalk8singress.NewIngressListWrap(obj)
				if err != nil {
					return nil, err
				}
			}
			ingl.V1().Items = append(ingl.V1().Items, *(obj.(*networkingv1.Ingress)).DeepCopy())
		case *networkingv1beta1.Ingress:
			if ingl.V1beta1() == nil {
				ingl, err = internalk8singress.NewIngressListWrap(obj)
				if err != nil {
					return nil, err
				}
			}
			ingl.V1beta1().Items = append(ingl.V1beta1().Items, *(obj.(*networkingv1beta1.Ingress)).DeepCopy())
		case *extensionsv1beta1.Ingress:
			if ingl.ExtensionsV1beta1() == nil {
				ingl, err = internalk8singress.NewIngressListWrap(obj)
				if err != nil {
					return nil, err
				}
			}
			ingl.ExtensionsV1beta1().Items = append(ingl.ExtensionsV1beta1().Items, *(obj.(*extensionsv1beta1.Ingress)).DeepCopy())
		default:
			return nil, errors.New("invalid ingress type")
		}
	}
	return ingl, nil
}

// storeToConfigMapLister makes a Store that lists ConfigMaps
type storeToConfigMapLister struct {
	cache.Store
}

// List lists all Ingress' in the store.
func (s *storeToConfigMapLister) List() (cfgm v1.ConfigMapList, err error) {
	for _, m := range s.Store.List() {
		cfgm.Items = append(cfgm.Items, *(m.(*v1.ConfigMap)))
	}
	return cfgm, nil
}

// indexerToPodLister makes an Indexer that lists Pods.
type indexerToPodLister struct {
	cache.Indexer
}

// ListByNamespace lists all Pods in the indexer for a given namespace that match the provided selector.
func (ipl indexerToPodLister) ListByNamespace(ns string, selector labels.Selector) (pods []*v1.Pod, err error) {
	err = cache.ListAllByNamespace(ipl.Indexer, ns, selector, func(m interface{}) {
		pods = append(pods, m.(*v1.Pod))
	})
	return pods, err
}

// Store for EndpointSlices
type storeToEndpointSliceLister struct {
	cache.Store
}

// GetServiceEndpointSlices returns the endpoints of a service, matched on service name.
func (s *storeToEndpointSliceLister) GetServiceEndpointSlices(svc *v1.Service) (endpointSlices []discovery_v1.EndpointSlice, err error) {
	for _, epStore := range s.Store.List() {
		ep := *epStore.(*discovery_v1.EndpointSlice)
		if svc.Name == ep.Labels["kubernetes.io/service-name"] && svc.Namespace == ep.Namespace {
			endpointSlices = append(endpointSlices, ep)
		}
	}
	if len(endpointSlices) > 0 {
		return endpointSlices, nil
	}
	return endpointSlices, fmt.Errorf("could not find endpointslices for service: %v", svc.Name)
}

// storeToEndpointLister makes a Store that lists Endpoints
type storeToEndpointLister struct {
	cache.Store
}

// GetServiceEndpoints returns the endpoints of a service, matched on service name.
func (s *storeToEndpointLister) GetServiceEndpoints(svc *v1.Service) (ep v1.Endpoints, err error) {
	for _, m := range s.Store.List() {
		ep = *m.(*v1.Endpoints)
		if svc.Name == ep.Name && svc.Namespace == ep.Namespace {
			return ep, nil
		}
	}
	return ep, fmt.Errorf("could not find endpoints for service: %v", svc.Name)
}

// findPort locates the container port for the given pod and portName.  If the
// targetPort is a number, use that.  If the targetPort is a string, look that
// string up in all named ports in all containers in the target pod.  If no
// match is found, fail.
func findPort(pod *v1.Pod, svcPort v1.ServicePort) (int32, error) {
	portName := svcPort.TargetPort
	switch portName.Type {
	case intstr.String:
		name := portName.StrVal
		for _, container := range pod.Spec.Containers {
			for _, port := range container.Ports {
				if port.Name == name && port.Protocol == svcPort.Protocol {
					return port.ContainerPort, nil
				}
			}
		}
	case intstr.Int:
		return int32(portName.IntValue()), nil
	}

	return 0, fmt.Errorf("no suitable port for manifest: %s", pod.UID)
}

// isMinion determines is an ingress is a minion or not
func isMinion(ingWrap *internalk8singress.IngressWrap) bool {
	switch ingWrap.GroupVersion() {
	case internalk8singress.IngressVersionNetworkingV1:
		ing := ingWrap.V1()
		return ing.Annotations["njet.org.cn/mergeable-ingress-type"] == "minion"
	case internalk8singress.IngressVersionNetworkingV1beta1:
		ing := ingWrap.V1beta1()
		return ing.Annotations["njet.org.cn/mergeable-ingress-type"] == "minion"
	case internalk8singress.IngressVersionExtensionsV1beta1:
		ing := ingWrap.ExtensionsV1beta1()
		return ing.Annotations["njet.org.cn/mergeable-ingress-type"] == "minion"
	}
	return false
}

// isMaster determines is an ingress is a master or not
func isMaster(ingWrap *internalk8singress.IngressWrap) bool {
	switch ingWrap.GroupVersion() {
	case internalk8singress.IngressVersionNetworkingV1:
		ing := ingWrap.V1()
		return ing.Annotations["njet.org.cn/mergeable-ingress-type"] == "master"
	case internalk8singress.IngressVersionNetworkingV1beta1:
		ing := ingWrap.V1beta1()
		return ing.Annotations["njet.org.cn/mergeable-ingress-type"] == "master"
	case internalk8singress.IngressVersionExtensionsV1beta1:
		ing := ingWrap.ExtensionsV1beta1()
		return ing.Annotations["njet.org.cn/mergeable-ingress-type"] == "master"
	}
	return false
}

func isChallengeIngress(ingWrap *internalk8singress.IngressWrap) bool {
	switch ingWrap.GroupVersion() {
	case internalk8singress.IngressVersionNetworkingV1:
		ing := ingWrap.V1()
		return ing.Labels["acme.cert-manager.io/http01-solver"] == "true"
	case internalk8singress.IngressVersionNetworkingV1beta1:
		ing := ingWrap.V1beta1()
		return ing.Labels["acme.cert-manager.io/http01-solver"] == "true"
	case internalk8singress.IngressVersionExtensionsV1beta1:
		ing := ingWrap.ExtensionsV1beta1()
		return ing.Labels["acme.cert-manager.io/http01-solver"] == "true"
	}
	return false
}

// hasChanges determines if current ingress has changes compared to old ingress
func hasChanges(old interface{}, current interface{}) (bool, string) {
	switch old.(type) {
	case *networkingv1.Ingress:
		c := current.(*networkingv1.Ingress)
		o := old.(*networkingv1.Ingress)
		o.Status.LoadBalancer.Ingress = c.Status.LoadBalancer.Ingress
		o.ResourceVersion = c.ResourceVersion
		return !reflect.DeepEqual(o, c), c.Name
	case *networkingv1beta1.Ingress:
		c := current.(*networkingv1beta1.Ingress)
		o := old.(*networkingv1beta1.Ingress)
		o.Status.LoadBalancer.Ingress = c.Status.LoadBalancer.Ingress
		o.ResourceVersion = c.ResourceVersion
		return !reflect.DeepEqual(o, c), c.Name
	case *extensionsv1beta1.Ingress:
		c := current.(*extensionsv1beta1.Ingress)
		o := old.(*extensionsv1beta1.Ingress)
		o.Status.LoadBalancer.Ingress = c.Status.LoadBalancer.Ingress
		o.ResourceVersion = c.ResourceVersion
		return !reflect.DeepEqual(o, c), c.Name
	}

	return false, ""
}

func getIngressName(obj interface{}) string {
	switch ing := obj.(type) {
	case *networkingv1.Ingress:
		return ing.Name
	case *networkingv1beta1.Ingress:
		return ing.Name
	case *extensionsv1beta1.Ingress:
		return ing.Name
	default:
		panic("invalid ingress type")
	}

	return ""
}

// ParseNamespaceName parses the string in the <namespace>/<name> format and returns the name and the namespace.
// It returns an error in case the string does not follow the <namespace>/<name> format.
func ParseNamespaceName(value string) (ns string, name string, err error) {
	res := strings.Split(value, "/")
	if len(res) != 2 {
		return "", "", fmt.Errorf("%q must follow the format <namespace>/<name>", value)
	}
	return res[0], res[1], nil
}

// GetK8sVersion returns the running version of k8s
func GetK8sVersion(client kubernetes.Interface) (v *version.Version, err error) {
	serverVersion, err := client.Discovery().ServerVersion()
	if err != nil {
		return nil, err
	}

	runningVersion, err := version.ParseGeneric(serverVersion.String())
	if err != nil {
		return nil, fmt.Errorf("unexpected error parsing running Kubernetes version: %w", err)
	}
	return runningVersion, nil
}

type Instance map[string]string

// SubsetOf is true if the label has identical values for the keys
func (i Instance) SubsetOf(that Instance) bool {
	if len(i) == 0 || len(that) == 0 || len(that) < len(i) {
		return false
	}

	for k, v1 := range i {
		if v2, ok := that[k]; !ok || v1 != v2 {
			return false
		}
	}
	return true
}
