package k8s

import (
	"fmt"
	corev1 "k8s.io/api/core/v1"
	discoveryv1 "k8s.io/api/discovery/v1"
	networking "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

type endpointLister struct {
	useEndpointSlice bool
}

func (lbc *LoadBalancerController) GetEndpoint(svc *corev1.Service) (Endpoint, error) {
	if lbc.WatchEndpointslices {
		return lbc.getEndpointSlices(svc)
	}
	return lbc.getEndpoint(svc)
}

func (lbc *LoadBalancerController) getEndpoint(svc *corev1.Service) (Endpoint, error) {
	ep, err := lbc.getNamespacedInformer(svc.Namespace).endpointLister.GetServiceEndpoints(svc)
	return &endpoint{
		endpointType: endpointTypeEndpoints,
		endpoint:     &ep,
	}, err
}

func (lbc *LoadBalancerController) getEndpointSlices(svc *corev1.Service) (Endpoint, error) {
	endpointSlices, err := lbc.getNamespacedInformer(svc.Namespace).endpointSliceLister.GetServiceEndpointSlices(svc)
	return &endpoint{
		endpointType:   endpointTypeEndpointSlices,
		endpointSlices: endpointSlices,
	}, err
}

// Endpoint is an encapsulation for the Kubernetes Endpoint and EndpointSlice objects.
type Endpoint interface {
	// Endpoints returns the corresponding endpoints which matches the ServicePort.
	GetEndpointsForPortForIngressVersionNetworkingV1beta1(lbc *LoadBalancerController, ingSvcPort intstr.IntOrString, svc *corev1.Service) ([]podEndpoint, error)
	GetEndpointsForPortForIngressVersionNetworkingV1(lbc *LoadBalancerController, backendPort networking.ServiceBackendPort, svc *corev1.Service) ([]podEndpoint, error)
}

type endpointType string

const (
	endpointTypeEndpoints      endpointType = "Endpoint"
	endpointTypeEndpointSlices endpointType = "EndpointSlices"
)

type endpoint struct {
	endpointType   endpointType
	endpoint       *corev1.Endpoints
	endpointSlices []discoveryv1.EndpointSlice
}

func (e *endpoint) GetEndpointsForPortForIngressVersionNetworkingV1beta1(lbc *LoadBalancerController, ingSvcPort intstr.IntOrString, svc *corev1.Service) ([]podEndpoint, error) {
	var targetPort int32
	var err error

	for _, port := range svc.Spec.Ports {
		if (ingSvcPort.Type == intstr.Int && port.Port == int32(ingSvcPort.IntValue())) || (ingSvcPort.Type == intstr.String && port.Name == ingSvcPort.String()) {
			targetPort, err = lbc.getTargetPort(port, svc)
			if err != nil {
				return nil, fmt.Errorf("Error determining target port for port %v in Ingress: %w", ingSvcPort, err)
			}
			break
		}
	}

	if targetPort == 0 {
		return nil, fmt.Errorf("No port %v in service %s", ingSvcPort, svc.Name)
	}

	if e.endpoint != nil {
		for _, subset := range e.endpoint.Subsets {
			for _, port := range subset.Ports {
				if port.Port == targetPort {
					var endpoints []podEndpoint
					for _, address := range subset.Addresses {
						addr := ipv6SafeAddrPort(address.IP, port.Port)
						podEnd := podEndpoint{
							Address: addr,
						}
						if address.TargetRef != nil {
							parentType, parentName := lbc.getPodOwnerTypeAndNameFromAddress(address.TargetRef.Namespace, address.TargetRef.Name)
							podEnd.OwnerType = parentType
							podEnd.OwnerName = parentName
							podEnd.PodName = address.TargetRef.Name
						}
						endpoints = append(endpoints, podEnd)
					}
					return endpoints, nil
				}
			}
		}

		return nil, fmt.Errorf("No endpoints for target port %v in service %s", targetPort, svc.Name)
	} else {
		endpointSet := make(map[podEndpoint]struct{})
		for _, endpointSlice := range e.endpointSlices {
			for _, endpointSlicePort := range endpointSlice.Ports {
				if *endpointSlicePort.Port == targetPort {
					for _, endpoint := range endpointSlice.Endpoints {
						if !*endpoint.Conditions.Ready {
							continue
						}
						for _, endpointAddress := range endpoint.Addresses {
							address := ipv6SafeAddrPort(endpointAddress, *endpointSlicePort.Port)
							podEndpoint := podEndpoint{
								Address: address,
							}
							if endpoint.TargetRef != nil {
								parentType, parentName := lbc.getPodOwnerTypeAndNameFromAddress(endpoint.TargetRef.Namespace, endpoint.TargetRef.Name)
								podEndpoint.OwnerType = parentType
								podEndpoint.OwnerName = parentName
								podEndpoint.PodName = endpoint.TargetRef.Name
							}
							endpointSet[podEndpoint] = struct{}{}
						}
					}
				}
			}
		}
		if len(endpointSet) == 0 {
			return nil, fmt.Errorf("no endpointslices for target port %v in service %s", targetPort, svc.Name)
		}
		endpoints := make([]podEndpoint, 0, len(endpointSet))
		for ep := range endpointSet {
			endpoints = append(endpoints, ep)
		}
		return endpoints, nil
	}
}

func (e *endpoint) GetEndpointsForPortForIngressVersionNetworkingV1(lbc *LoadBalancerController, backendPort networking.ServiceBackendPort, svc *corev1.Service) ([]podEndpoint, error) {
	var targetPort int32
	var err error

	for _, port := range svc.Spec.Ports {
		if (backendPort.Name == "" && port.Port == backendPort.Number) || port.Name == backendPort.Name {
			targetPort, err = lbc.getTargetPort(port, svc)
			if err != nil {
				return nil, fmt.Errorf("error determining target port for port %v in Ingress: %w", backendPort, err)
			}
			break
		}
	}

	if targetPort == 0 {
		return nil, fmt.Errorf("no port %v in service %s", backendPort, svc.Name)
	}

	if e.endpoint != nil {
		for _, subset := range e.endpoint.Subsets {
			for _, port := range subset.Ports {
				if port.Port == targetPort {
					var endpoints []podEndpoint
					for _, address := range subset.Addresses {
						addr := ipv6SafeAddrPort(address.IP, port.Port)
						podEnd := podEndpoint{
							Address: addr,
						}
						if address.TargetRef != nil {
							parentType, parentName := lbc.getPodOwnerTypeAndNameFromAddress(address.TargetRef.Namespace, address.TargetRef.Name)
							podEnd.OwnerType = parentType
							podEnd.OwnerName = parentName
							podEnd.PodName = address.TargetRef.Name
						}
						endpoints = append(endpoints, podEnd)
					}
					return endpoints, nil
				}
			}
		}

		return nil, fmt.Errorf("No endpoints for target port %v in service %s", targetPort, svc.Name)
	} else {
		endpointSet := make(map[podEndpoint]struct{})
		for _, endpointSlice := range e.endpointSlices {
			for _, endpointSlicePort := range endpointSlice.Ports {
				if *endpointSlicePort.Port == targetPort {
					for _, endpoint := range endpointSlice.Endpoints {
						if !*endpoint.Conditions.Ready {
							continue
						}
						for _, endpointAddress := range endpoint.Addresses {
							address := ipv6SafeAddrPort(endpointAddress, *endpointSlicePort.Port)
							podEndpoint := podEndpoint{
								Address: address,
							}
							if endpoint.TargetRef != nil {
								parentType, parentName := lbc.getPodOwnerTypeAndNameFromAddress(endpoint.TargetRef.Namespace, endpoint.TargetRef.Name)
								podEndpoint.OwnerType = parentType
								podEndpoint.OwnerName = parentName
								podEndpoint.PodName = endpoint.TargetRef.Name
							}
							endpointSet[podEndpoint] = struct{}{}
						}
					}
				}
			}
		}
		if len(endpointSet) == 0 {
			return nil, fmt.Errorf("no endpointslices for target port %v in service %s", targetPort, svc.Name)
		}
		endpoints := make([]podEndpoint, 0, len(endpointSet))
		for ep := range endpointSet {
			endpoints = append(endpoints, ep)
		}
		return endpoints, nil
	}

}
