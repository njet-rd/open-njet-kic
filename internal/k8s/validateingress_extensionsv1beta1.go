package k8s

import (
	internalk8singress "github.com/nginxinc/kubernetes-ingress/internal/k8s/ingress"
	extensionsv1beta1 "k8s.io/api/extensions/v1beta1"
	"k8s.io/apimachinery/pkg/util/sets"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

func validateIngressVersionExtensionsV1beta1(
	ingWrap *internalk8singress.IngressWrap,
	ing *extensionsv1beta1.Ingress,
	isPlus bool,
	appProtectEnabled bool,
	appProtectDosEnabled bool,
	internalRoutesEnabled bool,
	snippetsEnabled bool,
) field.ErrorList {
	allErrs := field.ErrorList{}
	allErrs = append(allErrs, validateIngressAnnotations(
		ing.Annotations,
		getSpecServicesForExtensionsV1beta1(ing.Spec),
		isPlus,
		appProtectEnabled,
		appProtectDosEnabled,
		internalRoutesEnabled,
		field.NewPath("annotations"),
		snippetsEnabled,
	)...)

	allErrs = append(allErrs, validateIngressSpecForExtensionsV1beta1(&ing.Spec, field.NewPath("spec"))...)

	if isMaster(ingWrap) {
		allErrs = append(allErrs, validateMasterSpecForExtensionsV1beta1(&ing.Spec, field.NewPath("spec"))...)
	} else if isMinion(ingWrap) {
		allErrs = append(allErrs, validateMinionSpecForExtensionsV1beta1(&ing.Spec, field.NewPath("spec"))...)
	}

	if isChallengeIngress(ingWrap) {
		allErrs = append(allErrs, validateChallengeIngressForExtensionsV1beta1(&ing.Spec, field.NewPath("spec"))...)
	}
	return allErrs
}

func getSpecServicesForExtensionsV1beta1(ingressSpec extensionsv1beta1.IngressSpec) map[string]bool {
	services := make(map[string]bool)
	if ingressSpec.Backend != nil {
		services[ingressSpec.Backend.ServiceName] = true
	}
	for _, rule := range ingressSpec.Rules {
		if rule.HTTP != nil {
			for _, path := range rule.HTTP.Paths {
				services[path.Backend.ServiceName] = true
			}
		}
	}
	return services
}

func validateIngressSpecForExtensionsV1beta1(spec *extensionsv1beta1.IngressSpec, fieldPath *field.Path) field.ErrorList {
	allErrs := field.ErrorList{}

	if spec.Backend != nil {
		allErrs = append(allErrs, validateBackendForExtensionsV1beta1(spec.Backend, fieldPath.Child("Backend"))...)
	}

	allHosts := sets.String{}

	if len(spec.Rules) == 0 {
		return append(allErrs, field.Required(fieldPath.Child("rules"), ""))
	}

	for i, r := range spec.Rules {
		idxRule := fieldPath.Child("rules").Index(i)

		if r.Host == "" {
			allErrs = append(allErrs, field.Required(idxRule.Child("host"), ""))
		} else if allHosts.Has(r.Host) {
			allErrs = append(allErrs, field.Duplicate(idxRule.Child("host"), r.Host))
		} else {
			allHosts.Insert(r.Host)
		}

		if r.HTTP == nil {
			continue
		}

		allErrs = append(allErrs, validatePathsForExtensionsV1beta1(r.HTTP.Paths, idxRule.Child("http").Child("paths"))...)

		for _, path := range r.HTTP.Paths {
			idxPath := idxRule.Child("http").Child("path").Index(i)

			allErrs = append(allErrs, validatePath(path.Path, idxPath.Child("path"))...)
			allErrs = append(allErrs, validateBackendForExtensionsV1beta1(&path.Backend, idxPath.Child("backend"))...)
		}
	}

	return allErrs
}

func validateBackendForExtensionsV1beta1(backend *extensionsv1beta1.IngressBackend, fieldPath *field.Path) field.ErrorList {
	allErrs := field.ErrorList{}

	if backend.Resource != nil {
		return append(allErrs, field.Forbidden(fieldPath.Child("resource"), "resource backends are not supported"))
	}

	return allErrs
}

func validatePathsForExtensionsV1beta1(paths []extensionsv1beta1.HTTPIngressPath, fieldPath *field.Path) field.ErrorList {
	allErrs := field.ErrorList{}
	allPaths := make(map[string]extensionsv1beta1.PathType)

	for i, r := range paths {
		idxPath := fieldPath.Index(i)

		pre, ok := allPaths[r.Path]
		if ok && *r.PathType == pre {
			allErrs = append(allErrs, field.Duplicate(idxPath.Child("path"), r.Path))
		} else {
			allPaths[r.Path] = *r.PathType
		}
	}
	return allErrs
}

func validateMasterSpecForExtensionsV1beta1(spec *extensionsv1beta1.IngressSpec, fieldPath *field.Path) field.ErrorList {
	allErrs := field.ErrorList{}

	if len(spec.Rules) != 1 {
		return append(allErrs, field.TooMany(fieldPath.Child("rules"), len(spec.Rules), 1))
	}

	// the number of paths of the first rule of the spec must be 0
	if spec.Rules[0].HTTP != nil && len(spec.Rules[0].HTTP.Paths) > 0 {
		pathsField := fieldPath.Child("rules").Index(0).Child("http").Child("paths")
		return append(allErrs, field.TooMany(pathsField, len(spec.Rules[0].HTTP.Paths), 0))
	}

	return allErrs
}

func validateMinionSpecForExtensionsV1beta1(spec *extensionsv1beta1.IngressSpec, fieldPath *field.Path) field.ErrorList {
	allErrs := field.ErrorList{}

	if len(spec.TLS) > 0 {
		allErrs = append(allErrs, field.TooMany(fieldPath.Child("tls"), len(spec.TLS), 0))
	}

	if len(spec.Rules) != 1 {
		return append(allErrs, field.TooMany(fieldPath.Child("rules"), len(spec.Rules), 1))
	}

	// the number of paths of the first rule of the spec must be greater than 0
	if spec.Rules[0].HTTP == nil || len(spec.Rules[0].HTTP.Paths) == 0 {
		pathsField := fieldPath.Child("rules").Index(0).Child("http").Child("paths")
		return append(allErrs, field.Required(pathsField, "must include at least one path"))
	}

	return allErrs
}

func validateChallengeIngressForExtensionsV1beta1(spec *extensionsv1beta1.IngressSpec, fieldPath *field.Path) field.ErrorList {
	allErrs := field.ErrorList{}
	if spec.Rules == nil || len(spec.Rules) != 1 {
		allErrs = append(allErrs, field.Forbidden(fieldPath.Child("rules"), "challenge Ingress must have exactly 1 rule defined"))
		return allErrs
	}
	r := spec.Rules[0]

	if r.HTTP == nil || r.HTTP.Paths == nil || len(r.HTTP.Paths) != 1 {
		allErrs = append(allErrs, field.Forbidden(fieldPath.Child("rules.HTTP.Paths"), "challenge Ingress must have exactly 1 path defined"))
		return allErrs
	}

	p := r.HTTP.Paths[0]

	if p.Backend.ServiceName == "" {
		allErrs = append(allErrs, field.Required(fieldPath.Child("rules.HTTP.Paths[0].Backend.ServiceName"), "challenge Ingress must have a Backend Service defined"))
	}

	//if p.Backend.Service.Port.Name != "" {
	//	allErrs = append(allErrs, field.Forbidden(fieldPath.Child("rules.HTTP.Paths[0].Backend.Service.Port.Name"), "challenge Ingress must have a Backend Service Port Number defined, not Name"))
	//}
	return allErrs
}
