package model

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/nginxinc/kubernetes-ingress/internal/configs/mainconfig"

	"github.com/golang/glog"
	"github.com/nginxinc/kubernetes-ingress/internal/configs/version1"
	"github.com/nginxinc/kubernetes-ingress/internal/configs/version2"
	njetapicontroller "github.com/nginxinc/kubernetes-ingress/internal/nginx/njetapi/controller"
)

// Operation defines an operation to perform for a resource.
type Operation int

const (
	// Delete the config of the resource
	Delete Operation = iota
	// AddOrUpdate the config of the resource
	AddOrUpdate
)

const (
	DynLocationAddressAndPort80  = "0.0.0.0:80"
	DynLocationAddressAndPort443 = "0.0.0.0:443"
	DynLocationServerName        = ""
)

type ResourceType int

const (
	UnknownResource ResourceType = iota
	SecretResource
	ServiceResource
	IngressResource
	EndpointResource
	VirtualServerResource
	LuaUpstream
	WorkerResource
	LogResource
	TransportServerResource
)

func (r ResourceType) String() string {
	switch r {

	case SecretResource:
		return "SecretResource"
	case ServiceResource:
		return "ServiceResource"
	case IngressResource:
		return "IngressResource"
	case EndpointResource:
		return "EndpointResource"
	case VirtualServerResource:
		return "VirtualServerResource"
	case LuaUpstream:
		return "LuaUpstream"
	case WorkerResource:
		return "WorkerResource"
	case LogResource:
		return "LogResource"
	default:
		return "UnknownResource"
	}
}

type K8sResourceInfo struct {
	RType     ResourceType
	Raw       interface{}
	Operation Operation
}

type PushNjetApiResourceContext struct {
	IngressNginxConfig    *version1.IngressNginxConfig
	VirtualServerConfig   *version2.VirtualServerConfig
	WorkerConfig          *mainconfig.WorkerConfig
	LogConfig             *mainconfig.LogConfig
	TransportServerConfig *version2.TransportServerConfig
	K8sResourceInfo       *K8sResourceInfo
}

// NewPushContext creates a new PushContext structure.
func NewPushNjetApiResourceContext() *PushNjetApiResourceContext {
	return &PushNjetApiResourceContext{}
}

type NjetApiContextWrap struct {
	Added   *NjetApiContext
	Updated *NjetApiContext
	Deleted *NjetApiContext
}

type NjetApiContext struct {
	Routes                         []*njetapicontroller.NjetRouteApi
	Upstreams                      []*njetapicontroller.NjetUpstreamApi
	CertMapping                    *njetapicontroller.NjetMapApi
	WorkerSetting                  *njetapicontroller.NjetWorkerApi
	AccessLog                      *njetapicontroller.AccessLogData
	PortRedirect                   *njetapicontroller.NjetPortRedirectApi
	StreamMap                      *njetapicontroller.NjetStreamMapApi
	StreamMapForUDPPortAndUpstream *njetapicontroller.NjetStreamMapApi
	Vss                            []*njetapicontroller.NjetVsApi
}

func CloneVsAndRouteForHttps(wrap *NjetApiContextWrap) {
	if wrap == nil {
		return
	}

	if wrap.Added != nil {
		for _, ar := range wrap.Added.Routes {
			tempar := *ar
			tempar.AddressAndPort = DynLocationAddressAndPort443
			wrap.Added.Routes = append(wrap.Added.Routes, &tempar)
		}
		for _, avs := range wrap.Added.Vss {
			tempavs := *avs
			tempavs.AddressAndPort = DynLocationAddressAndPort443
			wrap.Added.Vss = append(wrap.Added.Vss, &tempavs)
		}
	}

	if wrap.Deleted != nil {
		for _, dr := range wrap.Deleted.Routes {
			tempdr := *dr
			tempdr.AddressAndPort = DynLocationAddressAndPort443
			wrap.Deleted.Routes = append(wrap.Deleted.Routes, &tempdr)
		}
		for _, dvs := range wrap.Deleted.Vss {
			tempdvs := *dvs
			tempdvs.AddressAndPort = DynLocationAddressAndPort443
			wrap.Deleted.Vss = append(wrap.Deleted.Vss, &tempdvs)
		}
	}

	if wrap.Updated != nil {
		for _, ur := range wrap.Updated.Routes {
			tempur := *ur
			tempur.AddressAndPort = DynLocationAddressAndPort443
			wrap.Updated.Routes = append(wrap.Updated.Routes, &tempur)
		}

		for _, uvs := range wrap.Updated.Vss {
			tempuvs := *uvs
			tempuvs.AddressAndPort = DynLocationAddressAndPort443
			wrap.Updated.Vss = append(wrap.Updated.Vss, &tempuvs)
		}
	}

}

func CloneAccessLogForHttps(wrap *NjetApiContextWrap) {
	if wrap == nil {
		return
	}

	if wrap.Updated != nil && wrap.Updated.AccessLog != nil {
		for _, us := range wrap.Updated.AccessLog.Servers {
			tempus := us
			tempus.Listens = []string{DynLocationAddressAndPort443}
			wrap.Updated.AccessLog.Servers = append(wrap.Updated.AccessLog.Servers, tempus)
		}
	}

}

func (context *NjetApiContext) MergeVsAndRoute(addedvs []*njetapicontroller.NjetVsApi, updatedvs []*njetapicontroller.NjetVsApi, deletedvs []*njetapicontroller.NjetVsApi,
	addedRoutes []*njetapicontroller.NjetRouteApi, updatedRoutes []*njetapicontroller.NjetRouteApi, deletedRoutes []*njetapicontroller.NjetRouteApi) (added, updated, deleted *NjetApiContext) {

	// vs slice to map
	addedvsMap := make(map[string]*njetapicontroller.NjetVsApi, len(addedvs))
	updatedvsMap := make(map[string]*njetapicontroller.NjetVsApi, len(updatedvs))
	deletedvsMap := make(map[string]*njetapicontroller.NjetVsApi, len(deletedvs))
	for _, vs := range addedvs {
		addedvsMap[vs.ServerName] = vs
	}
	for _, vs := range updatedvs {
		updatedvsMap[vs.ServerName] = vs
	}
	for _, vs := range deletedvs {
		deletedvsMap[vs.ServerName] = vs
	}

	// route slice to map
	addedRoutesMap := make(map[string][]*njetapicontroller.NjetRouteApi, len(addedRoutes))
	updatedRoutesMap := make(map[string][]*njetapicontroller.NjetRouteApi, len(updatedRoutes))
	deletedRoutesMap := make(map[string][]*njetapicontroller.NjetRouteApi, len(deletedRoutes))
	for _, r := range addedRoutes {
		if _, ok := addedRoutesMap[r.ServerName]; ok {
			addedRoutesMap[r.ServerName] = append(addedRoutesMap[r.ServerName], r)
		} else {
			var routes []*njetapicontroller.NjetRouteApi
			routes = append(routes, r)
			addedRoutesMap[r.ServerName] = routes
		}
	}
	for _, r := range updatedRoutes {
		if _, ok := updatedRoutesMap[r.ServerName]; ok {
			updatedRoutesMap[r.ServerName] = append(updatedRoutesMap[r.ServerName], r)
		} else {
			var routes []*njetapicontroller.NjetRouteApi
			routes = append(routes, r)
			updatedRoutesMap[r.ServerName] = routes
		}
	}
	for _, r := range deletedRoutes {
		if _, ok := deletedRoutesMap[r.ServerName]; ok {
			deletedRoutesMap[r.ServerName] = append(deletedRoutesMap[r.ServerName], r)
		} else {
			var routes []*njetapicontroller.NjetRouteApi
			routes = append(routes, r)
			deletedRoutesMap[r.ServerName] = routes
		}
	}

	// merge vs and route
	var deletedRoutesMerge []*njetapicontroller.NjetRouteApi

	// merge delete event
	// Deleting vs also deletes all routes, so clear the routes associated with vs
	for rservername, rs := range deletedRoutesMap {
		for _, r := range rs {
			if _, ok := deletedvsMap[rservername]; !ok {
				deletedRoutesMerge = append(deletedRoutesMerge, r)
			}
		}
	}

	// merge update event
	// do not handle now

	// merge add event
	// nothing to do

	added = &NjetApiContext{
		Routes: addedRoutes,
		Vss:    addedvs,
	}
	updated = &NjetApiContext{
		Routes: updatedRoutes,
	}
	deleted = &NjetApiContext{
		Vss:    deletedvs,
		Routes: deletedRoutesMerge,
	}

	return
}

func (context *NjetApiContext) HasDifferentRoute(old *NjetApiContext) bool {
	ar, ur, dr := context.DifferentRoute(old)
	if len(ar.Routes) == 0 && len(ur.Routes) == 0 && len(dr.Routes) == 0 {
		return false
	}
	return true
}

func (context *NjetApiContext) HasDifferentVs(old *NjetApiContext) bool {
	avs, uvs, dvs := context.DifferentVs(old)
	if len(avs.Vss) == 0 && len(uvs.Vss) == 0 && len(dvs.Vss) == 0 {
		return false
	}
	return true
}

func (context *NjetApiContext) DifferentRoute(old *NjetApiContext) (added, updated, deleted *NjetApiContext) {
	ar, ur, dr := DifferentRoute(old.Routes, context.Routes)
	added = &NjetApiContext{
		Routes: ar,
	}
	updated = &NjetApiContext{
		Routes: ur,
	}
	deleted = &NjetApiContext{
		Routes: dr,
	}
	return
}

func (context *NjetApiContext) DifferentVs(old *NjetApiContext) (added, updated, deleted *NjetApiContext) {
	avs, uvs, dvs := DifferentVs(old.Vss, context.Vss)
	added = &NjetApiContext{
		Vss: avs,
	}
	updated = &NjetApiContext{
		Vss: uvs,
	}
	deleted = &NjetApiContext{
		Vss: dvs,
	}
	return
}

func (context *NjetApiContext) DifferentPortRedirect(old *NjetApiContext) (added, updated, deleted *NjetApiContext) {
	added = &NjetApiContext{}
	deleted = &NjetApiContext{}
	ap, dp := DifferentPortRedirect(old.PortRedirect, context.PortRedirect)
	if ap != nil {
		added.PortRedirect = ap
	}
	if dp != nil {
		deleted.PortRedirect = dp
	}

	return
}

func (context *NjetApiContext) DifferentAccessLog(old *NjetApiContext) bool {
	isDifferent := DifferentAccessLog(old.AccessLog, context.AccessLog)

	return isDifferent
}

func generateRouteKey(r *njetapicontroller.NjetRouteApi) string {
	if r == nil {
		glog.Warningf("NjetRouteApi is nil, RouteKey is %q", "defaultservername-defaultlocationname")
		return "defaultservername-defaultlocationname"
	}

	serverName := strings.ReplaceAll(fmt.Sprintf("%s", r.ServerName), ":", "_")
	var locationName = "defaultlocationname"
	for _, location := range r.Locations {
		locationName = location.LocationName
		break
	}

	routeKey := fmt.Sprintf("%s-%s", serverName, locationName)
	if locationName == "defaultlocationname" {
		glog.Warningf("NjetRouteApi.Locations is nil, RouteKey is %q", routeKey)
	}

	return routeKey
}

func DifferentRoute(olds, newRoutes []*njetapicontroller.NjetRouteApi) (added, updated, deleted []*njetapicontroller.NjetRouteApi) {
	if newRoutes == nil {
		return nil, nil, olds
	}
	if olds == nil {
		return newRoutes, nil, nil
	}

	if len(olds) == 0 && len(newRoutes) == 0 {
		return nil, nil, nil
	}
	oldMap := make(map[string]*njetapicontroller.NjetRouteApi, len(olds))
	newMap := make(map[string]*njetapicontroller.NjetRouteApi, len(newRoutes))
	for _, r := range olds {
		routeKey := generateRouteKey(r)
		oldMap[routeKey] = r
	}
	for _, r := range newRoutes {
		routeKey := generateRouteKey(r)
		newMap[routeKey] = r
	}

	for _, r := range newRoutes {
		routeKey := generateRouteKey(r)
		if or, ok := oldMap[routeKey]; !ok {
			added = append(added, r)
		} else if !reflect.DeepEqual(or, r) {
			updated = append(updated, r)
		}
	}
	for _, r := range olds {
		routeKey := generateRouteKey(r)
		if _, ok := newMap[routeKey]; !ok {
			deleted = append(deleted, r)
		}
	}
	return
}

func DifferentPortRedirect(old, new *njetapicontroller.NjetPortRedirectApi) (added, deleted *njetapicontroller.NjetPortRedirectApi) {
	if new == nil || old == new {
		return nil, nil
	}
	if old == nil {
		return new, nil
	}

	if !reflect.DeepEqual(old, new) {
		return new, old
	}
	return
}

func DifferentVs(olds, news []*njetapicontroller.NjetVsApi) (added, updated, deleted []*njetapicontroller.NjetVsApi) {
	if news == nil {
		return nil, nil, olds
	}
	if olds == nil {
		return news, nil, nil
	}

	if len(olds) == 0 && len(news) == 0 {
		return nil, nil, nil
	}
	oldMap := make(map[string]*njetapicontroller.NjetVsApi, len(olds))
	newMap := make(map[string]*njetapicontroller.NjetVsApi, len(news))
	for _, vs := range olds {
		oldMap[vs.ServerName] = vs
	}
	for _, vs := range news {
		newMap[vs.ServerName] = vs
	}

	for _, nvs := range news {
		if ovs, ok := oldMap[nvs.ServerName]; !ok {
			added = append(added, nvs)
		} else if !reflect.DeepEqual(ovs, nvs) {
			updated = append(updated, nvs)
		}
	}
	for _, ovs := range olds {
		if _, ok := newMap[ovs.ServerName]; !ok {
			deleted = append(deleted, ovs)
		}
	}
	return
}

func DifferentAccessLog(old, new *njetapicontroller.AccessLogData) bool {
	if new == nil || old == new {
		return false
	}
	if old == nil {
		return true
	}

	if !reflect.DeepEqual(old, new) {
		return true
	}
	return false
}

type PushNjetLuaApiResourceContext struct {
	IngressNginxConfig    map[string]*version1.IngressNginxConfig
	VirtualServerConfig   map[string]*version2.VirtualServerConfig
	TransportServerConfig map[string]*version2.TransportServerConfig
}

func NewPushNjetLuaApiResourceContext(ingressNginxConfig map[string]*version1.IngressNginxConfig, virtualServerConfig map[string]*version2.VirtualServerConfig,
	transportServerConfig map[string]*version2.TransportServerConfig) *PushNjetLuaApiResourceContext {
	return &PushNjetLuaApiResourceContext{
		IngressNginxConfig:    ingressNginxConfig,
		VirtualServerConfig:   virtualServerConfig,
		TransportServerConfig: transportServerConfig,
	}
}

type NjetLuaApiContext struct {
	Upstreams          []*njetapicontroller.LuaUpstreamApi
	UpstreamsForStream []*njetapicontroller.LuaUpstreamApi
}
