package model

import adccontroller "github.com/nginxinc/kubernetes-ingress/internal/adc/controller"

type AdcHostApiContextWrap struct {
	Added   map[string]*adccontroller.AdcHostApi
	Deleted map[string]*adccontroller.AdcHostApi
	Updated map[string]*adccontroller.AdcHostApi
}

type AdcSlbPoolContextWrap struct {
	// map key:SlbPool name, map value: SlbPool member address
	Added   map[string][]string
	Deleted map[string][]string
	Updated map[string][]string
}

type AdcVsContextWrap struct {
	// map key:vs name
	Added   map[string]adccontroller.SlbVirtualServerCreateApi
	Deleted map[string]adccontroller.SlbVirtualServerCreateApi
	Updated map[string]adccontroller.SlbVirtualServerCreateApi
}
