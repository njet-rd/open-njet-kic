package controller

import (
	"encoding/json"
	"fmt"
	"github.com/golang/glog"
	adcapihttp "github.com/nginxinc/kubernetes-ingress/internal/adc/controller/http"
	"net/http"
	"strings"
)

const (
	ListSlbPoolUri         = "/adc/v3.0/slb/pool/"
	GetSlbPoolUri          = "/adc/v3.0/slb/pool/%s"
	CreateSlbPoolUri       = "/adc/v3.0/slb/pool/"
	UpdateSlbPoolUri       = "/adc/v3.0/slb/pool/%s"
	DeleteSlbPoolUri       = "/adc/v3.0/slb/pool/%s"
	ListSlbRealServerUri   = "/adc/v3.0/slb/pool/%s/rserver/"
	GetSlbRealServerUri    = "/adc/v3.0/slb/pool/%s/rserver/%s"
	CreateSlbRealServerUri = "/adc/v3.0/slb/pool/%s/rserver/"
	UpdateSlbRealServerUri = "/adc/v3.0/slb/pool/%s/rserver/%s"
	DeleteSlbRealServerUri = "/adc/v3.0/slb/pool/%s/rserver/%s"
)

type SlbPoolApi struct {
	Name                string              `json:"name,omitempty"`
	Healthcheck         []HealthCheckHandle `json:"healthcheck,omitempty"`
	HealthcheckRelation string              `json:"healthcheck_relation,omitempty"`
	ElasticEnable       string              `json:"elastic_enable,omitempty"`
	ElasticLimit        string              `json:"elastic_limit,omitempty"`
	ElasticMinactive    string              `json:"elastic_minactive,omitempty"`
	ElasticTime         string              `json:"elastic_time,omitempty"`
	ElasticEype         string              `json:"elastic_type,omitempty"`
	Method              string              `json:"method,omitempty"`
	SnmpMonitorEnable   string              `json:"snmp_monitor_enable,omitempty"`
	PgEnable            string              `json:"pg_enable,omitempty"`
	PgActivations       string              `json:"pg_activations,omitempty"`
	WarmupEnable        string              `json:"warmup_enable,omitempty"`
	SilentPeriod        string              `json:"silent_period,omitempty"`
	WarmupPeriod        string              `json:"warmup_period,omitempty"`
	WarmupIncrease      string              `json:"warmup_increase,omitempty"`
	Community           string              `json:"community,omitempty"`
	CpuMemWeight        string              `json:"cpu_mem_weight,omitempty"`
	// slb_pool_states is empty, when create slbpool
	SlbPoolStates *SlbPoolStates     `json:"slb_pool_states,omitempty"`
	SlbRserver    []SlbRealServerApi `json:"slb_rserver,omitempty"`
	// slb_pool_states is empty, when create slbpool
	Uuid string `json:"uuid,omitempty"`
}

type SlbPoolStates struct {
	Connections    string `json:"connections"`
	CpuUsage       string `json:"cpu_usage"`
	Flowin         string `json:"flowin"`
	Flowout        string `json:"flowout"`
	MemUsage       string `json:"mem_usage"`
	NewConnections string `json:"new_connections"`
	Requests       string `json:"requests"`
	Uuid           string `json:"uuid"`
}

type SlbPoolListData struct {
	SlbPool []SlbPoolApi `json:"slb_pool"`
}

type HealthCheckHandle struct {
	HealthcheckUuid string `json:"healthcheck_uuid"`
	HealthcheckName string `json:"healthcheck_name"`
}

type SlbRealServerApi struct {
	Address               string              `json:"address,omitempty"`
	AliveState            string              `json:"alive_state,omitempty"`
	Bandwidth             string              `json:"bandwidth,omitempty"`
	Community             string              `json:"community,omitempty"`
	ConnPoolSize          string              `json:"conn_pool_size,omitempty"`
	ElasticEnable         string              `json:"elastic_enable,omitempty"`
	ElasticVirtualmachine string              `json:"elastic_virtualmachine,omitempty"`
	Enable                string              `json:"enable,omitempty"`
	Fqdn                  string              `json:"fqdn,omitempty"`
	Fqdnname              string              `json:"fqdnname,omitempty"`
	HealthcheckRelation   string              `json:"healthcheck_relation,omitempty"`
	Healthcheck           []HealthCheckHandle `json:"healthcheck,omitempty,omitempty"`
	Maxconn               string              `json:"maxconn,omitempty"`
	Maxreq                string              `json:"maxreq,omitempty"`
	OnlineRate            string              `json:"online_rate,omitempty"`
	PgPriority            string              `json:"pg_priority,omitempty"`
	PgState               string              `json:"pg_state,omitempty"`
	SlbNodeName           string              `json:"slb_node_name,omitempty"`
	SlbNodeUuid           string              `json:"slb_node_uuid,omitempty"`
	SlbPoolName           string              `json:"slb_pool_name,omitempty"`
	SlbPoolUuid           string              `json:"slb_pool_uuid,omitempty"`
	SlbRserverStates      *SlbRserverStates   `json:"slb_rserver_states,omitempty"`
	State                 string              `json:"state,omitempty"`
	Uuid                  string              `json:"uuid,omitempty"`
	Weight                string              `json:"weight,omitempty"`
}

type SlbRserverStates struct {
	Connections    string `json:"connections"`
	CpuUsage       string `json:"cpu_usage"`
	Flowin         string `json:"flowin"`
	Flowout        string `json:"flowout"`
	MemUsage       string `json:"mem_usage"`
	NewConnections string `json:"new_connections"`
	Requests       string `json:"requests"`
	Uuid           string `json:"uuid"`
}

type RealServerListData struct {
	SlbRserver []SlbRealServerApi `json:"slb_rserver"`
}

func NewRealServer() *SlbRealServerApi {
	return &SlbRealServerApi{
		Weight:                "10",
		Maxconn:               "0",
		Maxreq:                "0",
		Bandwidth:             "0",
		Healthcheck:           []HealthCheckHandle{},
		HealthcheckRelation:   "all",
		ElasticEnable:         "off",
		ElasticVirtualmachine: "",
		Enable:                "on",
		ConnPoolSize:          "1024",
		PgPriority:            "0",
		SlbRserverStates:      nil,
	}
}

func NewSlbPoolApi() *SlbPoolApi {
	return &SlbPoolApi{
		Name:                "",
		Healthcheck:         []HealthCheckHandle{},
		HealthcheckRelation: "all",
		ElasticEnable:       "off",
		ElasticLimit:        "",
		ElasticMinactive:    "",
		ElasticTime:         "",
		ElasticEype:         "",
		Method:              "rr",
		SnmpMonitorEnable:   "off",
		PgEnable:            "off",
		PgActivations:       "",
		WarmupEnable:        "off",
		SilentPeriod:        "10",
		WarmupPeriod:        "10",
		WarmupIncrease:      "100",
		Community:           "",
		CpuMemWeight:        "",
		SlbPoolStates:       nil,
		SlbRserver:          []SlbRealServerApi{},
		Uuid:                "",
	}

}

func (sp *SlbPoolApi) GetRs() []string {
	var members []string
	for _, rs := range sp.SlbRserver {
		members = append(members, rs.Address)
	}

	return members
}

func newSlbPoolClient(c *AdcApi) *ADCSlbPoolImp {
	return &ADCSlbPoolImp{
		urlPrefix: c.baseURL,
		adcApi:    c,
	}
}

type ADCSlbPoolImp struct {
	urlPrefix string
	adcApi    *AdcApi
}

func (slbpool *ADCSlbPoolImp) UpdateSlbPool(slbpoolName string, members []string) error {
	if slbpoolName == "" {
		glog.Errorf("slbpoolName is empty, UpdateSlbPool failed")
		return fmt.Errorf("slbpoolName is empty, UpdateSlbPool failed")
	}
	err, adcSlbPoolApis := slbpool.ListSlbPool()
	if err != nil {
		return err
	}

	var slbPool SlbPoolApi
	for _, adcSlbPoolApi := range adcSlbPoolApis {
		if adcSlbPoolApi.Name == slbpoolName {
			slbPool = adcSlbPoolApi
			break
		}
	}

	// delete old rs
	for _, rs := range slbPool.SlbRserver {
		slbpool.DeleteRealServer(slbPool.Uuid, rs.Uuid)
	}

	// create new rs
	return slbpool.CreateRealServer(slbPool.Uuid, members)
}

func (slbpool *ADCSlbPoolImp) ListSlbPool() (error, []SlbPoolApi) {
	listSlbPoolUrl := slbpool.urlPrefix + ListSlbPoolUri
	glog.V(3).Infof("ListSlbPoolUrl:%s", listSlbPoolUrl)
	restRequest := &adcapihttp.RestRequest{
		ResourceType: adcapihttp.SlbPoolResource,
		Method:       http.MethodGet,
		RequestURL:   listSlbPoolUrl,
		Retries:      0,
	}

	code, body, err := slbpool.adcApi.SendRequest(restRequest)
	if err == nil {
		var apiBody *adcapihttp.CommonApiBody
		err, apiBody = slbpool.adcApi.HandleResponse(restRequest, code, body)
		if err == nil {
			var slbPoolListData SlbPoolListData
			var bytes []byte
			bytes, err = json.Marshal(apiBody.Data)
			if err != nil {
				return err, nil
			}
			err = json.Unmarshal(bytes, &slbPoolListData)
			if err == nil {
				return nil, slbPoolListData.SlbPool
			} else {
				return err, nil
			}
		} else {
			return err, nil
		}
	}

	glog.Errorf("http request failed:%s", err)
	return fmt.Errorf("http request failed:%s", err), nil

}

func (slbpool *ADCSlbPoolImp) CreateSlbPool(slbpoolName string, members []string) error {
	if slbpoolName == "" {
		glog.Errorf("slbpoolName is empty, don't create slbpool")
		return fmt.Errorf("slbpoolName is empty, don't create slbpool")
	}

	// create slb_pool
	data := NewSlbPoolApi()
	data.Name = slbpoolName
	createSlbPoolUrl := slbpool.urlPrefix + CreateSlbPoolUri
	glog.V(3).Infof("ADC CreateSlbPoolUri:%s", createSlbPoolUrl)
	restRequest := &adcapihttp.RestRequest{
		ResourceType: adcapihttp.SlbPoolResource,
		Method:       http.MethodPost,
		RequestURL:   createSlbPoolUrl,
		Body:         data,
		Retries:      0,
	}

	slbpoolRes := NewSlbPoolApi()
	code, body, err := slbpool.adcApi.SendRequest(restRequest)
	if err == nil {
		var apiBody *adcapihttp.CommonApiBody
		err, apiBody = slbpool.adcApi.HandleResponse(restRequest, code, body)
		if err == nil {
			var bytes []byte
			bytes, err = json.Marshal(apiBody.Data)
			if err != nil {
				return err
			}
			err = json.Unmarshal(bytes, &slbpoolRes)
			if err != nil {
				return err
			}
		} else {
			return err
		}
	} else {
		glog.Errorf("http request failed:%s", err)
		return fmt.Errorf("http request failed:%s", err)
	}

	// create slb_pool members
	return slbpool.CreateRealServer(slbpoolRes.Uuid, members)
}

func (slbpool *ADCSlbPoolImp) DeleteSlbPool(slbpoolName string) error {
	if slbpoolName == "" {
		glog.Errorf("slbpoolName is empty, don't delete slbpool")
		return fmt.Errorf("slbpoolName is empty, don't delete slbpool")
	}

	err, adcSlbPoolApis := slbpool.ListSlbPool()
	if err != nil {
		return err
	}

	var uuid string
	for _, adcSlbPoolApi := range adcSlbPoolApis {
		if adcSlbPoolApi.Name == slbpoolName {
			uuid = adcSlbPoolApi.Uuid
			break
		}
	}
	if uuid == "" {
		return fmt.Errorf("SlbPool \"%s\" does not exist in ADC, delete failed", slbpoolName)
	}

	deleteSlbPoolUrl := slbpool.urlPrefix + fmt.Sprintf(DeleteSlbPoolUri, uuid)
	glog.V(3).Infof("ADC DeleteSlbPoolUrl:%s", deleteSlbPoolUrl)
	restRequest := &adcapihttp.RestRequest{
		ResourceType: adcapihttp.SlbPoolResource,
		Method:       http.MethodDelete,
		RequestURL:   deleteSlbPoolUrl,
		Retries:      0,
	}

	code, body, err := slbpool.adcApi.SendRequest(restRequest)
	if err == nil {
		err, _ = slbpool.adcApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func (slbpool *ADCSlbPoolImp) CreateRealServer(slbPoolUuid string, members []string) error {
	if len(members) == 0 || slbPoolUuid == "" {
		glog.Errorf("slbpool members is empty or slbPoolUuid is empty, don't create rs")
		return fmt.Errorf("slbpool members is empty or slbPoolUuid is empty, don't create rs")
	}

	// create slb_pool members
	var errs []string
	for _, address := range members {
		data := NewRealServer()
		data.Address = address
		createSlbRealServerUrl := slbpool.urlPrefix + fmt.Sprintf(CreateSlbRealServerUri, slbPoolUuid)
		glog.V(3).Infof("ADC CreateSlbRealServerUri:%s", createSlbRealServerUrl)
		restRequest := &adcapihttp.RestRequest{
			ResourceType: adcapihttp.SlbPoolRServerResource,
			Method:       http.MethodPost,
			RequestURL:   createSlbRealServerUrl,
			Body:         data,
			Retries:      0,
		}

		code, body, err := slbpool.adcApi.SendRequest(restRequest)
		if err == nil {
			err, _ = slbpool.adcApi.HandleResponse(restRequest, code, body)
		} else {
			glog.Errorf("http request failed:%s", err)
			err = fmt.Errorf("http request failed:%s", err)
		}

		if err != nil {
			errs = append(errs, fmt.Sprintf("%v", err))
		}
	}

	if len(errs) > 0 {
		return fmt.Errorf("%s", strings.Join(errs, ";"))
	}

	return nil
}

func (slbpool *ADCSlbPoolImp) DeleteRealServer(slbPoolUuid string, rsUuid string) error {
	if rsUuid == "" || slbPoolUuid == "" {
		glog.Errorf("rsUuid is empty or slbPoolUuid is empty, don't delete rs")
		return fmt.Errorf("rsUuid is empty or slbPoolUuid is empty, don't delete rs")
	}

	// delete slb_pool members
	deleteSlbRealServerUrl := slbpool.urlPrefix + fmt.Sprintf(DeleteSlbRealServerUri, slbPoolUuid, rsUuid)
	glog.V(3).Infof("ADC DeleteSlbRealServerUri:%s", deleteSlbRealServerUrl)
	restRequest := &adcapihttp.RestRequest{
		ResourceType: adcapihttp.SlbPoolRServerResource,
		Method:       http.MethodDelete,
		RequestURL:   deleteSlbRealServerUrl,
		Retries:      0,
	}

	code, body, err := slbpool.adcApi.SendRequest(restRequest)
	if err == nil {
		err, _ = slbpool.adcApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
		err = fmt.Errorf("http request failed:%s", err)
	}

	return err
}
