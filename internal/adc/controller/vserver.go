package controller

import (
	"encoding/json"
	"fmt"
	"github.com/golang/glog"
	adcapihttp "github.com/nginxinc/kubernetes-ingress/internal/adc/controller/http"
	"net/http"
)

const (
	ListVirtualServerUri   = "/adc/v3.0/slb/vserver"
	GetVirtualServerUri    = "/adc/v3.0/slb/vserver/%s"
	CreateVirtualServerUri = "/adc/v3.0/slb/vserver"
	UpdateVirtualServerUri = "/adc/v3.0/slb/vserver/%s"
	DeleteVirtualServerUri = "/adc/v3.0/slb/vserver/%s"
)

/*
{
"ip_type":"ipv4",
"protocol":"fast-tcp",
"mode":"nat",
"enable":"on",
"ssl_profile_uuid":"",
"web_security_profile_uuid":"",
"persistence_profile_uuid":"",
"fallback_persistence_profile_uuid":"",
"fallback_persistence_profile_name":"",
"slb_pool_uuid":"e2cc0349-9718-4893-804d-3e61bcbd1672",
"slb_backup_pool_uuid":"",
"dynamic_routing":"",
"tcp_udp_profile_uuid":"7f22b6f2-0829-42a1-840e-cfa69478cbe9",
"nat_name":["default"],
"name":"kic-test-vs",
"vs_desc":"",
"vlan_traffic_type":"0",
"vlans":[],
"vip":"192.168.40.186:7777"}
*/
type SlbVirtualServerCreateApi struct {
	AclProfileName                 string `json:"acl_profile_name,omitempty"`
	AclProfileUuid                 string `json:"acl_profile_uuid,omitempty"`
	AliveState                     string `json:"alive_state,omitempty"`
	ClientIpInsert                 string `json:"client_ip_insert,omitempty"`
	DynamicRouting                 string `json:"dynamic_routing,omitempty"`
	Enable                         string `json:"enable,omitempty"`
	FallbackPersistenceProfileName string `json:"fallback_persistence_profile_name,omitempty"`
	FallbackPersistenceProfileUuid string `json:"fallback_persistence_profile_uuid,omitempty"`
	HttpProfileName                string `json:"http_profile_name,omitempty"`
	HttpProfileUuid                string `json:"http_profile_uuid,omitempty"`
	IpType                         string `json:"ip_type,omitempty"`
	MblbProfileName                string `json:"mblb_profile_name,omitempty"`
	MblbProfileUuid                string `json:"mblb_profile_uuid,omitempty"`
	Mode                           string `json:"mode,omitempty"`
	Name                           string `json:"name,omitempty"`
	// The type of the create and list API is different
	NatName                []string          `json:"nat_name,omitempty"`
	OnlineRate             string            `json:"online_rate,omitempty"`
	PersistenceProfileName string            `json:"persistence_profile_name,omitempty"`
	PersistenceProfileUuid string            `json:"persistence_profile_uuid,omitempty"`
	Protocol               string            `json:"protocol,omitempty"`
	SlbBackupPoolName      string            `json:"slb_backup_pool_name,omitempty"`
	SlbBackupPoolUuid      string            `json:"slb_backup_pool_uuid,omitempty"`
	SlbPool                *SlbPoolApi       `json:"slb_pool,omitempty"`
	SlbPoolName            string            `json:"slb_pool_name,omitempty"`
	SlbPoolUuid            string            `json:"slb_pool_uuid,omitempty"`
	SlbVserverStates       *SlbVserverStates `json:"slb_vserver_states,omitempty"`
	SmoothEnable           string            `json:"smooth_enable,omitempty"`
	SmoothReqs             string            `json:"smooth_reqs,omitempty"`
	SmoothStart            string            `json:"smooth_start,omitempty"`
	SmoothTime             string            `json:"smooth_time,omitempty"`
	SslProfileName         string            `json:"ssl_profile_name,omitempty"`
	SslProfileUuid         string            `json:"ssl_profile_uuid,omitempty"`
	TcpUdpProfileName      string            `json:"tcp_udp_profile_name,omitempty"`
	TcpUdpProfileUuid      string            `json:"tcp_udp_profile_uuid,omitempty"`
	Uuid                   string            `json:"uuid,omitempty"`
	Vip                    string            `json:"vip"`
	//Vip                    string            `json:"vip,omitempty"`
	VlanTrafficType        string `json:"vlan_traffic_type,omitempty"`
	VsDesc                 string `json:"vs_desc,omitempty"`
	WebSecurityProfileName string `json:"web_security_profile_name,omitempty"`
	WebSecurityProfileUuid string `json:"web_security_profile_uuid,omitempty"`
}

type NatName struct {
	NatName string `json:"nat_name"`
	NatUuid string `json:"nat_uuid"`
}

type SlbVirtualServerApi struct {
	AclProfileName                 string `json:"acl_profile_name,omitempty"`
	AclProfileUuid                 string `json:"acl_profile_uuid,omitempty"`
	AliveState                     string `json:"alive_state,omitempty"`
	ClientIpInsert                 string `json:"client_ip_insert,omitempty"`
	DynamicRouting                 string `json:"dynamic_routing,omitempty"`
	Enable                         string `json:"enable,omitempty"`
	FallbackPersistenceProfileName string `json:"fallback_persistence_profile_name,omitempty"`
	FallbackPersistenceProfileUuid string `json:"fallback_persistence_profile_uuid,omitempty"`
	HttpProfileName                string `json:"http_profile_name,omitempty"`
	HttpProfileUuid                string `json:"http_profile_uuid,omitempty"`
	IpType                         string `json:"ip_type,omitempty"`
	MblbProfileName                string `json:"mblb_profile_name,omitempty"`
	MblbProfileUuid                string `json:"mblb_profile_uuid,omitempty"`
	Mode                           string `json:"mode,omitempty"`
	Name                           string `json:"name,omitempty"`
	// The type of the create and list API is different
	NatName                []NatName         `json:"nat_name,omitempty"`
	OnlineRate             string            `json:"online_rate,omitempty"`
	PersistenceProfileName string            `json:"persistence_profile_name,omitempty"`
	PersistenceProfileUuid string            `json:"persistence_profile_uuid,omitempty"`
	Protocol               string            `json:"protocol,omitempty"`
	SlbBackupPoolName      string            `json:"slb_backup_pool_name,omitempty"`
	SlbBackupPoolUuid      string            `json:"slb_backup_pool_uuid,omitempty"`
	SlbPool                *SlbPoolApi       `json:"slb_pool,omitempty"`
	SlbPoolName            string            `json:"slb_pool_name,omitempty"`
	SlbPoolUuid            string            `json:"slb_pool_uuid,omitempty"`
	SlbVserverStates       *SlbVserverStates `json:"slb_vserver_states,omitempty"`
	SmoothEnable           string            `json:"smooth_enable,omitempty"`
	SmoothReqs             string            `json:"smooth_reqs,omitempty"`
	SmoothStart            string            `json:"smooth_start,omitempty"`
	SmoothTime             string            `json:"smooth_time,omitempty"`
	SslProfileName         string            `json:"ssl_profile_name,omitempty"`
	SslProfileUuid         string            `json:"ssl_profile_uuid,omitempty"`
	TcpUdpProfileName      string            `json:"tcp_udp_profile_name,omitempty"`
	TcpUdpProfileUuid      string            `json:"tcp_udp_profile_uuid,omitempty"`
	Uuid                   string            `json:"uuid,omitempty"`
	Vip                    string            `json:"vip"`
	//Vip                    string            `json:"vip,omitempty"`
	VlanTrafficType        string `json:"vlan_traffic_type,omitempty"`
	VsDesc                 string `json:"vs_desc,omitempty"`
	WebSecurityProfileName string `json:"web_security_profile_name,omitempty"`
	WebSecurityProfileUuid string `json:"web_security_profile_uuid,omitempty"`
}

type SlbVserverStates struct {
	AllConnections    string `json:"all_connections,omitempty"`
	BytesIn           string `json:"bytes_in,omitempty"`
	BytesOut          string `json:"bytes_out,omitempty"`
	CacheHit          string `json:"cache_hit,omitempty"`
	CacheLookup       string `json:"cache_lookup,omitempty"`
	CacheNum          string `json:"cache_num,omitempty"`
	CompressRate      string `json:"compress_rate,omitempty"`
	Connections       string `json:"connections,omitempty"`
	CprAppExcel       string `json:"cpr_app_excel,omitempty"`
	CprAppJson        string `json:"cpr_app_json,omitempty"`
	CprAppMsword      string `json:"cpr_app_msword,omitempty"`
	CprAppPpt         string `json:"cpr_app_ppt,omitempty"`
	CprAppXjs         string `json:"cpr_app_xjs,omitempty"`
	CprAppXml         string `json:"cpr_app_xml,omitempty"`
	CprAppXmlrss      string `json:"cpr_app_xmlrss,omitempty"`
	CprTextCss        string `json:"cpr_text_css,omitempty"`
	CprTextHtml       string `json:"cpr_text_html,omitempty"`
	CprTextJs         string `json:"cpr_text_js,omitempty"`
	CprTextPlain      string `json:"cpr_text_plain,omitempty"`
	CprTextRichtext   string `json:"cpr_text_richtext,omitempty"`
	CprTextXml        string `json:"cpr_text_xml,omitempty"`
	Flowin            string `json:"flowin,omitempty"`
	Flowout           string `json:"flowout,omitempty"`
	NewConnections    string `json:"new_connections,omitempty"`
	Requests          string `json:"requests,omitempty"`
	SlbVserverUuid    string `json:"slb_vserver_uuid,omitempty"`
	UncprAppExcel     string `json:"uncpr_app_excel,omitempty"`
	UncprAppJson      string `json:"uncpr_app_json,omitempty"`
	UncprAppMsword    string `json:"uncpr_app_msword,omitempty"`
	UncprAppPpt       string `json:"uncpr_app_ppt,omitempty"`
	UncprAppXjs       string `json:"uncpr_app_xjs,omitempty"`
	UncprAppXml       string `json:"uncpr_app_xml,omitempty"`
	UncprAppXmlrss    string `json:"uncpr_app_xmlrss,omitempty"`
	UncprTextCss      string `json:"uncpr_text_css,omitempty"`
	UncprTextHtml     string `json:"uncpr_text_html,omitempty"`
	UncprTextJs       string `json:"uncpr_text_js,omitempty"`
	UncprTextPlain    string `json:"uncpr_text_plain,omitempty"`
	UncprTextRichtext string `json:"uncpr_text_richtext,omitempty"`
	UncprTextXml      string `json:"uncpr_text_xml,omitempty"`
	Uuid              string `json:"uuid,omitempty"`
}

type SlbVirtualServerData struct {
	SlbVs []SlbVirtualServerApi `json:"slb_vserver"`
}

/*
{
"ip_type":"ipv4",
"protocol":"fast-tcp",
"mode":"nat",
"enable":"on",
"ssl_profile_uuid":"",
"web_security_profile_uuid":"",
"persistence_profile_uuid":"",
"fallback_persistence_profile_uuid":"",
"fallback_persistence_profile_name":"",
"slb_pool_uuid":"e2cc0349-9718-4893-804d-3e61bcbd1672",
"slb_backup_pool_uuid":"",
"dynamic_routing":"",
"tcp_udp_profile_uuid":"7f22b6f2-0829-42a1-840e-cfa69478cbe9",
"nat_name":["default"],
"name":"kic-test-vs",
"vs_desc":"",
"vlan_traffic_type":"0",
"vlans":[],
"vip":"192.168.40.186:7777"}
*/
func NewSlbVirtualServer() *SlbVirtualServerCreateApi {
	return &SlbVirtualServerCreateApi{
		IpType:                 "ipv4",
		Protocol:               "fast-tcp",
		Mode:                   "nat",
		NatName:                []string{"default"},
		Enable:                 "on",
		SslProfileUuid:         "",
		WebSecurityProfileUuid: "",
		PersistenceProfileUuid: "",
		SlbBackupPoolUuid:      "",
		DynamicRouting:         "",
		VsDesc:                 "",
		VlanTrafficType:        "0",
		Vip:                    "",
	}
}

func NewSlbVirtualServerData() *SlbVirtualServerApi {
	return &SlbVirtualServerApi{
		IpType:                 "ipv4",
		Protocol:               "fast-tcp",
		Mode:                   "nat",
		Enable:                 "on",
		SslProfileUuid:         "",
		WebSecurityProfileUuid: "",
		PersistenceProfileUuid: "",
		SlbBackupPoolUuid:      "",
		DynamicRouting:         "",
		VsDesc:                 "",
		VlanTrafficType:        "0",
		Vip:                    "",
	}
}

func newSlbVsClient(c *AdcApi) *ADCSlbVsImp {
	return &ADCSlbVsImp{
		urlPrefix: c.baseURL,
		adcApi:    c,
	}
}

type ADCSlbVsImp struct {
	urlPrefix string
	adcApi    *AdcApi
}

func (slbvs *ADCSlbVsImp) CreateSlbVs(vs SlbVirtualServerCreateApi) error {
	if vs.Name == "" || vs.SlbPoolUuid == "" || vs.TcpUdpProfileUuid == "" || vs.Vip == "" {
		glog.Errorf("vs.Name or vs.Vip or vs.SlbPoolUuid or vs.TcpUdpProfileUuid is empty, don't create slbvs")
		return fmt.Errorf("vs.Name or vs.Vip or vs.SlbPoolUuid or vs.TcpUdpProfileUuid is empty, don't create slbvs")
	}

	// create slbvs
	createSlbVsUrl := slbvs.urlPrefix + CreateVirtualServerUri
	glog.V(3).Infof("ADC CreateVirtualServerUrl:%s", createSlbVsUrl)
	vs.Uuid = ""
	restRequest := &adcapihttp.RestRequest{
		ResourceType: adcapihttp.SlbVsResource,
		Method:       http.MethodPost,
		RequestURL:   createSlbVsUrl,
		Body:         vs,
		Retries:      0,
	}

	code, body, err := slbvs.adcApi.SendRequest(restRequest)
	if err == nil {
		err, _ = slbvs.adcApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func (slbvs *ADCSlbVsImp) ListSlbVs() (error, []SlbVirtualServerApi) {
	listSlbVsUrl := slbvs.urlPrefix + ListVirtualServerUri
	glog.V(3).Infof("ListVirtualServerUrl:%s", listSlbVsUrl)
	restRequest := &adcapihttp.RestRequest{
		ResourceType: adcapihttp.SlbVsResource,
		Method:       http.MethodGet,
		RequestURL:   listSlbVsUrl,
		Retries:      0,
	}

	code, body, err := slbvs.adcApi.SendRequest(restRequest)
	if err == nil {
		var apiBody *adcapihttp.CommonApiBody
		err, apiBody = slbvs.adcApi.HandleResponse(restRequest, code, body)
		if err == nil {
			var slbVirtualServerData SlbVirtualServerData
			var bytes []byte
			bytes, err = json.Marshal(apiBody.Data)
			if err != nil {
				return err, nil
			}
			err = json.Unmarshal(bytes, &slbVirtualServerData)
			if err == nil {
				return nil, slbVirtualServerData.SlbVs
			} else {
				return err, nil
			}
		} else {
			return err, nil
		}
	}

	glog.Errorf("http request failed:%s", err)
	return fmt.Errorf("http request failed:%s", err), nil

}

func (slbvs *ADCSlbVsImp) UpdateSlbVs(vs SlbVirtualServerCreateApi) error {
	if vs.Uuid == "" {
		glog.Errorf("vs.Uuid  is empty, don't update slbvs")
		return fmt.Errorf("vs.Uuid  is empty, don't update slbvs")
	}

	// update slbvs
	updateSlbVsUrl := slbvs.urlPrefix + fmt.Sprintf(UpdateVirtualServerUri, vs.Uuid)
	glog.V(3).Infof("ADC UpdateVirtualServerUrl:%s", updateSlbVsUrl)
	// Otherwise, the adc reports an error
	vs.Uuid = ""
	vs.Name = ""
	restRequest := &adcapihttp.RestRequest{
		ResourceType: adcapihttp.SlbVsResource,
		Method:       http.MethodPut,
		RequestURL:   updateSlbVsUrl,
		Body:         vs,
		Retries:      0,
	}

	code, body, err := slbvs.adcApi.SendRequest(restRequest)
	if err == nil {
		err, _ = slbvs.adcApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func (slbvs *ADCSlbVsImp) DeleteSlbVs(vs SlbVirtualServerCreateApi) error {
	if vs.Uuid == "" {
		glog.Errorf("vs.Uuid  is empty, don't update slbvs")
		return fmt.Errorf("vs.Uuid  is empty, don't update slbvs")
	}

	// delete slbvs
	deleteSlbVsUrl := slbvs.urlPrefix + fmt.Sprintf(DeleteVirtualServerUri, vs.Uuid)
	glog.V(3).Infof("ADC DeleteVirtualServerUrl:%s", deleteSlbVsUrl)
	// Otherwise, the adc reports an error
	restRequest := &adcapihttp.RestRequest{
		ResourceType: adcapihttp.SlbVsResource,
		Method:       http.MethodDelete,
		RequestURL:   deleteSlbVsUrl,
		Retries:      0,
	}

	code, body, err := slbvs.adcApi.SendRequest(restRequest)
	if err == nil {
		err, _ = slbvs.adcApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}
