package http

type CommonApiResponse struct {
	Code   string `json:"code"`
	Status string `json:"status"`
	Msg    string `json:"msg"`
}

type CommonCreateData struct {
	Uuid string `json:"uuid"`
}

type CommonApiBody struct {
	Res  CommonApiResponse `json:"res"`
	Data interface{}       `json:"data"`
}
