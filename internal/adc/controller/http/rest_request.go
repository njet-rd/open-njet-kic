package http

import "net/http"

const (
	HostResource           = "host"
	SlbPoolResource        = "slbpool"
	SlbPoolRServerResource = "slbpoolrs"
	SlbVsResource          = "slbvs"
	LoginResource          = "login"
)

type RestRequest struct {
	Method       string
	RequestURL   string
	ResourceType string
	Headers      *map[string]string
	Body         interface{}

	Retries int
}

type ClientImpl struct {
	Httpclient *http.Client
	Headers    map[string]interface{}
}
