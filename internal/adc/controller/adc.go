package controller

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/golang/glog"
	"github.com/nginxinc/kubernetes-ingress/bootstrap"
	adcapihttp "github.com/nginxinc/kubernetes-ingress/internal/adc/controller/http"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"
)

const AdcBaseURL = "http://127.0.0.1:12001"

type AdcApi struct {
	httpclient     *adcapihttp.ClientImpl
	baseURL        string
	PassWord       string
	UserName       string
	mugslbPoolUUID sync.Mutex
	gslbPoolUUID   string
	domainName     *DomainNameImp
	adcLogin       *ADCLoginImp
	slbPoolImp     *ADCSlbPoolImp
	slbVsImp       *ADCSlbVsImp

	//Protect the Token
	mu    sync.Mutex
	Token string
}

// DomainName return DomainName client
func (c *AdcApi) DomainName() *DomainNameImp {
	return c.domainName
}

func (c *AdcApi) ADCLogin() *ADCLoginImp {
	return c.adcLogin
}

func (c *AdcApi) ADCSlbPool() *ADCSlbPoolImp {
	return c.slbPoolImp
}

func (c *AdcApi) ADCSlbVs() *ADCSlbVsImp {
	return c.slbVsImp
}

func NewAdcApi(c *bootstrap.AdcConfig) *AdcApi {
	adcApi := &AdcApi{
		httpclient: &adcapihttp.ClientImpl{
			Httpclient: &http.Client{
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: true,
					},
				},
				Timeout: 10 * time.Second,
			},
			Headers: map[string]interface{}{
				"Content-Type": "application/json",
			},
		},
		UserName:     c.AdcUsername,
		PassWord:     c.AdcPassword,
		baseURL:      c.ServerURI,
		gslbPoolUUID: c.AdcGslbPoolUuid,
	}

	adcApi.domainName = newDomainNameClient(adcApi)
	adcApi.adcLogin = newADCLoginImpClient(adcApi)
	adcApi.slbPoolImp = newSlbPoolClient(adcApi)
	adcApi.slbVsImp = newSlbVsClient(adcApi)

	return adcApi
}

func (c *AdcApi) SendRequest(data *adcapihttp.RestRequest) (int, []byte, error) {
	if data == nil {
		return 0, nil, fmt.Errorf("ADC RestfulRequest is nil")
	}
	glog.V(2).Infof("ADC HTTP Request: ADC resource type: %q, method: %s, url:%s,  http request payload:%+v", data.ResourceType, data.Method, data.RequestURL, data.Body)
	var pd io.Reader = nil
	client := c.httpclient.Httpclient
	if data.Body != nil {
		if data.Headers != nil {
			contentType, ok := (*data.Headers)["content-type"]
			if ok && strings.Compare(contentType, "application/json") != 0 {

				if v, ok := data.Body.([]byte); ok {
					glog.V(2).Infof("ADC content-type:%q, http client request body:%s", contentType, string(v))
					pd = bytes.NewReader(data.Body.([]byte))
				} else {
					(*data.Headers)["content-type"] = "application/json"
				}
			}
		}
		if pd == nil {
			payload, err := json.Marshal(data.Body)
			if err != nil {
				glog.Errorf("ADC Http client send request err, json.Marshal failed:%s", err)
				return 0, nil, err
			}
			glog.V(2).Infof("ADC http client request body:%s", string(payload))
			pd = strings.NewReader(string(payload))
		}

	}

	method := data.Method
	url := data.RequestURL
	req, err := http.NewRequest(method, url, pd)
	if err != nil {
		return 0, nil, err
	}
	headers := map[string]string{}
	for hk, hv := range c.httpclient.Headers {
		headers[hk] = fmt.Sprintf("%v", hv)
	}
	for k, v := range headers {
		req.Header.Add(k, v)
	}
	if data.Headers != nil {
		for k, v := range *data.Headers {
			req.Header.Set(k, v)
		}
	}

	token := c.GetToken()
	req.Header.Add("X-Access-token", token)
	glog.V(2).Infof("ADC http client request headers:%v", req.Header)
	res, err := client.Do(req)
	if err != nil {
		return 0, nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return res.StatusCode, nil, err
	}
	return res.StatusCode, body, nil
}

func (c *AdcApi) HandleResponse(restRequest *adcapihttp.RestRequest, statusCode int, body []byte) (error, *adcapihttp.CommonApiBody) {
	if restRequest != nil {
		glog.V(2).Infof("ADC HTTP Response: ADC resource type: %q, method: %s, url:%s, http request payload:%+v, http code :%v , http response:%s", restRequest.ResourceType, restRequest.Method, restRequest.RequestURL, restRequest.Body,
			statusCode, string(body))
	}

	var err error
	if statusCode == 200 {
		apiBody := new(adcapihttp.CommonApiBody)

		err = json.Unmarshal(body, apiBody)
		if err != nil {
			glog.Errorf("ADC Unmarshal AdcApiBody error:%s, http status code is %d, and body is %q", err, statusCode, string(body))
			return fmt.Errorf("ADC Unmarshal AdcApiBody error, http status code is %d, and body is %q", statusCode, string(body)), nil
		}
		if apiBody.Res.Status == "" || strings.Compare(apiBody.Res.Status, "success") != 0 {
			glog.Errorf("ADC status:%s, msg:%s", apiBody.Res.Status, apiBody.Res.Msg)
			return fmt.Errorf("ADC status:%s, msg:%s", apiBody.Res.Status, apiBody.Res.Msg), nil
		}

		return nil, apiBody
	} else {
		glog.Errorf("ADC http status code is %d, and body is %q", statusCode, string(body))
		return fmt.Errorf("ADC http status code is %d, and body is %q", statusCode, string(body)), nil
	}
}

func (c *AdcApi) UpdateToken(token string) {
	glog.V(2).Infof("ADC http client update token:%s", token)
	c.mu.Lock()
	defer c.mu.Unlock()
	c.Token = token
}

func (c *AdcApi) GetToken() string {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.Token
}

func (c *AdcApi) UpdateGslbPoolUuid(uuid string) {
	c.mugslbPoolUUID.Lock()
	defer c.mugslbPoolUUID.Unlock()
	c.gslbPoolUUID = uuid
}

func (c *AdcApi) GetGslbPoolUuid() string {
	c.mugslbPoolUUID.Lock()
	defer c.mugslbPoolUUID.Unlock()
	return c.gslbPoolUUID
}
