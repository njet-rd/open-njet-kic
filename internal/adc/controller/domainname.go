package controller

import (
	"encoding/json"
	"fmt"
	"github.com/golang/glog"
	adcapihttp "github.com/nginxinc/kubernetes-ingress/internal/adc/controller/http"
	"net/http"
)

const (
	CreateOrListHostUri = "/adc/v3.0/gslb/vservers"
	DeleteHostUri       = "/adc/v3.0/gslb/vservers/%s"
	UpdateHostUri       = "/adc/v3.0/gslb/vservers/%s"
)

//https://192.168.40.181:10443/adc/v3.0/gslb/vservers POST
//enable:"on"
//gslb_pool:"cbeed8c1-6dcd-4fdd-a659-ca742957f0c9"
//name:"test.hello.com"
//persistence:""
//qps:0
//timeout:900
//ttl:10

/*
{
"data": {
"uuid": "d855e8ed-42a2-4cd7-b149-ea68a4541f35"
},
"res": {
"code": "1104-0000",
"msg": "success",
"status": "success"
}
}
*/

//delete
//https://192.168.40.181:10443/adc/v3.0/gslb/vservers/d855e8ed-42a2-4cd7-b149-ea68a4541f35 DELETE
/*
{
   "res" : {
      "code" : "1104-0000",
      "msg" : "success",
      "status" : "success"
   }
}
*/

type AdcHostApi struct {
	Enable      string `json:"enable,omitempty"`
	Gslbpool    string `json:"gslb_pool"`
	Name        string `json:"name"`
	Persistence string `json:"persistence,omitempty"`
	Qps         int    `json:"qps"`
	Timeout     string `json:"timeout"`
	Ttl         int    `json:"ttl"`
	Uuid        string `json:"uuid,omitempty"`
}

type AdcHostUpdateApi struct {
	Enable   string `json:"enable,omitempty"`
	Gslbpool string `json:"gslb_pool"`
}

func newDomainNameClient(c *AdcApi) *DomainNameImp {
	return &DomainNameImp{
		urlPrefix: c.baseURL,
		adcApi:    c,
	}
}

type DomainNameImp struct {
	urlPrefix string
	adcApi    *AdcApi
}

func (dn *DomainNameImp) Create(host string) error {
	if host == "" {
		fmt.Errorf("host is empty")
	}

	data := AdcHostApi{
		Enable:      "on",
		Gslbpool:    dn.adcApi.gslbPoolUUID,
		Name:        host,
		Persistence: "",
		Qps:         0,
		Timeout:     "900",
		Ttl:         10,
	}
	createHostUrl := dn.urlPrefix + CreateOrListHostUri
	glog.V(3).Infof("ADC CreateHostUrl:%s", createHostUrl)
	restRequest := &adcapihttp.RestRequest{
		ResourceType: adcapihttp.HostResource,
		Method:       http.MethodPost,
		RequestURL:   createHostUrl,
		Body:         data,
		Retries:      0,
	}

	code, body, err := dn.adcApi.SendRequest(restRequest)
	if err == nil {
		err, _ = dn.adcApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func (dn *DomainNameImp) List() (error, []AdcHostApi) {
	listHostUrl := dn.urlPrefix + CreateOrListHostUri
	glog.V(3).Infof("ListHostUrl:%s", listHostUrl)
	restRequest := &adcapihttp.RestRequest{
		ResourceType: adcapihttp.HostResource,
		Method:       http.MethodGet,
		RequestURL:   listHostUrl,
		Retries:      0,
	}

	code, body, err := dn.adcApi.SendRequest(restRequest)
	if err == nil {
		var apiBody *adcapihttp.CommonApiBody
		err, apiBody = dn.adcApi.HandleResponse(restRequest, code, body)
		if err == nil {
			var adcHostApis map[string][]AdcHostApi
			var bytes []byte
			bytes, err = json.Marshal(apiBody.Data)
			if err != nil {
				return err, nil
			}
			err = json.Unmarshal(bytes, &adcHostApis)
			if err == nil {
				return nil, adcHostApis["vservers"]
			} else {
				return err, nil
			}
		} else {
			return err, nil
		}
	}

	glog.Errorf("http request failed:%s", err)
	return fmt.Errorf("http request failed:%s", err), nil

}

func (dn *DomainNameImp) Delete(host string) error {
	if host == "" {
		fmt.Errorf("host is empty")
	}

	err, adcHostApis := dn.List()
	if err != nil {
		return err
	}

	var uuid string
	for _, adcHostApi := range adcHostApis {
		if adcHostApi.Name == host {
			uuid = adcHostApi.Uuid
			break
		}
	}
	if uuid == "" {
		return fmt.Errorf("Host \"%s\" does not exist in ADC, delete failed.", host)
	}

	deleteHostUrl := dn.urlPrefix + fmt.Sprintf(DeleteHostUri, uuid)
	glog.V(3).Infof("ADC DeleteHostUrl:%s", deleteHostUrl)
	restRequest := &adcapihttp.RestRequest{
		ResourceType: adcapihttp.HostResource,
		Method:       http.MethodDelete,
		RequestURL:   deleteHostUrl,
		Retries:      0,
	}

	code, body, err := dn.adcApi.SendRequest(restRequest)
	if err == nil {
		err, _ = dn.adcApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}

func (dn *DomainNameImp) Update(hostApi *AdcHostApi) error {
	if hostApi == nil {
		fmt.Errorf("AdcHostApi is nil")
	}

	data := AdcHostUpdateApi{
		Enable:   "on",
		Gslbpool: hostApi.Gslbpool,
	}

	updateHostUrl := dn.urlPrefix + fmt.Sprintf(UpdateHostUri, hostApi.Uuid)
	glog.V(3).Infof("ADC UpdateHostUrl:%s", updateHostUrl)
	restRequest := &adcapihttp.RestRequest{
		ResourceType: adcapihttp.HostResource,
		Method:       http.MethodPut,
		RequestURL:   updateHostUrl,
		Body:         data,
		Retries:      0,
	}

	code, body, err := dn.adcApi.SendRequest(restRequest)
	if err == nil {
		err, _ = dn.adcApi.HandleResponse(restRequest, code, body)
	} else {
		glog.Errorf("http request failed:%s", err)
	}

	return err
}
