package controller

import (
	"encoding/json"
	"fmt"
	"github.com/golang/glog"
	adcapihttp "github.com/nginxinc/kubernetes-ingress/internal/adc/controller/http"
	"net/http"
)

const (
	LoginUri = "/adc/v3.0/login"
)

type Login struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginRes struct {
	Data ResData   `json:"data"`
	Res  ResStatus `json:"res"`
}

type ResData struct {
	Token    string `json:"token"`
	UserUuid string `json:"user_uuid"`
}

type ResStatus struct {
	Code   string `json:"code"`
	Msg    string `json:"msg"`
	Status string `json:"status"`
}

type ADCLoginImp struct {
	urlPrefix string
	adcApi    *AdcApi
}

func newADCLoginImpClient(c *AdcApi) *ADCLoginImp {
	return &ADCLoginImp{
		urlPrefix: c.baseURL,
		adcApi:    c,
	}
}

func (l *ADCLoginImp) ADCLogin() (string, error) {
	glog.V(2).Infof("login:username:%s,password:%s", l.adcApi.UserName, l.adcApi.PassWord)
	if l.adcApi.UserName == "" || l.adcApi.PassWord == "" {
		return "", fmt.Errorf("adc-username or adc-password is null")
	}

	login := Login{
		Username: l.adcApi.UserName,
		Password: l.adcApi.PassWord,
	}
	req := &adcapihttp.RestRequest{
		Method:       http.MethodPost,
		RequestURL:   l.urlPrefix + LoginUri,
		ResourceType: adcapihttp.LoginResource,
		Body:         login,
		Retries:      0,
	}

	code, body, err := l.adcApi.SendRequest(req)
	if err != nil {
		return "", err
	}

	var loginRes LoginRes
	if code == 200 {
		err := json.Unmarshal(body, &loginRes)
		if err != nil {
			return "", err
		}

		glog.V(2).Infof("ADCLogin res: %s, token:%s", string(body), loginRes.Data.Token)
		// failed
		// "res": {
		// 		"code": "1115-0200",
		// 		"msg": "无效的用户名或密码",
		// 		"status": "failure"
		// }
		// success
		// "res": {
		//		"code": "1115-0000",
		//		"msg": "success",
		//		"status": "success"
		// }

		if loginRes.Res.Code == "1115-0000" {
			return loginRes.Data.Token, nil
		} else {
			return loginRes.Data.Token, fmt.Errorf("%s", loginRes.Res.Msg)
		}
	} else {
		glog.Errorf("ADCLogin failed:http code:%s", code)
		return "", fmt.Errorf("ADCLogin failed:http code:%s", code)
	}

}
