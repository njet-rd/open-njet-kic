package adc

import (
	"fmt"
	"github.com/golang/glog"
	"github.com/hashicorp/go-multierror"
	"github.com/nginxinc/kubernetes-ingress/bootstrap"
	adcapicontroller "github.com/nginxinc/kubernetes-ingress/internal/adc/controller"
	"github.com/nginxinc/kubernetes-ingress/internal/model"
	"k8s.io/client-go/util/workqueue"
	"time"
)

// AdcManager updates ADC configuration.
type AdcManager struct {
	adcConfig *bootstrap.AdcConfig
	//implementing the adc API
	adcApi      *adcapicontroller.AdcApi
	requestTask workqueue.RateLimitingInterface
}

// NewLocalManager creates a LocalManager.
func NewAdcManager(c *bootstrap.AdcConfig) *AdcManager {
	manager := AdcManager{
		adcConfig:   c,
		adcApi:      adcapicontroller.NewAdcApi(c),
		requestTask: workqueue.NewRateLimitingQueue(workqueue.DefaultControllerRateLimiter()),
	}

	//go manager.run(stopCh)
	token, err := manager.adcApi.ADCLogin().ADCLogin()
	if err != nil {
		glog.Fatal("http client connect ADC failed:", err)
	}
	manager.adcApi.UpdateToken(token)
	return &manager
}

func (adcm *AdcManager) GetAdcConfig() *bootstrap.AdcConfig {
	return adcm.adcConfig
}

func (adcm *AdcManager) GetAdcApi() *adcapicontroller.AdcApi {
	return adcm.adcApi
}

func (adcm *AdcManager) Run(stopCh <-chan struct{}) {
	go adcm.healthCheckAndUpdateToken(stopCh)

	handleNext := func() {
		obj, quit := adcm.requestTask.Get()
		if quit {
			return
		}

		adcm.requestTask.Forget(obj)
		defer adcm.requestTask.Done(obj)

		adcm.sync(obj.(Task))
	}

	for {
		select {
		case <-stopCh:
			return
		default:
			handleNext()
		}
	}
}

func (adcm *AdcManager) SendRequestToQueue(t Task) {
	adcm.requestTask.Add(t)
}

// sync is called for each item in the queue
func (adcm *AdcManager) sync(t Task) {
	glog.Infof("AdcManager syncing %s", t.Kind.String())

	switch t.Kind {
	case Host:
		adcm.PushAdcHostData(t.Obj)
	case SlbPool:
		adcm.PushAdcSlbPoolData(t.Obj)
	case VS:
		adcm.PushAdcSlbVsData(t.Obj)
	}
}

func (adcm *AdcManager) PushAdcHostData(obj interface{}) error {
	if wrapAdcHostApiContext, ok := obj.(*model.AdcHostApiContextWrap); ok {
		var merr *multierror.Error
		if wrapAdcHostApiContext != nil {
			if wrapAdcHostApiContext.Deleted != nil {
				glog.Infof("PushAdcApiResource:Deleted:Hosts:%d", len(wrapAdcHostApiContext.Deleted))
				for host, _ := range wrapAdcHostApiContext.Deleted {
					if err := adcm.adcApi.DomainName().Delete(host); err != nil {
						merr = multierror.Append(merr, err)
					}
				}
			}

			if wrapAdcHostApiContext.Added != nil {
				glog.Infof("PushAdcApiResource:Added:Hosts:%d", len(wrapAdcHostApiContext.Added))
				for host, _ := range wrapAdcHostApiContext.Added {
					if err := adcm.adcApi.DomainName().Create(host); err != nil {
						merr = multierror.Append(merr, err)
					}
				}
			}

			if wrapAdcHostApiContext.Updated != nil {
				glog.Infof("PushAdcApiResource:Updated:Hosts:%d", len(wrapAdcHostApiContext.Updated))
				for _, hostApi := range wrapAdcHostApiContext.Updated {
					if err := adcm.adcApi.DomainName().Update(hostApi); err != nil {
						merr = multierror.Append(merr, err)
					}
				}
			}
		}

		if merr != nil {
			glog.Errorf("PushAdcApiResource error:%s", merr)
			return merr
		}
	} else {
		glog.Errorf("obj to AdcHostApiContextWrap failed")
		return fmt.Errorf("obj to AdcHostApiContextWrap failed")
	}

	return nil
}

func (adcm *AdcManager) PushAdcSlbPoolData(obj interface{}) error {
	if wrapAdcSlbPoolContext, ok := obj.(*model.AdcSlbPoolContextWrap); ok {
		var merr *multierror.Error
		if wrapAdcSlbPoolContext != nil {
			if wrapAdcSlbPoolContext.Deleted != nil {
				glog.Infof("PushAdcApiResource:Deleted:SlbPool:%d", len(wrapAdcSlbPoolContext.Deleted))
				for slbpoolName, _ := range wrapAdcSlbPoolContext.Deleted {
					if err := adcm.adcApi.ADCSlbPool().DeleteSlbPool(slbpoolName); err != nil {
						merr = multierror.Append(merr, err)
					}
				}
			}

			if wrapAdcSlbPoolContext.Added != nil {
				glog.Infof("PushAdcApiResource:Added:SlbPool:%d", len(wrapAdcSlbPoolContext.Added))
				for slbpoolName, members := range wrapAdcSlbPoolContext.Added {
					// first delete slbPool, When the process has just started, and the adc may have old data
					slbPoolUuid, _ := adcm.getSlbPoolUuidBySlbPoolName(slbpoolName)
					if slbPoolUuid != "" {
						err := adcm.adcApi.ADCSlbPool().DeleteSlbPool(slbpoolName)
						if err != nil {
							merr = multierror.Append(merr, err)
							glog.Infof("SlbPool busy, can't delete, update: %s", slbpoolName)
							// resource busy
							if err := adcm.adcApi.ADCSlbPool().UpdateSlbPool(slbpoolName, members); err != nil {
								merr = multierror.Append(merr, err)
							} else {
								continue
							}
						}
					}
					if err := adcm.adcApi.ADCSlbPool().CreateSlbPool(slbpoolName, members); err != nil {
						merr = multierror.Append(merr, err)
					}
				}
			}

			if wrapAdcSlbPoolContext.Updated != nil {
				glog.Infof("PushAdcApiResource:Updated:SlbPool:%d", len(wrapAdcSlbPoolContext.Updated))
				for slbpoolName, members := range wrapAdcSlbPoolContext.Updated {
					if err := adcm.adcApi.ADCSlbPool().UpdateSlbPool(slbpoolName, members); err != nil {
						merr = multierror.Append(merr, err)
					}
				}
			}
		}

		if merr != nil {
			glog.Errorf("PushAdcSlbPoolResource error:%s", merr)
			return merr
		}
	} else {
		glog.Errorf("obj to AdcSlbPoolContextWrap failed")
		return fmt.Errorf("obj to AdcSlbPoolContextWrap failed")
	}

	return nil
}

func (adcm *AdcManager) PushAdcSlbVsData(obj interface{}) error {
	if wrapAdcVsContext, ok := obj.(*model.AdcVsContextWrap); ok {
		var merr *multierror.Error
		if wrapAdcVsContext != nil {
			if wrapAdcVsContext.Deleted != nil {
				glog.Infof("PushAdcApiResource:Deleted:SlbVs:%d", len(wrapAdcVsContext.Deleted))
				for _, adcVs := range wrapAdcVsContext.Deleted {
					slbVsUuid, err := adcm.getSlbVsUuidBySlbVsName(adcVs.Name)
					if err != nil {
						merr = multierror.Append(merr, err)
						continue
					}
					adcVs.Uuid = slbVsUuid
					if err := adcm.adcApi.ADCSlbVs().DeleteSlbVs(adcVs); err != nil {
						merr = multierror.Append(merr, err)
					}
				}
			}

			if wrapAdcVsContext.Added != nil {
				glog.Infof("PushAdcApiResource:Added:SlbVs:%d", len(wrapAdcVsContext.Added))
				for _, adcVs := range wrapAdcVsContext.Added {
					// get slbpool uuid
					// Get it here because it is not affected by slbpool creation
					slbPoolUuid, err := adcm.getSlbPoolUuidBySlbPoolName(adcVs.SlbPoolName)
					if err != nil {
						merr = multierror.Append(merr, err)
						continue
					}
					adcVs.SlbPoolUuid = slbPoolUuid
					// first delete slbVs, When the process has just started, and the adc may have old data
					slbVsUuid, _ := adcm.getSlbVsUuidBySlbVsName(adcVs.Name)
					if slbVsUuid != "" {
						vsTemp := adcapicontroller.NewSlbVirtualServer()
						vsTemp.Uuid = slbVsUuid
						err := adcm.adcApi.ADCSlbVs().DeleteSlbVs(*vsTemp)
						if err != nil {
							merr = multierror.Append(merr, err)
							continue
						}
					}
					if err := adcm.adcApi.ADCSlbVs().CreateSlbVs(adcVs); err != nil {
						merr = multierror.Append(merr, err)
					}
				}
			}

			if wrapAdcVsContext.Updated != nil {
				glog.Infof("PushAdcApiResource:Updated:SlbVs:%d", len(wrapAdcVsContext.Updated))
				for _, adcVs := range wrapAdcVsContext.Updated {
					slbVsUuid, err := adcm.getSlbVsUuidBySlbVsName(adcVs.Name)
					if err != nil {
						merr = multierror.Append(merr, err)
						continue
					}
					slbPoolUuid, err := adcm.getSlbPoolUuidBySlbPoolName(adcVs.SlbPoolName)
					if err != nil {
						merr = multierror.Append(merr, err)
						continue
					}
					adcVs.Uuid = slbVsUuid
					adcVs.SlbPoolUuid = slbPoolUuid
					if err := adcm.adcApi.ADCSlbVs().UpdateSlbVs(adcVs); err != nil {
						merr = multierror.Append(merr, err)
					}
				}
			}
		}

		if merr != nil {
			glog.Errorf("PushAdcSlbVsResource error:%s", merr)
			return merr
		}
	} else {
		glog.Errorf("obj to AdcVsContextWrap failed")
		return fmt.Errorf("obj to AdcVsContextWrap failed")
	}

	return nil
}

func (adcm *AdcManager) getSlbPoolUuidBySlbPoolName(slbPoolName string) (string, error) {
	if slbPoolName == "" {
		return "", fmt.Errorf("slbPoolName is empty, can't get slbpooluuid")
	}
	var slbPoolUuid string
	err, slbPools := adcm.GetAdcApi().ADCSlbPool().ListSlbPool()
	if err != nil {
		glog.Errorf("list slbpool failed, can't get uuid:%v", err)
		return "", err
	}

	foundSlbpool := false
	for _, slbPool := range slbPools {
		if slbPool.Name == slbPoolName {
			slbPoolUuid = slbPool.Uuid
			foundSlbpool = true
			break
		}
	}

	if !foundSlbpool {
		glog.Errorf("%s:no slbpool found", slbPoolName)
		return "", fmt.Errorf("%s:no slbpool found", slbPoolName)
	}

	return slbPoolUuid, nil
}

func (adcm *AdcManager) getSlbVsUuidBySlbVsName(slbVsName string) (string, error) {
	if slbVsName == "" {
		return "", fmt.Errorf("slbVsName is empty, can't get slbvsuuid")
	}
	var slbVsUuid string
	err, slbVss := adcm.GetAdcApi().ADCSlbVs().ListSlbVs()
	if err != nil {
		glog.Errorf("list slbvs failed, can't get slbvsuuid:%v", err)
		return "", err
	}

	foundSlbVs := false
	for _, slbVs := range slbVss {
		if slbVs.Name == slbVsName {
			slbVsUuid = slbVs.Uuid
			foundSlbVs = true
			break
		}
	}

	if !foundSlbVs {
		glog.Errorf("%s:no slbvs found", slbVsName)
		return "", fmt.Errorf("%s:no slbvs found", slbVsName)
	}

	return slbVsUuid, nil
}

func (adcm *AdcManager) healthCheckAndUpdateToken(stopCh <-chan struct{}) {
	getToken := func() {
		token, err := adcm.adcApi.ADCLogin().ADCLogin()
		if err != nil {
			glog.Errorf("http client connect failed:%s", err)
			glog.V(2).Infof("Adc no health...")
			return
		}
		adcm.adcApi.UpdateToken(token)
		glog.V(2).Infof("Adc health...")
	}
	interval := time.Duration(adcm.adcConfig.HealthCheckInterval)
	ticker := time.NewTicker(interval * time.Second)
	for {
		select {
		case <-stopCh:
			return
		case <-ticker.C:
			getToken()
		}
	}
}

// kind represents the kind of the Kubernetes resources of a task
type kind int

// resources
const (
	Host = iota
	SlbPool
	VS
)

// Task is an element of a taskQueue
type Task struct {
	Kind kind
	Obj  interface{}
}

func (k kind) String() string {
	switch k {
	case Host:
		return "host"
	case SlbPool:
		return "slbpool"
	case VS:
		return "vs"
	default:
		return "UnknownResource"
	}
}
