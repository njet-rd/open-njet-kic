- [NJet Ingress Controller](#njet-ingress-controller)     
    - [Overview](#overview)   
    - [Build NJet Ingress Controller image](#build-njet-ingress-controller-image)    
    - [Deploy NJet Ingress Controller](#deploy-njet-ingress-controller)    
    - [Uninstall NJet Ingress Controller](#uninstall-njet-ingress-controller)       
    - [Getting Started](#getting-started)   
    - [Acknowledgments](#acknowledgments)    
   

# NJet Ingress Controller
## Overview
架构图  
![architecture](docs/picture/njet-kic-architecture.png)   

- NJet Ingress Controller基于NJet proxy的动态特性、高性能实现。弥补nginx 在云原生场景中应用的不足。  
- NJet Ingress Controller提供了丰富的流量管理功能，如动态location、host/path路由、负载均衡、动态upstream、金丝雀发布、SNI等。 
- NJet Ingress Controller基于Nginx官方kubernetes-ingress项目定制开发，主要实现了配置的动态化，不需要reload NJet。   
- NJet Ingress Controller在upstream 动态化实现方面，使用lua实现upstream动态配置。     


## Build NJet Ingress Controller image
通过git submodule，下载njet_main和lua_module源码
```sh
cd njet-kic
git submodule init
git submodule update 
```
执行成功后，NJet及lua源码被下载到build/njet_lua_src/。进入build/njet_lua_src/njet_main目录，git checkout xxx，否则引用的是HEAD，最终生成的KIC镜像无法获取到正确的NJet 分支名。

构建NJet KIC镜像, 在项目顶级目录中，执行如下指令：
```sh
#PREFIX、VERSION可以不指定，Makefile中定义了默认值
make ubuntu-image-njet PREFIX=njet-ingress VERSION=1.0 
```

### gitlab环境下构建
支持gitlab CI构建，脚本详见.gitlab-ci.yml    


## Deploy NJet Ingress Controller 
### Kubernetes manifests 
创建ns和sa
```sh
cd njet-kic
kubectl apply -f ./deployments/common/ns-and-sa.yaml 
```
创建configMap和ingressClass
```sh
cd njet-kic
kubectl apply -f ./deployments/common/njet-configMap.yaml
kubectl apply -f ./deployments/common/njet-class.yaml 
```
创建CR
```sh
cd njet-kic
kubectl apply -f ./deployments/common/crds/ 
```
配置RBAC
```sh
cd njet-kic
kubectl apply -f ./deployments/rbac/rbac.yaml 
```
运行NJet Ingress Controller
```sh
cd njet-kic
kubectl apply -f  ./deployments/deployment/njet-ingress.yaml 
```
通过nodePort service访问NJet Ingress Controller
```sh
cd njet-kic
kubectl apply -f ./deployments/service/nodeport.yaml
```


## Uninstall NJet Ingress Controller
### Kubernetes manifests
删除njet-ingress namespace
```sh
kubectl delete namespace njet-ingress
```

删除RBAC
```sh
kubectl delete clusterrolebinding  njet-ingress    
kubectl delete clusterrole njet-ingress
```

删除CRD
```sh
kubectl delete -f ./deployments/common/crds/
```   


## Getting Started
1. 学习更多，请查阅 [docs](https://gitee.com/njet-rd/docs).


## Acknowledgments
由于kubernetes-ingress项目和ingress-nginx 项目的贡献才有了如今NJet KIC项目的诞生，非常感谢kubernetes-ingress项目和ingress-nginx 项目的贡献者，致敬。
