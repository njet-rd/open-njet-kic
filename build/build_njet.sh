#!/bin/bash
export BUILD_PATH=/tmp/build

mkdir --verbose -p "$BUILD_PATH"
cd "$BUILD_PATH"

apt-get update
apt-get install gcc make -y
apt-get install libpcre3-dev libssl-dev zlib1g-dev perl cmake g++ vim libcap2-bin -y

ln -sf /sbin/setcap /usr/sbin/setcap
# Install luajit
export LUAJIT_INC='/usr/local/include/luajit-2.1'
export LUAJIT_LIB='/usr/local/lib'

chmod +x $BUILD_PATH/njet_main/auto/lib/tassl/Configure
rm -f $BUILD_PATH/njet_main/auto/lib/tassl/Makefile

cd "$BUILD_PATH/njet_main/luajit"
make CCDEBUG=-g
make install

ln -sf /usr/local/lib/libluajit-5.1.so.2.1.0 /usr/lib/libluajit-5.1.so
ln -sf /usr/local/lib/libluajit-5.1.so.2.1.0 /usr/lib/libluajit-5.1.so.2

# build njet
cd "$BUILD_PATH/njet_main"

#CC_OPT="-O0 -fsanitize=address -static-libgcc -static-libasan "
#LD_OPT="-fsanitize=address -static-libgcc -static-libasan -ldl -lm "
CC_OPT=" -O0 "
LD_OPT="-ldl -lm "

WITH_FLAGS=" --with-debug \
  --with-stream \
  --with-http_addition_module \
  --with-http_auth_request_module \
  --with-http_dav_module \
  --with-http_flv_module \
  --with-http_gunzip_module \
  --with-http_gzip_static_module \
  --with-http_mp4_module \
  --with-http_random_index_module \
  --with-http_realip_module \
  --with-http_secure_link_module \
  --with-http_slice_module \
  --with-http_ssl_module \
  --with-http_stub_status_module \
  --with-http_sub_module \
  --with-http_v2_module \
  --with-mail \
  --with-mail_ssl_module \
  --with-stream_realip_module \
  --with-stream_ssl_module \
  --with-stream_ssl_preread_module \
  --with-openssl=auto/lib/tongsuo "

WITH_MODULES=" \
  --add-module=$BUILD_PATH/njet_main/src/ext/lua/kit \
  --add-module=$BUILD_PATH/njet_main/src/ext/lua/http \
  --add-module=$BUILD_PATH/njet_main/src/ext/lua/stream \
  --add-module=$BUILD_PATH/njet_main/modules/njet-stream-proto-module \
  --add-module=$BUILD_PATH/njet_main/modules/njet-util-module \
  --add-module=$BUILD_PATH/njet_main/modules/njet-stream-upstream-dynamic-servers-module \
  --add-module=$BUILD_PATH/njet_main/modules/njet-http-upstream-dynamic-servers-module \
  --add-module=$BUILD_PATH/njet_main/modules/njet-http-kv-module \
  --add-module=$BUILD_PATH/njet_main/modules/njet-mqconf-module \
  --add-module=$BUILD_PATH/njet_main/modules/njet-vts-module \
  --add-module=$BUILD_PATH/njet_main/modules/njet-http-proxy-connect-module \
  --add-module=$BUILD_PATH/njet_main/modules/njet-cache-purge-module \
  --add-module=$BUILD_PATH/njet_main/modules/njet-http-if-location-module \
  --add-module=$BUILD_PATH/njet_main/modules/njet-stream-proxy-protocol-tlv-module \
  --add-module=$BUILD_PATH/njet_main/modules/njet-http-api-register-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-vtsc-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-vtsd-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-http-location-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-http-location-api-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-http-upstream-api-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-helper-ctrl-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-helper-broker-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-config-api-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-http-sendmsg-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-http-dyn-bwlist-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-http-split-clients-2-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-health-check-helper \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-http-dyn-map-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-stream-dyn-map-module \
  --add-module=$BUILD_PATH/njet_main/modules/njet-http-fault-inject-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/src/ext/lua/http \
  --add-dynamic-module=$BUILD_PATH/njet_main/src/ext/lua/stream \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-range-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-http-dyn-range-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-http-range-api-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-http-dyn-server-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-http-dyn-server-api-module \
  --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-http-register-module "

# --add-dynamic-module=$BUILD_PATH/njet_main/modules/njet-doc-module \
# --add-module=$BUILD_PATH/njet_main/modules/njet-jwt-module \

#api doc make tar file
doctar=doc.tar

if [ -f $doctar ]
then
   rm $doctar
fi

if [ -f $doctar.gz ]
then
   rm $doctar.gz
fi
tar cvf $doctar doc
gzip $doctar
xxd -i $doctar.gz > src/http/njt_doc_gz.h
if [ -f $doctar ]
then
   rm $doctar
fi

if [ -f $doctar.gz ]
then
   rm $doctar.gz
fi

./configure \
  --prefix=/usr/local/njet \
  --conf-path=/etc/njet/njet.conf \
  --lock-path=/usr/local/njet/njet.lock \
  --modules-path=/usr/local/njet/dynamic-modules \
  --pid-path=/usr/local/njet/njet.pid \
  --http-client-body-temp-path=/var/cache/njet/client_body_temp \
  --http-fastcgi-temp-path=/var/cache/njet/fastcgi_temp \
  --http-proxy-temp-path=/var/cache/njet/proxy_temp \
  --http-scgi-temp-path=/var/cache/njet/scgi_temp \
  --http-uwsgi-temp-path=/var/cache/njet/uwsgi_temp \
  ${WITH_FLAGS} \
  --with-cc-opt="${CC_OPT}" \
  --with-ld-opt="${LD_OPT}" \
  --without-mail_pop3_module \
  --without-mail_smtp_module \
  --without-mail_imap_module \
  --without-http_uwsgi_module \
  --without-http_scgi_module \
  ${WITH_MODULES}

make
make install

cd "$BUILD_PATH/lua_module/resty-core"
make install

cd "$BUILD_PATH/lua_module/resty-balancer"
make all
make install

export LUA_INCLUDE_DIR=/usr/local/include/luajit-2.1
ln -s $LUA_INCLUDE_DIR /usr/include/lua5.1

cd "$BUILD_PATH/lua_module/resty-cjson"
make all
make install

cd "$BUILD_PATH/lua_module/resty-cookie"
make all
make install

cd "$BUILD_PATH/lua_module/resty-dns"
make install

cd "$BUILD_PATH/lua_module/resty-lock"
make install

# required for OCSP verification
cd "$BUILD_PATH/lua_module/resty-http"
make install

cd "$BUILD_PATH/lua_module/resty-ipmatcher"
INST_LUADIR=/usr/local/lib/lua make install

cd "$BUILD_PATH/lua_module/resty-global-throttle"
make install

rm -r $BUILD_PATH/*
apt-get autoremove gcc make perl cmake g++  -y
apt-get clean
rm -rf  /var/log/*log /var/lib/apt/lists/* /var/log/apt/* /var/lib/dpkg/*-old /var/cache/debconf/*-old



