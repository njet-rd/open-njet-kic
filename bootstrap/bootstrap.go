package bootstrap

import (
	"fmt"
	"os"
	"strings"
)

const (
	ADC_SERVER_IP   = "ADC_SERVER_IP"
	ADC_SERVER_PORT = "ADC_SERVER_PORT"
	ADC_USERNAME    = "ADC_USERNAME"
	ADC_PASSWORD    = "ADC_PASSWORD"
)

type AdcCmdFlags struct {
	IP                  string
	Port                string
	AdcUsername         string
	AdcPassword         string
	HealthCheckInterval int64
	AdcGslbPoolUuid     string
	NjetSvcname         string
}
type AdcConfig struct {
	ServerURI           string
	AdcUsername         string
	AdcPassword         string
	HealthCheckInterval int64
	AdcGslbPoolUuid     string
	NjetSvcname         string
}

func NewConfig(cmdFlags *AdcCmdFlags) (*AdcConfig, error) {
	config := &AdcConfig{}
	var adcIP string
	var adcPort string
	if cmdFlags.IP == "" {
		adcIP = os.Getenv(ADC_SERVER_IP)
	} else {
		adcIP = cmdFlags.IP
	}

	if cmdFlags.Port == "" {
		adcPort = os.Getenv(ADC_SERVER_PORT)
	} else {
		adcPort = cmdFlags.Port
	}
	if adcIP == "" || adcPort == "" {
		return config, fmt.Errorf("unable to load adc ip and port configuration, env \"ADC_SERVER_PORT\" and \"ADC_SERVER_IP\" or command line parameter \"adc-ip\" and \"adc-port\" must be defined")
	}

	config.ServerURI = fmt.Sprintf("https://%s", strings.Join([]string{adcIP, adcPort}, ":"))

	var adcUsername string
	var adcPassword string
	if cmdFlags.AdcUsername == "" {
		adcUsername = os.Getenv(ADC_USERNAME)
	} else {
		adcUsername = cmdFlags.AdcUsername
	}

	if cmdFlags.AdcPassword == "" {
		adcPassword = os.Getenv(ADC_PASSWORD)
	} else {
		adcPassword = cmdFlags.AdcPassword
	}
	if adcUsername == "" || adcPassword == "" {
		return config, fmt.Errorf("unable to load adc username and password configuration, env \"ADC_USERNAME\" and \"ADC_PASSWORD\" or command line parameter \"adc-username\" and \"adc-password\" must be defined")
	}

	if cmdFlags.NjetSvcname == "" {
		return config, fmt.Errorf("command line parameter \"njet-servicename\" must be defined")
	}
	config.AdcUsername = adcUsername
	config.AdcPassword = adcPassword
	config.HealthCheckInterval = cmdFlags.HealthCheckInterval
	config.AdcGslbPoolUuid = cmdFlags.AdcGslbPoolUuid
	config.NjetSvcname = cmdFlags.NjetSvcname

	return config, nil
}
