#!/bin/bash

VERSION=$1
if BUILD_GIT_REVISION=$(git rev-parse HEAD 2> /dev/null); then
  if [[ -z "${IGNORE_DIRTY_TREE}" ]] && [[ -n "$(git status --porcelain 2>/dev/null)" ]]; then
    BUILD_GIT_REVISION=${BUILD_GIT_REVISION}"-dirty"
  fi
else
  BUILD_GIT_REVISION=unknown
fi

# Check for local changes
tree_status="Clean"
if [[ -z "${IGNORE_DIRTY_TREE}" ]] && ! git diff-index --quiet HEAD --; then
  tree_status="Modified"
fi

GIT_DESCRIBE_TAG=$(git describe --tags 2> /dev/null)
BRANCH=$(git symbolic-ref --short -q HEAD 2> /dev/null)

echo "github.com/nginxinc/kubernetes-ingress/internal/nginx/version.RELEASE=${VERSION:-$BUILD_GIT_REVISION}"
echo "github.com/nginxinc/kubernetes-ingress/internal/nginx/version.COMMIT=${BUILD_GIT_REVISION}"
echo "github.com/nginxinc/kubernetes-ingress/internal/nginx/version.TAG=${GIT_DESCRIBE_TAG}"
echo "github.com/nginxinc/kubernetes-ingress/internal/nginx/version.BRANCH=${BRANCH}"

NJET_SRC=./build/njet_lua_src/njet_main/
cd $NJET_SRC

if BUILD_GIT_REVISION_NJET=$(git rev-parse HEAD 2> /dev/null); then
  if [[ -z "${IGNORE_DIRTY_TREE}" ]] && [[ -n "$(git status --porcelain 2>/dev/null)" ]]; then
    BUILD_GIT_REVISION=${BUILD_GIT_REVISION_NJET}"-dirty"
  fi
else
  BUILD_GIT_REVISION_NJET=unknown
fi

# Check for local changes
tree_status="Clean"
if [[ -z "${IGNORE_DIRTY_TREE}" ]] && ! git diff-index --quiet HEAD --; then
  tree_status="Modified"
fi

GIT_DESCRIBE_TAG_NJET=$(git describe --tags)
NJET_BRANCH=$(git branch | grep \* | cut -d ' ' -f2 2> /dev/null)

echo "github.com/nginxinc/kubernetes-ingress/internal/nginx/version.NJET_RELEASE=${BUILD_GIT_REVISION_NJET}"
echo "github.com/nginxinc/kubernetes-ingress/internal/nginx/version.NJET_COMMIT=${BUILD_GIT_REVISION_NJET}"
echo "github.com/nginxinc/kubernetes-ingress/internal/nginx/version.NJET_TAG=${GIT_DESCRIBE_TAG_NJET}"
echo "github.com/nginxinc/kubernetes-ingress/internal/nginx/version.NJET_BRANCH=${NJET_BRANCH}"